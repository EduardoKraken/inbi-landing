<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Aprender inglés no siempre tiene por qué ser aburrido";
    $canonical = "<link rel='canonical' href='https://inbi.mx/aprender-ingles-no-siempre-tiene-por-que-ser-aburrido'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/aprender-ingles-no-siempre-tiene-por-que-ser-aburrido.php';
    include 'includes/footers/footer.php';
?>