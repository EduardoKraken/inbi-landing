<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "La mejor escuela de inglés para niños, jóvenes y adultos en línea";
    $metadescription = "Estas a un clic de aprender inglés desde tu celular o computadora. Aprende inglés al mejor precio con nuestras becas. Conoce nuestras promociones y sé bilingüe.";
    $canonical = "<link rel='canonical' href='https://inbi.mx/sucursal_online'/>";
    
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/sucursales/sucursal_online.php';
    include 'includes/footers/footer.php';
?>