<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Info pago";
    $canonical = "";
    $canonical = "<link rel='canonical' href='https://inbi.mx/info-pago'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/info-pago.php';
    include 'includes/footers/footer.php';
?>