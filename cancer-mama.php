<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Cáncer de mama alrededor del mundo “Breast cancer worldwide”";
    $canonical = "<link rel='canonical' href='https://inbi.mx/cancer-mama'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/cancer-mama.php';
    include 'includes/footers/footer.php';
?>