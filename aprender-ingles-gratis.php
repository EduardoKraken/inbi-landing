<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Aprender Inglés Gratis";
    $canonical = "<link rel='canonical' href='https://inbi.mx/aprender-ingles-gratis'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/aprender-ingles-gratis.php';
    include 'includes/footers/footer.php';
?>