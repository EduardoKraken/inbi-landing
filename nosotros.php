<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "Sobre nosotros";
  $canonical = "<link rel='canonical' href='https://inbi.mx/nosotros'/>";
  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/nosotros.php';
  include 'includes/footers/footer.php';
?>