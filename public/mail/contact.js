$(function () {

  $("#contactForm input, #contactForm textarea").jqBootstrapValidation({
    preventSubmit: true,
    submitError: function ($form, event, errors) {
    },
    submitSuccess: function ($form, event) {
      event.preventDefault();
      var name = $("input#name").val();
      var email = $("input#email").val();
      var subject = $("input#subject").val();
      var message = $("textarea#message").val();

      $this = $("#sendMessageButton");
      $this.prop("disabled", true);

      $.ajax({
        url: "contact.php",
        type: "POST",
        data: {
          name: name,
          email: email,
          subject: subject,
          message: message
        },
        cache: false,
        success: function () {
          $('#success').html("<div class='alert alert-success'>");
          $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
          .append("</button>");
          $('#success > .alert-success')
          .append("<strong>Your message has been sent. </strong>");
          $('#success > .alert-success')
          .append('</div>');
          $('#contactForm').trigger("reset");
        },
        error: function () {
          $('#success').html("<div class='alert alert-danger'>");
          $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
          .append("</button>");
          $('#success > .alert-danger').append($("<strong>").text("Sorry " + name + ", it seems that our mail server is not responding. Please try again later!"));
          $('#success > .alert-danger').append('</div>');
          $('#contactForm').trigger("reset");
        },
        complete: function () {
          setTimeout(function () {
            $this.prop("disabled", false);
          }, 1000);
        }
      });
    },
    filter: function () {
      return $(this).is(":visible");
    },
  });

  $("#contactForma input, #contactForma textarea").jqBootstrapValidation({
    preventSubmit: true,
    submitError: function ($form, event, errors) {
    },
    submitSuccess: function ($form, event) {
      event.preventDefault();
      if ($('#floatingNombre').val() == "") {
        $('#floatingNombre').addClass('is-invalid');
      } else {
        $('#floatingNombre').removeClass('is-invalid');
      }

      if ($('#floatingCelular').val() == "") {
        $('#floatingCelular').addClass('is-invalid');
      } else {
        $('#floatingCelular').removeClass('is-invalid');
      }


      if ($('#floatingNombre').val() == "" || $('#floatingCelular').val() == "" ) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Lo sentimos no pueden ir campos vacios',
        });

        return;
      }

      $.ajax({
        type: 'POST',
        url: "app/operaciones/contacto_vinculacion.php",
        data: {
          "nombre": $('#floatingNombre').val(),
          "celular": $('#floatingCelular').val(),
          "mensaje": $('#floatingMensaje').val()
        },
        dataType: 'JSON',
        beforeSend: function () {
          Swal.fire({
            icon: 'info',
            title: 'Un momento',
            text: 'Estamos enviando tus datos!',
            showCancelButton: false,
            showConfirmButton: false
          });
        },
      }).done(function (response) {
        if (response[0].error) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: response[0].mensaje,
          });
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'Tus datos han sido enviados con éxito, nos comunicaremos contigo lo mas pronto posible.',
          });
          $('#floatingNombre').val('');
          $('#floatingCelular').val('');
        }
      });
    },
    filter: function () {
      return $(this).is(":visible");
    },
  });

  $("a[data-toggle=\"tab\"]").click(function (e) {
    e.preventDefault();
    $(this).tab("show");
  });
});

$('#name').focus(function () {
  $('#success').html('');
});
