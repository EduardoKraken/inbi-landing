$('#btn-enviar-datos-vacantes').click(function () {

    if ($('#floatingNombre').val() == "") {
      $('#floatingNombre').addClass('is-invalid');
    } else {
      $('#floatingNombre').removeClass('is-invalid');
    }

    if ($('#floatingEdad').val() == "") {
      $('#floatingEdad').addClass('is-invalid');
    } else {
      $('#floatingEdad').removeClass('is-invalid');
    }
  
    if ($('#floatingCelular').val() == "") {
      $('#floatingCelular').addClass('is-invalid');
    } else {
      $('#floatingCelular').removeClass('is-invalid');
    }

    if ($('#floatingEmail').val() == "") {
      $('#floatingEmail').addClass('is-invalid');
    } else {
      $('#floatingEmail').removeClass('is-invalid');
    }

    if ($('#floatingSucursal').val() == 0) {
      $('#floatingSucursal').addClass('is-invalid');
    } else {
      $('#floatingSucursal').removeClass('is-invalid');
    }

    if ($('#floatingVacante').val() == 0) {
      $('#floatingVacante').addClass('is-invalid');
    } else {
      $('#floatingVacante').removeClass('is-invalid');
    }
  
  
    if ($('#floatingNombre').val() == "" || $('#floatingCelular').val() == ""  || $('#floatingEdad').val() == "" || $('#floatingEmail').val() == "" || $('#floatingSucursal').val() == 0 || $('#floatingVacante').val() == 0  )  {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Lo sentimos no pueden ir campos vacios',
      });
  
      return;
    }

    var archivoInput = document.getElementById('floatingCV');
    var archivo = archivoInput.files[0];

    var formData = new FormData();
    formData.append('nombre', $('#floatingNombre').val());
    formData.append('edad', $('#floatingEdad').val());
    formData.append('celular', $('#floatingCelular').val());
    formData.append('correo', $('#floatingEmail').val());
    formData.append('sucursal', $('#floatingSucursal').val());
    formData.append('vacante', $('#floatingVacante').val());
    formData.append('archivo', archivo);
  
    $.ajax({
      type: 'POST',
      url: "app/operaciones/bolsa_trabajo.php",
      data: formData,
      processData: false,
      contentType: false,
      beforeSend: function () {
        Swal.fire({
          icon: 'info',
          title: 'Un momento',
          text: 'Estamos enviando tus datos!',
          showCancelButton: false,
          showConfirmButton: false
        });
      },
    }).done(function (response) {
      if (response[0].error) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: response[0].mensaje,
        });
      } else {
        Swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'Tus datos han sido enviados con éxito, nos comunicaremos contigo lo mas pronto posible.',
        });
        $('#floatingNombre').val('');
        $('#floatingEdad').val('');
        $('#floatingCelular').val('');
        $('#floatingEmail').val('');
        $('#floatingSucursal').val(0);
        $('#floatingVacante').val(0);
      }
    });
  });
  