<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Cómo mejorar nuestra pronunciación";
    $canonical = "<link rel='canonical' href='https://inbi.mx/como-mejorar-nuestra-pronunciacion'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/como-mejorar-nuestra-pronunciacion.php';
    include 'includes/footers/footer.php';
?>