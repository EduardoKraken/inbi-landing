<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Inglés en Ingenieria";
    $canonical = "<link rel='canonical' href='https://inbi.mx/ingles-en-ingenieria'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/ingles-en-ingenieria.php';
    include 'includes/footers/footer.php';
?>