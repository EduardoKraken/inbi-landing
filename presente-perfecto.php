<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Presente perfecto";
    $canonical = "<link rel='canonical' href='https://inbi.mx/presente-perfecto'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/presente-perfecto.php';
    include 'includes/footers/footer.php';
?>