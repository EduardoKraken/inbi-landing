<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Contacto vinculación";
    $metadescription = "Tenemos la mejor solución para que tus colaboradores aprendan inglés presencialmente o en línea. Conoce nuestra propuesta.";
    $canonical = "<link rel='canonical' href='https://inbi.mx/contacto-vinculacion'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/contacto-vinculacion.php';
    include 'includes/footers/footer.php';
?>