<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "La importancia de los idiomas en el mercado laboral";
    $canonical = "<link rel='canonical' href='https://inbi.mx/importancia-idiomas-mercado-laboral'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/importancia-idiomas-mercado-laboral.php';
    include 'includes/footers/footer.php';
?>