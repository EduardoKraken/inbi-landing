<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Curso de inglés para jóvenes y adultos - Tenemos becas!";
    $metadescription = "Si necesitas aprender inglés para la escuela, el trabajo o viajar, somos tu mejor opción. Nuestras clases son 100% en inglés e interactivas. Conoce nuestras becas!";
    $canonical = "<link rel='canonical' href='https://inbi.mx/curso-intensivo-adultos'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/cursos/curso-intensivo-adultos.php';
    include 'includes/footers/footer.php';
?>