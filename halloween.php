<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Halloween";
    $canonical = "<link rel='canonical' href='https://inbi.mx/halloween'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/halloween.php';
    include 'includes/footers/footer.php';
?>