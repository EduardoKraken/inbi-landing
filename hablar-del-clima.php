<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Hablar del clima";
    $canonical = "<link rel='canonical' href='https://inbi.mx/hablar-del-clima'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/hablar-del-clima.php';
    include 'includes/footers/footer.php';
?>