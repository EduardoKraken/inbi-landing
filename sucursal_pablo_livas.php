<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Clases de inglés en Guadalupe - Conoce nuestras becas!!";
    $metadescription = "Estamos muy cerca de ti, conoce nuestros planteles en Guadalupe. Aprende inglés al mejor precio con nuestras becas. Conoce nuestras promociones y sé bilingüe.";
    $canonical = "<link rel='canonical' href='https://inbi.mx/sucursal_pablo_livas'/>";

    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/sucursales/sucursal_pablo_livas.php';
    include 'includes/footers/footer.php';
?>