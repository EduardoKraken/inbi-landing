<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Sucursal Lincoln";
    $metadescription = "La mejor escuela de inglés para niños, jóvenes y adultos en Lincoln y Ruiz Cortines";
    $canonical = "<link rel='canonical' href='https://inbi.mx/sucursal_lincoln'/>";

    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/sucursales/sucursal_lincoln.php';
    include 'includes/footers/footer.php';
?>