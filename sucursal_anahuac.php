<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "La mejor escuela de inglés para niños, jóvenes y adultos en San Nicolás";
    $metadescription = "Estamos muy cerca de ti, conoce nuestros planteles en San Nicolás. Aprende inglés al mejor precio con nuestras becas. Conoce nuestras promociones y sé bilingüe.  ";
    $canonical = "<link rel='canonical' href='https://inbi.mx/sucursal_anahuac'/>";
    
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/sucursales/sucursal_anahuac.php';
    include 'includes/footers/footer.php';
?>