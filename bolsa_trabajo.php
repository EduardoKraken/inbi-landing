<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Bolsa de Trabajo";
    $canonical = "<link rel='canonical' href='https://inbi.mx/bolsa_trabajo'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/bolsa_trabajo.php';
    include 'includes/footers/footer.php';
?>