<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Utilizar la lectura para mejorar nuestro dominio del inglés";
    $canonical = "<link rel='canonical' href='https://inbi.mx/lectura-mejora-dominio-ingles'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/lectura-mejora-dominio-ingles.php';
    include 'includes/footers/footer.php';
?>