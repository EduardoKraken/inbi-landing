<?php
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  // require '../../vendor/autoload.php';

  require '../../vendor/phpmailer/phpmailer/src/Exception.php';
  require '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
  require '../../vendor/phpmailer/phpmailer/src/SMTP.php';

  $nombre     = $_POST['nombre'];
  $edad       = $_POST['edad'];
  $celular    = $_POST['celular'];
  $email      = $_POST['correo'];
  $vacante    = $_POST['vacante'];
  $sucursal   = $_POST['sucursal'];

  $archivo    = $_FILES['archivo'];

  // Ruta temporal del archivo
  $archivo_tmp = $archivo['tmp_name'];

  // Nombre original del archivo
  $nombre_archivo = $archivo['name'];

  // Carpeta de destino para guardar el archivo
  $carpeta_destino = 'archivos/';

  // Mover el archivo a la carpeta de destino
  $ruta_destino = $carpeta_destino . $nombre_archivo;
  move_uploaded_file($archivo_tmp, $ruta_destino);

  //Obtenemos la fecha 
  $date = date('d-m-Y');
  //Definimos variable mes
  $mes = "";
  //Desglosamos los días con un guión
  list($day, $month, $year) = explode('-', $date);

  //Obtenemos el mes dependiendo del numero
  if($month == '01'){
    $mes = "Enero";
  }

  if($month == '02'){
    $mes = "Febrero";
  }

  if($month == '03'){
    $mes = "Marzo";
  }

  if($month == '04'){
    $mes = "Abril";
  }

  if($month == '05'){
    $mes = "Mayo";
  }

  if($month == '06'){
    $mes = "Junio";
  }

  if($month == '07'){
    $mes = "Julio";
  }

  if($month == '08'){
    $mes = "Agosto";
  }

  if($month == '09'){
    $mes = "Septiembre";
  }

  if($month == '10'){
    $mes = "Octubre";
  }

  if($month == '11'){
    $mes = "Noviembre";
  }

  if($month == '12'){
    $mes = "Diciembre";
  }

  //Obtenemos la vacante
  switch ($vacante) {
    case 1:
      $vacante = "Teacher";
    break;
    
    case 2:
      $vacante = "Recepcionista";
    break;
    
    case 3:
      $vacante = "Encargada de Sucursal";
    break;  

  }

  //Obtenemos la sucursal
  switch ($sucursal) {
    case 1:
      $sucursal = "Sucursal Anáhuac";
    break;
    case 2:
      $sucursal = "Sucursal Apodaca";
    break;
    case 3:
      $sucursal = "Sucursal Casa Blanca";
    break;
    case 4:
      $sucursal = "Sucursal Escobedo";
    break;
    case 5:
      $sucursal = "Sucursal Fresnos";
    break;
    case 6:
      $sucursal = "Sucursal Lincoln";
    break;
    case 7:
      $sucursal = "Sucursal Miguel Alemán";
    break;
    case 8:
      $sucursal = "Online";
    break;
    case 9:
      $sucursal = "Sucursal Pablo Livas";
    break;
    case 10:
      $sucursal = "Sucursal San Miguel";
    break;
  }

  //Obtenemos la fecha que se usara en el mensaje del correo
  $fecha_final = $day . " de " . $mes . " del " . $year;


  // $mail = new PHPMailer(true);
  //Inicializa variable a devolver
  $data = array();
  try {
    $mail = new PHPMailer();
    //$mail->SMTPDebug = 3;
    // $mail = \Config\Services::email();
    // SMTP Parametros.
    $mail->isSMTP();
    // SMTP Server Host. 
    $mail->Host = 'smtp.gmail.com';
    // Use SMTP autentificacion. 
    $mail->SMTPAuth = true;
    // Introduce la incriptacion del sistema. 
    $mail->SMTPSecure = 'tls';
    // SMTP usuario. 
    $mail->Username = 'staff.inbischool@gmail.com';
    // SMTP contraseña. 
    $mail->Password = 'sgjywlyabjawvlzv';
    // Introduce el SMTP Puerto.
    $mail->Port = 587;
    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';

    // Añade remitente.
    $mail->setFrom('web-master@inbi.mx', 'Inbi School');
    // Añade destinatario.
    $mail->addAddress('rh.schoolofenglish@gmail.com', 'Informes INBI-MX');
    // Asunto del correo.
    $mail->Subject = 'Postulante en Bolsa de Trabajo en inbi.mx';
    
    // Cuerpo del correo.
    $mail->isHTML(false);

    $mail->Body = "Nombre: $nombre\nEdad: $edad\nCelular: $celular\nEmail: $email\nVacante: $vacante\nSucursal: $sucursal\n\nSe adjunta el archivo enviado.";

    // $mail->Body = 
    // '<html>
    //     <h2>Se ha recibido un nuevo postulante</h2>
    //     <p>Nombre: '. $nombre .'</p>
    //     <p>Edad: '. $edad .'</p>
    //     <p>Celular: '. $celular .'</p>
    //     <p>Email: '. $email .'</p>
    //     <p>Vacante: '. $vacante  .'</p>
    //     <p>Sucursal: '. $sucursal .'</p>
    //     <p>Fecha: '. $fecha_final .' </p>
    //     <p><b> CV Adjuntado </b></p>
    // </html>';

    // En caso de incluir un archivo
    $mail->addAttachment($ruta_destino, $nombre_archivo);
    
    // Enviar correo
    $mail->send();

    $data[] = array('error' => false, 'mensaje' => "Correo enviado");
    //retornar datos
    echo json_encode($data);
  } catch (Exception $e) {
    $data[] = array('error' => true, 'mensaje' => $e->errorMessage());
    echo json_encode($data);
  } catch (\Exception $e) {
    $data[] = array('error' => true, 'mensaje' => $e->getMessage());
    echo json_encode($data);
  }

  // echo "Enviado";
?>