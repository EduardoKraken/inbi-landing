<!-- Navbar Start -->
<div class="container-fluid nav-bar p-0 page-header">
  <div class="container-lg p-0">

    <nav class="navbar navbar-expand-lg bg-secondary page-header navbar-dark">

      <a href="index" class="navbar-brand" aria-label="Inicio de la página">
        <img src="public/img/logo_vertical_blanco.png" alt="" width="200" height="80">
      </a>
      <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse" aria-label="boton_hamburguesa">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse justify-content-between " id="navbarCollapse">
        <div class="navbar-nav ml-auto py-0">
          <a 
            href="index"  
            class="nav-item nav-link <?php if ($titulo == 'Inicio') { echo 'active'; } ?>"
          >
            Inicio
          </a>

          <a 
            href="nosotros" 
            class="nav-item nav-link <?php if ($titulo == 'Sobre nosotros') { echo 'active'; } ?>"
          >
            Nosotros
          </a>
          
          <a 
            href="cursos" 
            class="nav-item nav-link <?php if ($titulo == 'Cursos') { echo 'active'; } ?>"
          >
            Cursos
          </a>
          
          <div class="nav-item dropdown">
            <a 
              href="#" 
              class="nav-link dropdown-toggle" 
              data-toggle="dropdown"
            >
              Planteles
            </a>
            
            <div class="dropdown-menu border-0 rounded-0 m-0">
              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Anáhuac') { echo 'active'; } ?>" 
                href="sucursal_anahuac"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Anáhuac
              </a>

              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Apodaca') { echo 'active'; } ?>" 
                href="sucursal_apodaca"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Apodaca
              </a>

              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Casa Blanca') { echo 'active'; } ?>" 
                href="sucursal_casa_blanca"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Casa Blanca
              </a>

              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Escobedo') { echo 'active'; } ?>" 
                href="sucursal_escobedo"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Escobedo
              </a>
              
              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Fresnos') { echo 'active'; } ?>" 
                href="sucursal_fresnos"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Fresnos
              </a>

              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Lincoln') { echo 'active'; } ?>" 
                href="sucursal_lincoln"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Lincoln
              </a>
              
              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Miguel Alemán') { echo 'active'; } ?>" 
                href="sucursal_linda_vista"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Miguel Alemán
              </a>

              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Online') { echo 'active'; } ?>" 
                href="sucursal_online"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Online
              </a>
              
              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal Pablo Livas') { echo 'active'; } ?>" 
                href="sucursal_pablo_livas"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal Pablo Livas
              </a>

              <a 
                class="dropdown-item <?php if ($titulo == 'Sucursal San Miguel') { echo 'active'; } ?>" 
                href="sucursal_san_miguel"
              >
                <i class="fa fa-angle-right mr-2"></i> Sucursal San Miguel
              </a>
            </div>
          </div>

          <a 
            href="contacto-vinculacion" 
            class="nav-item nav-link <?php if ($titulo == 'Empresas') { echo 'active'; } ?>"
          >
            Empresas
          </a>

          <a 
            href="promociones" 
            class="nav-item nav-link <?php if ($titulo == 'Promociones') { echo 'active'; } ?>"
          >
            Promociones
          </a>

          <a 
            href="contacto"                            
            class="nav-item nav-link <?php if ($titulo == 'Contacto') { echo 'active'; } ?>"
          >
            Contáctanos
          </a>

          <a 
            href="bolsa_trabajo"                            
            class="nav-item nav-link <?php if ($titulo == 'bolsa_trabajo') { echo 'active'; } ?>"
          >
            ¡Únete a nosotros!
          </a>

          <a 
            href="examen_ubicacion_pagina"  
            class="nav-item nav-link <?php if ($titulo == 'Examen de Ubicación') { echo 'active'; } ?>"
          >
            Examen ubicación
          </a>

          <a 
            href="https://inbi.mx/lms" 
            target="_blank" 
            class="nav-item nav-link <?php if ($titulo == 'Iniciar Sesión') { echo 'active'; } ?>"
          >
            Iniciar sesión
          </a>

          <a 
            href="https://qa.inbi.mx/procesarpago.php" 
            target="_blank" 
            class="nav-item nav-link"
          >
            Pago
          </a>

        </div>
      </div>
    </nav>
  </div>
</div>
