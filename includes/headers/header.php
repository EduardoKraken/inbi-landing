<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <title><?php echo $titulo; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="<?php echo $metadescription; ?>" name="description">

  <?php echo $canonical; ?>

  <!-- Favicon -->
  <link href="public/img/favicon.ico" rel="icon">


  <!-- Google Web Fonts -->
  <!-- <link rel="preconnect" href="https://fonts.gstatic.com"> -->
  <!-- <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">  -->

  <!-- Font Awesome -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">

  <!-- Libraries Stylesheet -->
  <link href="public/lib/owlcarousel/assets/owl.carousel.min.css?v=<?php echo(rand()); ?>" rel="stylesheet">

  <!-- Customized Bootstrap Stylesheet -->
  <link href="public/css/style.css?v=<?php echo(rand()); ?>" rel="stylesheet">

  <!-- STYLE CURSOS -->
  <link href="public/css/style_cursos.css?v=<?php echo(rand()); ?>" rel="stylesheet">

  <!-- GOOGE ANALYTICS -->
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PHJFCZG');</script>
  <!-- End Google Tag Manager -->

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171876901-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-171876901-1');
  </script>

	<meta name="facebook-domain-verify" content="qv2h1qsjio8s8rwqmc6tg7j003ki53"/>


  <?php
    header("Cache-Control: max-age=31536000");
  ?>
  

  <link rel="shortcut icon" href="public/images/logo.png">

  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '302732868233280');
    fbq('track', 'PageView');
  </script>

  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=302732868233280&ev=PageView&noscript=1"/>
  </noscript>

  <!-- End Facebook Pixel Code -->
</head>

<style>
  #btn-whatsapp {
    position: fixed;
    left: 20px;
    bottom: 20px;
  }
</style>
	

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHJFCZG"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->