<!-- Footer Start -->
<div class="container-fluid bg-gradiant_primary text-white mt-5 px-sm-3 px-md-5">
  <div class="row pt-4">
    <div class="col-lg-3 col-md-6 mb-3">
      <a href="index" class="navbar-brand" aria-label="Inicio de la página">
        <img src="public/img/logo_vertical_blanco.png" alt="" width="200" height="80">
      </a>
      <p>SÍguenos en nuestras redes sociales</p>
      <div class="d-flex justify-content-start text-white mt-4">
        <a 
          class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" 
          aria-label="Síguenos en twitter"
          style="width: 38px; height: 38px;" 
          target="_blank"
          href="https://twitter.com/inbimx"
        >
          <i class="fab fa-twitter"></i>
        </a>
        <a 
          class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" 
          aria-label="Síguenos en Facebook"
          style="width: 38px; height: 38px;" 
          href="https://www.facebook.com/INBImx/" 
          target="_blank"
        >
          <i class="fab fa-facebook-f"></i>
        </a>
        <a 
          class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" 
          aria-label="Envía un WhatsApp"
          style="width: 38px; height: 38px;" 
          href="https://wa.link/5pzw9k" 
          target="_blank"
        >
          <i class="fab fa-whatsapp"></i>
        </a>
        <a 
          class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" 
          aria-label="Síguenos en Instagram"
          style="width: 38px; height: 38px;" 
          href="https://www.instagram.com/inbimx/"
          target="_blank"
        >
          <i class="fab fa-instagram"></i>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 mb-3">
      <span class="font-weight-bold text-white mb-5">Cursos</span>
      <div class="d-flex flex-column justify-content-start mt-2">
        <a class="text-white mb-2" href="curso-kids"><i class="fa fa-angle-right  mr-2"></i>Kids (5 a 8 años)</a>
        <a class="text-white mb-2" href="curso-intensivo-teens"><i class="fa fa-angle-right  mr-2"></i>Teens ( 8 a 12 años)</a>
        <a class="text-white mb-2" href="curso-intensivo-adultos"><i class="fa fa-angle-right  mr-2"></i>Adultos (13 años en adelante) </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 mb-3">
      <span class="font-weight-bold text-white mb-5" href="blog">Blog</span>
      <div class="d-flex flex-column justify-content-start mt-2">
        <a class="text-white mb-2" href="ingles-en-ingenieria"><i class="fa fa-angle-right  mr-2"></i>Inglés en Ingenieria</a>
        <a class="text-white mb-2" href="practicar-ingles-usando-videojuegos"><i class="fa fa-angle-right  mr-2"></i>Practicar ingles usando videojuegos</a>
        <a class="text-white mb-2" href="aprender-ingles-gratis"><i class="fa fa-angle-right  mr-2"></i>Aprender inglés gratis</a>
        <a class="text-white mb-2" href="como-mejorar-nuestra-pronunciacion"><i class="fa fa-angle-right  mr-2"></i>Cómo mejorar nuestra pronunciación</a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 mb-3">
      <span class="font-weight-bold text-white mb-5">Contáctanos</span>
      <p class="text-white mt-2"><i class="fa fa-phone-alt mr-2"></i>+52 (81) 2047 4412</p>
      <p class="text-white"><i class="fa fa-envelope  mr-2"></i>informes@inbi.mx</p>
    </div>
  </div>
</div>

<img 
  class="btn-whatsapp" 
  src="public/img/whatsapp.webp" 
  width="64" 
  height="64" 
  alt="Whatsapp" 
  onclick="window.open('https://api.whatsapp.com/send?phone=+528120474412&text=%C2%A1Me%20gustar%C3%ADa%20saber%20m%C3%A1s%20sobre%20las%20clases%20de%20ingl%C3%A9s!')">

</body>

<!-- <a href='https://wa.link/5pzw9k' id="btn-whatsapp" target="_blank"><img src="public/images/whatsapp-logo.png" width="75"></a> -->
<!-- JavaScript Bundle with Popper -->
<!-- JavaScript Libraries -->
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.6.3.min.js"
  integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU="
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="public/lib/easing/easing.min.js"></script>
<script src="public/lib/waypoints/waypoints.min.js"></script>
<script src="public/lib/counterup/counterup.min.js"></script>
<script src="public/lib/owlcarousel/owl.carousel.min.js"></script>

<!-- Contact Javascript File -->
<script src="public/mail/jqBootstrapValidation.min.js"></script>
<script src="public/mail/contact.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- Template Javascript -->
<script src="public/js/main.js"></script>

<!-- Archivos JS por pagina -->
<?php
  switch ($titulo) {
    case 'La mejor escuela de inglés - Cursos para todas las edades - INBI':
       echo "<script src='public/js/contacto.js?v=2'></script>";
    break;

    case 'Contacto':
      echo "<script src='public/js/contacto.js?v=2'></script>";
    break;

    case 'Bolsa de Trabajo':
      echo "<script src='public/js/bolsa_trabajo.js?v=2'></script>";
    break;
  }
?>

</html>