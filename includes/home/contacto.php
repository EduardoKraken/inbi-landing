<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5">
  <div class="d-inline-flex text-white">
  </div>
</div>

<!-- Contact Start -->
<div class="container-fluid py-5">
  <div class="container">
    <div class="text-center">
      <h1 class="mt-2 mb-5">Envíanos tus datos para comunicarnos contigo</h1>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="d-flex align-items-center border mb-3 p-4">
          <i class="fas fa-2x fa-phone-alt text-primary mr-3"></i>
          <div class="d-flex flex-column">
            <h5 class="font-weight-bold">Llámanos</h5>
            <p class="m-0">
            <a href="tel:+8120474412">+52 (81) 2047 4412</a></p>
          </div>
        </div>

        <div class="d-flex align-items-center border mb-3 p-4">
          <i class="fa fa-2x fa-envelope-open text-primary mr-3"></i>
          <div class="d-flex flex-column">
            <h5 class="font-weight-bold">Correo</h5>
            <a href="mailto:informes@inbi.mx">informes@inbi.mx</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="contact-form">
          <form name="sentMessage" id="contactForm" novalidate="novalidate">
            <div class="form-row">
              <div class="col-md-6">
                <div class="control-group">
                  <input type="text" class="form-control p-4" id="floatingNombre" placeholder="Nombre" required="required" data-validation-required-message="Ingresa tu nombre" />
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="control-group">
                  <input  class="form-control p-4" id="floatingCelular" placeholder="Celular" required="required" data-validation-required-message="Ingresta tu celular" />
                  <p class="help-block text-danger"></p>
                </div>
              </div>
            </div>
            <div class="control-group">
              <textarea class="form-control" rows="5" id="floatingMensaje" placeholder="Message" required="required" data-validation-required-message="Ingresa un mensaje">Quiero información acerca del curso y las promociones vigentes.</textarea>
            </div>
            <div>
              <button class="btn btn-primary font-weight-semi-bold px-4 text-white" style="height: 50px;" type="submit" id="btn-enviar-datos">Enviar Mensaje</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
