<div class="py-5">
  <div class="row">
  <div class="col py-5">
      <div class="container-fluid py-5">
        <div class="container">
          <div class="row align-items-center pb-1">
            <div class="col-lg-7 mt-5 mt-lg-0">
              <h1 class="mt-2 mb-4 text-center">¡ÚNETE A NUESTRO EQUIPO!</h1>
              <div class=" border mb-3 p-4">
                <h3><u>Beneficios:</u></h3>
                <p>
                  <br>
                  • Beca 100% de Inglés para ti ó familiar
                  <br>
                  <br>
                  • Atención Psicológica
                  <br>
                  <br>
                  • Capacitación Constante
                  <br>
                  <br>
                  • Bonos por recomendación
                  <br>
                  <br>
                  • Estacionamiento gratuito
                  <br>
                  <br>
                  • Prestaciones de Ley (A excepción de Teachers)
                  <br>
                  <br>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="container-fluid py-4 ">
        <div class="col">
          <div class="card o-hidden border-2 shadow my-5">

            <div class="card-body p-4">
              <h5 class="text-center"><b>Envíanos tus datos para comunicarnos contigo</b></h5>
              <hr>
              <div class="row">
                <div class="col">
                  <div class="d-flex align-items-center border mb-3 p-4">
                    <i class="fas fa-2x fa-phone-alt text-primary mr-3"></i>
                    <div class="d-flex flex-column">
                      <h5 class="font-weight-bold">Llámanos</h5>
                      <p class="m-0">
                        <a href="tel:+8120474412">+52 (81) 8052 0919</a>
                      </p>
                    </div>
                  </div>

                  <div class="d-flex align-items-center border mb-3 p-4">
                    <i class="fa fa-2x fa-envelope-open text-primary mr-3"></i>
                    <div class="d-flex flex-column">
                      <h5 class="font-weight-bold">Correo</h5>
                      <a href="mailto:rh.schoolofenglish@gmail.com">rh.schoolofenglish@gmail.com</a></p>
                    </div>
                  </div>
                  <div >
                    <img class="img-thumbnail" src="public/img/estudiantes_tecnologia.webp" alt="" width="600px">
                  </div>
                </div>
                <div class="col-md-6">

                  <div class="form-floating mb-4">
                    <input type="text" class="form-control" id="floatingNombre" placeholder="Nombre">
                  </div>
                  <div class="form-floating mb-4">
                    <input type="text" class="form-control" id="floatingEdad" placeholder="Edad">
                  </div>
                  <div class="form-floating mb-4">
                    <input type="text" class="form-control" id="floatingCelular" placeholder="Celular">
                  </div>
                  <div class="form-floating mb-4">
                    <input type="email" class="form-control" id="floatingEmail" placeholder="Correo">
                  </div>
                  <div class="form-floating mb-4">
                    <select class="form-select" id="floatingVacante" aria-label="Vacante">
                      <option selected value="0">-Selecciona-</option>
                      <option value="1">Teacher</option>
                      <option value="2">Recepcionista</option>
                      <option value="3">Encargada de Sucursal</option>
                    </select>
                    <label for="floatingVacante">Vacante</label>
                  </div>
                  <div class="form-floating mb-4">
                    <select class="form-select" id="floatingSucursal" aria-label="Sucursal">
                      <option selected value="0">-Selecciona-</option>
                      <option value="1">Anáhuac</option>
                      <option value="2">Apodaca</option>
                      <option value="3">Casa Blanca</option>
                      <option value="4">Escobedo</option>
                      <option value="5">Fresnos</option>
                      <option value="6">Lincoln</option>
                      <option value="7">Miguel Alemán</option>
                      <option value="8">Online</option>
                      <option value="9">Pablo Livas</option>
                      <option value="10">San Miguel</option>
                    </select>
                    <label for="floatingSucursal">Sucursal</label>
                  </div>
                  <div class="form-group mb-4">
                    <label for="floatingCV">Adjuntar CV:</label>
                    <input type="file" class="form-control" id="floatingCV" accept="application/pdf">
                  </div>
                  <div class="form-group text-center">
                    <button class="btn btn-primary font-weight-semi-bold px-4 text-white" style="height: 50px;" type="submit" id="btn-enviar-datos-vacantes">Enviar Datos</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <div class="container-fluid  pl-5">
        <div class="row align-items-center pb-1">
          <div class="col-lg-7 mt-5 mt-lg-0">
            <div class=" border mb-3 p-4">
              <h3>Teacher</h3>
              <p>
                <u>Competencias y conocimientos:</u>
                <br>
                • Dominio Idioma Inglés
                <br>
                • Resolución de conflictos
                <br>
                • Manejo de grupo
                <br>
                - Manejo paquetería office
                <br>
                <br>
                <u>Actividades a realizar:</u>
                <br>
                • Impartir clases de acuerdo al plan académico
                <br>
                • Evaluar el desempeño de los alumnos
                <br>
                • Retroalimentación de los resultados académicos
                <br>
                <br>
                <u> Aptitudes:</u>
                <br>
                • Desenvuelto
                <br>
                • Honesto
                <br>
                • Proactivo
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="container-fluid  pl-5">
        <div class="row align-items-center pb-1">
          <div class="col-lg-7 mt-5 mt-lg-0">
            <div class=" border mb-3 p-4">
              <h3>Recepcionista</h3>
              <p>
                <u>Competencias y conocimientos:</u>
                <br>
                • Atención al cliente
                <br>
                • Seguimiento de llamadas vía telefónica
                <br>
                • Manejo de efectivo
                <br>
                • Manejo de paquete Office
                <br>
                <br>
                <u>Actividades a realizar:</u>
                <br>
                • Atención al cliente con los alumnos
                <br>
                • Recibir a los alumnos y darles la bienvenida
                <br>
                • Trabajo en equipo
                <br>
                • Mantener limpio el lugar de trabajo
                <br>
                • Cobranza (Efectivo y Terminal)
                <br>
                <br>
                <u> Aptitudes:</u>
                <br>
                • Paciencia
                <br>
                • Excelente presentación
                <br>
                • Organización
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="container-fluid  pl-5">
        <div class="row align-items-center pb-1">
          <div class="col-lg-7 mt-5 mt-lg-0">
            <div class=" border mb-3 p-4">
              <h3>Encargada de Sucursal</h3>
              <p>
                <u>Competencias y conocimientos:</u>
                <br>
                • Ventas (Vía telefónica y presencial)
                <br>
                • Manejo de personal
                <br>
                • Paquetería Office
                <br>
                • Manejo de efectivo
                <br>
                <br>
                <u>Actividades a realizar:</u>
                <br>
                • Ventas CRM o ERP
                <br>
                • Atención al cliente
                <br>
                • Logística de clases
                <br>
                • Cobranza (Efectivo y Terminal)
                <br>
                <br>
                <u> Aptitudes:</u>
                <br>
                • Liderazgo
                <br>
                • Excelente presentación
                <br>
                • Labor de venta
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<style>

.row {
display: flex;
justify-content: center;
}

</style>