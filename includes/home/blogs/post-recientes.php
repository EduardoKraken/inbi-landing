<div class="col-lg-4 mt-5 mt-lg-0">
	<!-- Search Form Start -->
	<div class="mb-5">
		<!-- Recent Post Start -->
		<div class="mb-5">
			<h3 class="font-weight-bold mb-4">Post Recientes</h3>
			<div class="d-flex mb-3">
				<img class="img-fluid" src="public/img/blogs/portada-identificar-tu-nivel-ingles.webp" style="width: 160px; height: 80px;" alt="">
				<div class="d-flex align-items-center border border-left-0 px-3" style="height: 80px;">
					<a class="text-secondary font-weight-semi-bold" href="como-identificar-tu-nivel-de-ingles">¿Cómo identificar tu nivel de inglés?</a>
				</div>
			</div>
			<div class="d-flex mb-3">
				<img class="img-fluid" src="public/img/blogs/portada-importancia-idiomas-mercado-laboral.webp" style="width: 160px; height: 80px;" alt="">
				<div class="d-flex align-items-center border border-left-0 px-3" style="height: 80px;">
					<a class="text-secondary font-weight-semi-bold" href="importancia-idiomas-mercado-laboral">La importancia de los idiomas en el mercado laboral</a>
				</div>
			</div>
			<div class="d-flex mb-3">
				<img class="img-fluid" src="public/img/blogs/portada-lectura-mejora-dominio-ingles.webp" style="width: 160px; height: 80px;" alt="">
				<div class="d-flex align-items-center border border-left-0 px-3" style="height: 80px;">
					<a class="text-secondary font-weight-semi-bold" href="lectura-mejora-dominio-ingles">Utilizar la lectura para mejorar nuestro dominio del inglés</a>
				</div>
			</div>
			<div class="d-flex mb-3">
				<img class="img-fluid" src="public/img/blogs/portada-aprender-ingles-no-siempre-tiene-por-que-ser-aburrido.webp" style="width: 160px; height: 80px;" alt="">
				<div class="d-flex align-items-center border border-left-0 px-3" style="height: 80px;">
					<a class="text-secondary font-weight-semi-bold" href="aprender-ingles-no-siempre-tiene-por-que-ser-aburrido">Aprender inglés no siempre tiene por qué ser aburrido.</a>
				</div>
			</div>
			<div class="d-flex mb-3">
				<img class="img-fluid" src="public/img/blogs/portada-presente-perfecto.webp" style="width: 160px; height: 80px;" alt="">
				<div class="d-flex align-items-center border border-left-0 px-3" style="height: 80px;">
					<a class="text-secondary font-weight-semi-bold" href="presente-perfecto">Presente perfecto</a>
				</div>
			</div>
			<div class="d-flex mb-3">
				<img class="img-fluid" src="public/img/blogs/portada-ingles-internet.webp" style="width: 160px; height: 80px;" alt="">
				<div class="d-flex align-items-center border border-left-0 px-3" style="height: 80px;">
					<a class="text-secondary font-weight-semi-bold" href="ingles-internet">El inglés y el internet.</a>
				</div>
			</div>
		</div>
		<!-- Plain Text End -->
	</div>
</div>