<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-ingles-para-ingenieria.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
          <h6 class="text-uppercase mt-2 mb-n2 text-white">Ene</h6>
          <h1 class="m-0 text-white">25</h1>
        </div>
      </div>
      <div class="pt-4 pb-2">
        <div class="d-flex mb-3">
          <div class="d-flex align-items-center ml-4">
            <i class="far fa-bookmark text-primary mr-2"></i>Inglés en Ingenieria
          </div>
        </div>
        <h2 class="font-weight-bold">Inglés en Ingenieria</h2>
      </div>

      <div class="mb-5">
        <p>
          En artículos anteriores te mencionamos que es importante aprender inglés. Normalmente se aprende para el trabajo o la escuela y en este post te hablaremos de cómo podemos utilizar el inglés dentro de la ingeniería. Si bien cada ingeniería tiene sus propios términos y son bastante diversas entre sí, tienen aspectos en común y serán los que comentaremos.
        </p>

        <h2 class="mb-4">¿Porque es importante?</h2>
        <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/ingles-en-ingenieria-2.webp" alt="Image">
        <p class="mt-4">
          Siempre se ha dicho que para un ingeniero es de vital importancia saber inglés, pero nunca mencionan el motivo. Algunos mencionan que, por el dinero, que si sabes inglés y eres ingeniero ganas más dinero; otros dicen que es porque viajas mucho y nunca sabes a quién podrías conocer en esos viajes. Existen muchas razones por las que un ingeniero debe saber y dominar el inglés, pero la que tiene mayor peso dentro de este mundo laboral es que, al ser un idioma que se habla mundialmente, es necesario que lo sepan para poder mantener conversaciones con compañeros, clientes o incluso el jefe o dueño de la empresa para la que trabajas.
          <br/>
          <br/>
          Una gran mayoría de oportunidades laborales que encuentra un ingeniero, son fuera del país, por lo que será necesario saber el inglés para poderse comunicar y realizar el trabajo de forma adecuada. Aunque la compañía para la que trabajes se encuentre en un país de habla hispana, es muy probable que vayas a necesitar platicar e intercambiar información con compañeros de otros países, con otras compañías que les dan productos o con compañías que quieran adquirir algún producto o servicio de ustedes.
          <br/>
          <br/>
          Además de esto, el inglés es utilizado para investigaciones y, aunque no sea de tu interés publicar en alguna revista o asistir a una conferencia, para poder comprender todo ese material de estudio, revistas, libros, investigaciones, etc., es necesario que utilices el inglés para que puedas aplicarlo dentro de tu trabajo. Muchas de estas investigaciones son expuestas en congresos o conferencias a la cual asisten millones de ingenieros interesados en el tema y puedes aprender mucho de estas investigaciones y mantener conversaciones con los demás ingenieros que puedan asistir.
        </p>

        <h3 class="mb-4">¿Cuáles son las claves del inglés para ingeniería?</h3>
        <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/ingles-en-ingenieria-3.webp" alt="Image">
        <p>
          Como mencionamos antes, cada ingeniería es diferente; sin embargo, comparten cosas y estos son algunos de los aspectos claves que debe dominar un ingeniero. Es necesario recordar que, aunque el inglés es igual para todos, los términos técnicos son muy diversos entre cada especialización y son los que generan las diferencias. Teniendo los siguientes aspectos en consideración, te será mucho más sencillo aprender el inglés para la rama en la que te quieras enfocar.
          <br/>
          <br/>
          El primero es dominar el inglés general. No se espera que lo dominen al 100%, pero si les será útil tener una base sólida del inglés antes de comenzar a enfocarse en términos específicos de la ingeniería. Una vez se tenga una base sólida y se conozcan los aspectos básicos, te puedes centrar en lo técnico o específico de tu especialidad.
          <br/>
          <br/>
          Estos términos específicos serán los que utilices con mayor frecuencia dentro de tu área; sin embargo, debes recordar que hay muchos conceptos y estructuras que será necesario que utilices para que los demás puedan comprender lo que expresas. Parte importante de la ingeniería son los números y las unidades de medida como centímetros, mililitros, etc. Recordemos que hay lugares que utilizan unidades como yardas, pulgadas, etc., por lo que es necesario conozcas el nombre en inglés.
          <br/>
          <br/>
          Dos partes importantes del trabajo de un ingeniero son explicar procesos y escribir reportes. Para explicar procesos es importante saber explicar de forma adecuada y sencilla ya que esto es considerado una habilidad muy valiosa. Para ello es necesario utilizar conectores temporales como “first” y “then”, pero también expresar horas o el tiempo que se requiere, las partes de un sistema, método o herramienta que son utilizados para que funcione de manera correcta y, especialmente, los resultados que se obtuvieron.
          <br/>
          <br/>
          Cada día se requieren reportes de muchos ingenieros en cualquier área y además de utilizar un lenguaje específico, debe ser claro y ordenado para que los demás puedan leerlo y comprenderlo. Para este tipo de reportes es necesario saber estructuras como el condicional cero, voz pasiva, entre otros, que nos permitan tener un reporte claro y comprensible. Dentro de estos reportes se deben utilizar todo lo aprendido como unidades de medición, vocabulario técnico, etc.
          <br/>
          <br/>
          Finalmente, una de las partes clave del inglés para ingeniería es dominar las presentaciones. Además de escribir los informes y reportes, es necesario presentar a los compañeros, al jefe e incluso al dueño, los resultados de la investigación, el funcionamiento de algún diseño o la forma en que se desarrolló un proyecto. Es necesario saber las estructuras adecuadas, así como la forma de explicar de forma sencilla pero comprensible. Desarrolla la confianza suficiente. No tengas miedo a equivocarte mientras aprendes. Esto te permitirá desarrollar las habilidades necesarias para realizar esto de forma más sencilla y crecer personalmente.
        </p>


        <h2 class="mb-4">Conclusiones</h2>
        <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/ingles-en-ingenieria-4.webp" alt="Image">
        <p>
          Como te puedes dar cuenta es bastante similar a aprender inglés general. La diferencia es que se utilizan los nombres de ciertas herramientas que son utilizadas día con día únicamente por un ingeniero y que cualquier persona ajena a este contenido posiblemente no comprenda.
          <br/>
          <br/>
          Siguiendo las claves del inglés para ingeniería te será muy sencillo destacar dentro de tu área. Sin importar cuál sea, siempre será recomendable establecer una base confiable antes de comenzar a enfocarte en lo tuyo. Debes tener especial cuidado con la fluidez, ya que no querrás mantener una conversación con un experto en un tema y quedarte pensando cuál es la palabra o cómo se pronuncia correctamente.
        </p>
      </div>
    </div>
    <!-- Comment Form End -->
    <!-- Blog Detail End -->

    <?php
      include 'post-recientes.php';
    ?>
  </div>
</div>
<!-- Detail End -->
