<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada_clima.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Oct.</h6>
					<h1 class="m-0 text-white">31</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary"></i>
	          <a class="text-muted ml-2" href="halloween">Hablar del clima sin frustrarte en el intento</a>
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Hablar del clima sin frustrarte en el intento</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	Todos nosotros aprendemos inglés con la esperanza de utilizarlo de forma cotidiana en algún punto de la vida ya sea en nuestro trabajo o mejor aun yendo de vacaciones a un lugar donde solo se hable inglés y podamos practicar mientras disfrutamos de un merecido periodo de descanso.
	      	<br/>
	      	<br/>
	      	Sin embargo, ya sea que usemos el idioma por trabajo o por placer la posibilidad de tener que expresarnos del clima es muy alta. Hablar del clima sirve como un rompe hielo cuando queremos conocer a alguien e incluso nos puede ayudar a evitar pasar horas en el trafico si es que sabemos que vendrá una lluvia muy fuerte. Así que en este artículo veremos palabras y expresiones que te serán muy útiles al momento de platicar acerca del clima.
	      </p>

	      <h2 class="mb-4">Describing the weather</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/Blog-32.webp" alt="Image">
	      <p>
	      	Estas expresiones te ayudaran a que describas las condiciones climáticas de una manera natural. Se pueden utilizar en conversaciones casuales o formales sin ningún problema.
	      	<br/>
	      	<br/>
					<b>How is the weather going to be? (¿Cómo va a estar el clima?):</b> Esta pregunta es esencial en cualquier persona que hable inglés, con esta pregunta estaremos directamente preguntando como serán las condiciones del clima. Today is getting colder (Hoy se pondrá más frio): Esta frase nos ayudara a tener precaución al momento de salir ya que se nos está informando que la temperatura bajara durante el día.
	      	<br/>
	      	<br/>
					<b>It´s a nice day (Es un día increíble):</b> Esta frase nos ayudara cada vez que queramos expresar que las condiciones del clima son óptimas. Cada vez que digamos o escuchemos esta idea sabremos que podemos estar tranquilos en cuanto al clima se refiere.
	      	<br/>
	      	<br/>
					<b>Careful, today is getting worse (Cuidado, hoy se pondrá peor):</b> Esta frase se usará cuando el clima es malo, pero hay pronóstico de que se ponga aun peor. Se usará para prevenir a una persona acerca de una condición climatológica.
					<br/>
	      	<br/>
					<b>It´s tipping down (Está lloviendo intensamente):</b> Esta expresión evidentemente se usa para indicarle a alguien que tome su paraguas porque lo va a necesitar.
					<br/>
	      	<br/>
					<b>Asking about the weather</b> Estas preguntas básicas que veremos a continuación serán tus mejores aliadas al momento de querer saber acerca del clima. Apréndelas ya que sin duda te ayudaran muchísimo sobre todo cuando recién llegas a vivir a algún lugar.
					<br/>
	      	<br/>
					<b>Is it raining/snowing over there? (¿Está lloviendo o nevando allá?):</b> Si vas a trasladarte a algún lugar esta pregunta es esencial ya que lo último que querrás es correr bajo la lluvia antes de una reunión.
					<br/>
	      	<br/>
					<b>What´s the weather like outside? (¿Cómo está el clima afuera?):</b> Si estas en la oficina y ocupas salir a comprar algo, esta pregunta te puede salvar de un desastre.

				</p>

	      <h3 class="mb-4">Kinds of weather</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/Blog-33.webp" alt="Image">
	      <p>
	      	Conocer estas 8 descripciones para el clima hará que puedas comunicarte de manera eficiente y sobre todo que puedas expresar la condición del clima de manera correcta lo cual la persona que te pregunta te lo agradecerá.
					<br/>
					<br/>
					<b>Sunny.-</b> Soleado
					<br/>
					<b>Partially cloudy.-</b> Parcialmente nublado
					<br/>
					<b>Cloudy.-</b> Nublado
					<br/>
					<b>Overcast.-</b> Lluvia ligera
					<br/>
					<b>Rainy.-</b> Lluvioso
					<br/>
					<b>Snowy.-</b> Con nieve
					<br/>
					<b>Stormy.-</b> Con tormentas
					<br/>
					<b>Foggy.-</b> Con neblina
					<br/>
					<br/>
					Con esta guía tendrás las herramientas necesarias para iniciar conversaciones en inglés acerca del clima. Nunca pierdas de vista que el idioma se trata de avanzar constantemente y nunca dejar de aprender. Si conoces alguna otra expresión que consideres útil en el idioma no dudes en compartírnosla, nos encantara escucharte.
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
