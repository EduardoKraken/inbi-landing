<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-importancia-idiomas-mercado-laboral.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Ago</h6>
					<h1 class="m-0 text-white">17</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>La importancia de los idiomas en el mercado laboral
	        </div>
	      </div>
	      <h2 class="font-weight-bold">La importancia de los idiomas en el mercado laboral</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	Hoy en día sin duda el mercado laboral está lleno de competencia. Muchas personas están hoy en día terminando su carrera y buscando una oportunidad de obtener un excelente empleo.
	      	<br/>
	      	<br/>
					Sin embargo, la competencia crece y crece cada día más, es por eso que es una buena idea tratar de diferenciarnos de los demás competidores. Al final del día, las empresas van a seleccionar a aquellas personas que estén más preparadas y que puedan desempeñar el trabajo con eficiencia.
					<br/>
	      	<br/>
					Y es aquí donde llegamos al elemento clave: la diferenciación. Para obtener un salario más alto, debemos distinguirnos de los demás competidores y ofrecer cosas que la mayoría no puede. A continuación, detallaremos elementos que pueden ayudarte a ser diferente y por ende tener mejores posibilidades de empleo.
	      </p>

	      <h2 class="mb-4">Posgrados</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/importancia-idiomas-mercado-laboral-1.webp" alt="Image">
	      <p>
	      	Supongamos que la empresa requiere a una persona que tenga un posgrado en alguna materia en específico. Generalmente, las empresas van a pagarle más a estas personas no porque sepan más, sino por el simple hecho de que, si estas personas decidieron no continuar trabajando en la empresa, sería muchísimo más complicado conseguir a alguien con esas características. Es así cómo se determinan los salarios
	      	<br/>
	      	<br/>
					Los posgrados son una excelente herramienta para poder obtener mejores salarios dentro del mercado laboral. Sin embargo, el inconveniente es que para poder llegar a ellos tenemos que terminar primero con nuestros estudios de licenciatura, y después concluir algún estudio de posgrado. Esto nos va a llevar como mínimo entre 5 a 7 años de poder realizar, y aunque es bastante eficiente, el tiempo también es un factor a considerar.
				</p>

	      <h3 class="mb-4">Idiomas</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/importancia-idiomas-mercado-laboral-2.webp" alt="Image">
	      <p>
	      	Supongamos que nosotros somos una empresa ubicada en un país donde se habla español como idioma natal y queremos contratar a una persona que nos puede ayudar incrementar nuestras ventas en el mismo país. Como empresa estaríamos dispuestos a pagar un salario que vaya acorde a lo que la persona nos puede proporcionar. En este caso el salario que la empresa, normalmente ofrecería, sería un salario no muy alto debido a que es fácil encontrar personas que hablen español. Es por ello que el salario no va a ser tan atractivo, debido a que puede ser sustituido con relativa facilidad.
	      	<br/>
	      	<br/>
					Por otro lado, supongamos que como empresa tenemos la necesidad de contratar a una persona que nos ayude a aumentar nuestras ventas no sólo en el país sino también en Estados Unidos, esa persona debe hablar inglés, en este caso el sueldo va a ser mayor que en el primer caso, debido a que es más complicado encontrar personas que hablen inglés de manera perfecta en un país. Entonces, la empresa va a tratar de incrementar el salario para retener a la persona el mayor tiempo posible.
					<br/>
	      	<br/>
					Esto también pasa con otros idiomas no solamente con el inglés, si la empresa vende a Brasil, el portugués va a ser muy bien valorado, si vende a China, evidentemente el chino es el que va a ser muy valorado. Al final del día, los idiomas no son más que un medio de diferenciación que adquirimos de una forma muy rápida y funcionan igual que los posgrados al momento de crear una distinción.
					<br/>
	      	<br/>
					Para resumir la información las empresas van a estar dispuestas a pagar una mayor cantidad de dinero a aquellas personas que le sea más complicado sustituir en dado caso que decidan ya no trabajar para ellos. Es por eso que estudiar idiomas, hacer posgrados y tomar cursos adicionales es una excelente manera de generar una diferenciación, y por ende obtener mejores oportunidades en el campo laboral
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->

