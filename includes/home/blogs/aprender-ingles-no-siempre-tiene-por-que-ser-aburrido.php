<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-aprender-ingles-no-siempre-tiene-por-que-ser-aburrido.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Sep</h6>
					<h1 class="m-0 text-white">07</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>Aprender inglés no siempre tiene por qué ser aburrido.
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Aprender inglés no siempre tiene por qué ser aburrido.</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	Recuerda que la mejor manera de aprender es divirtiéndote en el proceso, así que es muy importante utilizar herramientas multimedia para poder acostumbrarnos al idioma. En estos tiempos, estamos muy expuestos a diferentes materiales que están en inglés. Así que, es extremadamente sencillo poder encontrarlos. Hace años, para poder practicar el idioma mediante programas provenientes de Estados Unidos era necesario contar con televisión de cable, que regularmente eran muy cara o bien conseguir video cassettes que vinieran en su idioma de origen, lo cual era también bastante complicado.
	      	<br/>
	      	<br/>
					Para poder desarrollar nuestra habilidad de escuchar inglés, es muy importante que comencemos con series de acuerdo a nuestro nivel- Sabemos que tal vez, esa serie que te apasiona es con la que quieras aprender el idioma. Sin embargo, una mala técnica y una mala selección de la serie puede llevarte momentos de frustración que no es lo que estamos buscando en el aprendizaje.
					<br/>
	      	<br/>
					Recuerda no estamos diciendo que esa serie que tanto te gusta no sea buena simplemente estamos diciendo que tal vez no sea la mejor decisión para poder comenzar a practicar el escuchar el idioma en fases tempranas del aprendizaje
	      </p>

	      <h2 class="mb-4">Friends</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/aprender-ingles-no-siempre-tiene-por-que-ser-aburrido-2.webp" alt="Image">
	      <p>
	      	Esta es una de las series más emblemáticas al momento de aprender el idioma, debido a la simpleza con la que se expresan sus personajes. Esta serie transcurre en un barrio normal, con personas normales, hablando de cosas cotidianas. El ambiente perfecto para poder aprender un idioma
	      	<br/>
	      	<br/>
					Aunque tal vez estarás pensando que esta serie tiene algunos años, y que sus maneras de expresarse no van a ser tan actuales. Tienes razón, sin embargo, va a ser una excelente opción para poder aprender el inglés desde sus bases y a partir de ahí, poder acoplar el idioma a estos tiempos.
					<br/>
	      	<br/>
					Tal vez, el contra más grande de esta serie es su duración. Tiene una cantidad de capítulos que no a la mayoría les gusta ver, sin embargo, si tú eres un apasionado de las series que tienen muchos capítulos esta opción te va a encantar.
				</p>

	      <h3 class="mb-4">Los Simpsons</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/aprender-ingles-no-siempre-tiene-por-que-ser-aburrido-3.webp" alt="Image">
	      <p>
	      	Otra de las series consentidas del público. Sin duda, todo mundo conocemos a la familia más popular de todo Estados Unidos.
	      	<br/>
	      	<br/>
					Esta serie ofrece prácticamente lo mismo que la anterior, debemos advertir que la gran diferencia es el sentido con el que se dicen las cosas dentro de esta serie. Es bastante común escuchar chistes o doble sentido que harán que el espectador se ría dentro de cada uno de los capitulos.
					<br/>
	      	<br/>
					Sin lugar a dudas, esta es una de las mejores opciones para todos aquellos que además de aprender el idioma, quieren pasar un excelente rato riéndose de las ocurrencias de esta loca familia.
				</p>


				<h2 class="mb-4">How I met your mother</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/aprender-ingles-no-siempre-tiene-por-que-ser-aburrido-4.webp" alt="Image">
	      <p>
	      	Esta serie es muy parecida a Friends, transcurre con amigos muy normales que viven situaciones extremadamente cotidianas. Como ya te pudiste haber dado cuenta, este es una de las características que debemos buscar al momento de comenzar a practicar con el idioma.
	      	<br/>
	      	<br/>
					Estamos seguros de que pasaras un excelente momento viendo todas las aventuras que viven los personajes de esta serie. Además, de que podrás responder la pregunta que vivió dentro de la cabeza de tantas personas por muchos años, y fue la manera en la que el personaje principal conoció al amor de su vida.
				</p>

				<h3 class="mb-4">Que series no ver</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/aprender-ingles-no-siempre-tiene-por-que-ser-aburrido-5.webp" alt="Image">
	      <p>
	      	Por último, el mejor consejo que te podemos dar es alejarte por el momento de las series que utilizan lenguaje muy específico como Grey´s Anatomy donde prácticamente toda la serie utilizan vocabulario médico que es extremadamente comprender incluso en español o series policiales, donde van a utilizar términos de leyes que puede ser muy complicado comprender. Esto es un proceso y entre más sencillo se vuelva, más rápido podrás dominar el idioma y podrás ver tus series favoritas en su idioma de origen.
				</p>

	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
