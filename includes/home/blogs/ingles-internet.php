<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-ingles-internet.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Sep</h6>
					<h1 class="m-0 text-white">23</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>Utilizar la lectura para mejorar nuestro dominio del inglés
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Utilizar la lectura para mejorar nuestro dominio del inglés</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	Sin duda, las redes sociales han llegado para quedarse y en este post vamos a aprender palabras que harán que comprendas el inglés utilizado en el internet.
	      	<br/>
	      	<br/>
					No importa si eres una persona que ama pasar el tiempo en la web o eres de los que prefiere mantenerse alejado de los dispositivos electrónicos, no podemos negar que el internet y las redes sociales han llegado para quedarse y se convierten cada vez más en una herramienta indispensable en la vida de jóvenes y adultos.
					<br/>
	      	<br/>
					Comencemos con palabras que sin duda te ayudaran a que comprendas mucho mejor lo que pasa a tu alrededor.
	      </p>

	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/ingles-internet-1.webp" alt="Image">
	      <p>
	      	<b>• Surf:</b> Es el verbo que se utilizara para indicar que estas utilizando el internet “I am surfing the web” (Estoy navegando en la red).
	      	<br/>
					<b>• Drag and drop:</b> Esto se usará cuando un archivo se pueda arrastrar “You can drag the file and drop it there” (Puedes arrastrar el archivo y soltarlo ahí).
					<br/>
					<b>• Sign up:</b> Es el verbo que utilizaremos para registrarnos en una página por primera vez.
					<br/>
					<b>• Log in:</b> Es el verbo que usaremos para ingresar a una página en la que ya estábamos previamente registrados “You must log in everyday” (Debes ingresar todos los días).
					<br/>
					<b>• Homepage:</b> Significa literalmente página de inicio, la mayoría de las personas tiene “Google” como “Homepage”.
					<br/>
					<b>• Download:</b> Es el verbo que indica que vamos a descargar un archivo “I want to download that picture” (Quiero descargar esa imagen).
					<br/>
					<b>• Upload:</b> Este verbo se usará cuando queramos subir un archivo “You must upload your homework today” (Debes subir tu tarea hoy).
					<br/>
				</p>

	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/ingles-internet-2.webp" alt="Image">
	      <p>
	      	Las redes sociales también están llenas de palabras muy específicas que debes saber para que no tengas ningún problema usándolas. Recuerda que puedes aprender inglés cada dia, usando todo lo que tengas a la mano.
					<br/>
	      	<br/>
					<b>• Social network:</b> Este es el nombre de las redes sociales. Es un término que prácticamente todos conocen hoy en dia “I will get a social network account” (Tendré una cuenta en una red social).
					<br/>
					<b>• Share:</b> Es el verbo que se utiliza cuando se quiere compartir algo, puede ser una foto o un archivo “I will share a video” (Compartiré un video).
					<br/>
					<b>• Friend request:</b> Esta frase la veremos cuando una persona quiera ser nuestro amigo dentro de las redes sociales “She has a los of friend requests” (Ella tiene muchas solicitudes de amistad).
					<br/>
					<b>• Hashtag:</b> Literalmente significa “Etiqueta” y son utilizadas para seccionar el material dentro de las redes sociales “Her posts have a lot of hastags” (Sus posts tienen muchas etiquetas).
					<br/>
	      	<br/>
					El internet es hoy en dia un ambiente donde las personas pasan gran cantidad de tiempo, es por eso que se han creado palabras y contracciones que se utilizan solo en ese entorno. Generalmente son los jóvenes quienes crean estas conversaciones para poder comunicarse de una forma más discreta dentro de un entorno tan público como lo son las redes sociales. Veamos algunas de las más famosas y sus significados.
					<br/>
	      	<br/>
					<b>• Lmk:</b> “Let me know” se utiliza cuando queremos enterarnos de algún chisme.
					<br/>
					<b>• Brb:</b> “Be right back” esta contracción se utiliza cuando una persona va a dejar de contestar por algún tiempo.
					<br/>
					<b>• TBH:</b> “To be honest” usaremos esta expresión cuando queremos decir que algo es la verdad.
					<br/>
					<b>• Tl;dr:</b> “Too long, don´t read” esta expresión funciona cuando alguien nos manda un texto tan largo que nos da flojera leerlo.
					<br/>
	      	<br/>
					Como ya pudiste darte cuenta, el internet es un espacio que nos permite aprender muchas cosas que nos servirán para comunicarnos con más personas día con día. Como mencionamos al principio, no debes tenerle miedo a la red. Por el contrario, debes usarla como una herramienta para practicar el idioma día con día.
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
