<!-- BLOG -->
<div class="section layout_padding padding_bottom-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="full">
					<div class="text-center">
						<h1 class="mt-2 mb-5">Conoce todas las noticias del mundo en inglés</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div id="demo" class="carousel slide" data-ride="carousel">

					<div class="carousel-inner">
						
						<!-- 1ER SLICER -->
						<div class="carousel-item active">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada_halloween.webp" alt="portada halloween">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="halloween">Halloween</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="halloween">El otoño llegó y trae consigo una de las más queridas festividades en la cultura norteamericana, nos estamos refiriendo a “Halloween” o “Noche de brujas”. </a>
									</div>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada_clima.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="hablar-del-clima">Hablar del clima sin frustrarte en el intento</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="hablar-del-clima">Todos nosotros aprendemos inglés con la esperanza de utilizarlo de forma cotidiana en algún punto de la vida ya sea en nuestro trabajo...</a>
									</div>
								</div>
							</div>
						</div>

						<!-- 2DO SLICER -->
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada_cancer_mama.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="cancer-mama">Cáncer de mama alrededor del mundo “Breast cancer worldwide”</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="cancer-mama">Lo que más disfrutamos de lo que hacemos, es impactar la vida de las personas mediante el conocimiento.</a>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada_videojuegos.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="practicar-ingles-usando-videojuegos">Practicar inglés usando videojuegos</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="practicar-ingles-usando-videojuegos">Así es, tal como lo leíste. Jugar videojuegos es una excelente manera de practicar el idioma. Hoy vivimos en un mundo globalizado donde el acceso a los mejores juegos es casi instantáneo y los podemos usar no solo con fines recreativos.</a>
									</div>
								</div>
							</div>
						</div>


						<!-- 3ERO SLICER -->
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-ingles-para-ingenieria.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="ingles-en-ingenieria">Inglés en Ingenieria</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="ingles-en-ingenieria">En artículos anteriores te mencionamos que es importante aprender inglés. Normalmente se aprende para el trabajo o la escuela y en este post te hablaremos de cómo podemos utilizar el inglés dentro de la ingeniería.</a>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-ingles-gratis.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="aprender-ingles-gratis">Aprender inglés Gratis</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="aprender-ingles-gratis">En este artículo vamos a hablar de los pros y contras de aprender inglés gratis. Sabemos que pensaras, está escrito por una escuela así que me dirán que es malo, sin embargo, seremos muy objetivos para poderte ofrecer un...</a>
									</div>
								</div>
							</div>
						</div>

						<!-- 4TO SLICER -->
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-pronunciacion.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="como-mejorar-nuestra-pronunciacion">¿Cómo mejorar nuestra pronunciación?</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="como-mejorar-nuestra-pronunciacion">
											En el siguiente artículo te hablaremos sobre como mejorar y desarrollar tus habilidades para pronunciar de manera correcta el inglés.
										</a>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-identificar-tu-nivel-ingles.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="como-identificar-tu-nivel-de-ingles">¿Cómo identificar tu nivel de inglés?</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="como-identificar-tu-nivel-de-ingles">
											Para poder indetificar tu nivel de inglés, te invitamos a ller el siguiente artículo, que habla sobre como saber en que nivel de inglés te ubicas
										</a>
									</div>
								</div>
							</div>
						</div>



						<!-- 5to SLICER -->
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-importancia-idiomas-mercado-laboral.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="importancia-idiomas-mercado-laboral">La importancia de los idiomas en el mercado laboral</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="importancia-idiomas-mercado-laboral">
											Hoy en día sin duda el mercado laboral está lleno de competencia. Muchas personas están hoy en día terminando su carrera y buscando una oportunidad de obtener un excelente empleo.
										</a>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-lectura-mejora-dominio-ingles.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="lectura-mejora-dominio-ingles">Utilizar la lectura para mejorar nuestro dominio del inglés</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="lectura-mejora-dominio-ingles">
											Existen diferentes maneras en la que puedes mejorar tu dominio del idioma, sin embargo, la mayoría no nos sentimos del todo cómodos con la idea de incluir la lectura dentro de nuestra práctica diaria.
										</a>
									</div>
								</div>
							</div>
						</div>


						<!-- 6TO SLICER -->
						<div class="carousel-item">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-aprender-ingles-no-siempre-tiene-por-que-ser-aburrido.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="aprender-ingles-no-siempre-tiene-por-que-ser-aburrido">Aprender inglés no siempre tiene por qué ser aburrido.</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="aprender-ingles-no-siempre-tiene-por-que-ser-aburrido">
											Aprender inglés no siempre tiene por qué ser aburrido. El día de hoy vamos a recomendarte tres series con las cuales podrás practicar el idioma y comenzar a mejorar la forma en la que escuchas el inglés.
										</a>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-presente-perfecto.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="presente-perfecto">Presente perfecto</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="presente-perfecto">
											El día de hoy hablaremos acerca uno de los temas más confusos al momento de aprender el idioma inglés. Estamos hablando del presente perfecto.
										</a>
									</div>
								</div>
							</div>
						</div>


						<!-- 7MO SLICER -->
						<div class="carousel-item">
							<div class="row">

								<!-- Inicia un blog -->
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="position-relative">
										<img class="img-fluid w-100" src="public/img/blogs/portada-ingles-internet.webp" alt="portada del blog">
									</div>
									<div class="border border-top-0" style="padding: 30px;">
										<div class="d-flex mb-3">
											<div class="d-flex align-items-center ml-4">
												<i class="far fa-bookmark text-primary"></i>
												<a class="text-muted ml-2" href="ingles-internet">El inglés y el internet.</a>
											</div>
										</div>
										<a class="h5 font-weight-bold" href="ingles-internet">
											Sin duda, las redes sociales han llegado para quedarse y en este post vamos a aprender palabras que harán que comprendas el inglés utilizado en el internet.
										</a>
									</div>
								</div>
							</div>
						</div>

					</div>

					<!-- Left and right controls -->
					<a class="carousel-control-prev cursos" href="#demo" data-slide="prev" aria-label="Siguiente slice">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next cursos" href="#demo" data-slide="next" aria-label="Slice anterior">
						<span class="carousel-control-next-icon"></span>
					</a>

				</div>
			</div>

		</div>        
	</div>
<!-- FIN DEL BLOG -->