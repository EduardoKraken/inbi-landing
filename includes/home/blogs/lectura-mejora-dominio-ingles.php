<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-lectura-mejora-dominio-ingles.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Ago</h6>
					<h1 class="m-0 text-white">18</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>Utilizar la lectura para mejorar nuestro dominio del inglés
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Utilizar la lectura para mejorar nuestro dominio del inglés</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	Existen diferentes maneras en la que puedes mejorar tu dominio del idioma, sin embargo, la mayoría no nos sentimos del todo cómodos con la idea de incluir la lectura dentro de nuestra práctica diaria.
	      	<br/>
	      	<br/>
					Cuando una persona quiere aprender inglés, lo primero que quiere hacer es: hablar, hablar y hablar. Y es por eso, que enfoca todos sus esfuerzos a lograrlo de manera correcta. Sin embargo, para poder hablar necesitamos tener unas bases fuertes, y esto nos lo puede ofrecer una lectura constante, sin embargo, existen algunos errores que debemos evitar al momento de practicar el idioma. En este momento te daremos algunos consejos para que puedas mejorar tu manera de expresarte en inglés a través de la lectura .
	      </p>

	      <h2 class="mb-4">1.- Elige libros que te gusten</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/lectura-mejora-dominio-ingles-1.webp" alt="Image">
	      <p>
	      	Tal vez el principal error al momento de querer aprender inglés mediante los libros, es empezar a leer un libro de gramática. La mayoría de los alumnos no les gusta el aprendizaje del idioma mediante la gramática, y eso no es malo, pero debemos tener en cuenta que la gramática avanzada no es para todos y no por que no puedan aprenderla sino porque difícilmente va a ser utilizada en la vida diaria.
	      	<br/>
	      	<br/>
					Es por ello que te recomendamos que elijas un libro que te guste, y puedas practicar la lectura poco a poco. Evidentemente, al principio va a haber palabras que no conozcas, sin embargo, es muchísimo más divertido encontrar palabras para descifrar algo que a nosotros nos gusta, a simplemente descifrar palabras por tener que hacerlo para entender una estructura. Así que inténtalo, elige un libro que te llame la atención y comienza a leerlo.
				</p>

	      <h3 class="mb-4">2.- Utiliza tu celular</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/lectura-mejora-dominio-ingles-2.webp" alt="Image">
	      <p>
	      	Una de las principales desventajas para poder leer es el hecho de que tenemos que estar cargando siempre un libro con nosotros, afortunadamente hoy en día con la tecnología, esto ya no tiene por qué ser así
	      	<br/>
	      	<br/>
					Los celulares se han convertido en un aliado muy importante en nuestra vida diaria, así que podemos descargar un libro y podemos leerlo en pequeños pasajes durante nuestros tiempos libres.
					<br/>
	      	<br/>
					Sé, de antemano, que hay algunas personas que van a decir que esto no es leer, que para leer se necesita un libro de verdad, y necesitan de esa sensación de hojear un libro. Sin embargo, existe la opción de utilizar la tecnología, y ya quedará en cada quien si hacer uso de ella o no.
				</p>


				<h2 class="mb-4">Elige libros acordes a tu nivel</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/lectura-mejora-dominio-ingles-3.webp" alt="Image">
	      <p>
	      	Los abuelos tenían un dicho que decía “Camina antes de correr” y esto también aplica para todo en la vida, incluso los idiomas.
	      	<br/>
	      	<br/>
					Otro de los errores más comunes, que cometen las personas al momento de querer practicar el idioma es elegir un libro que utiliza términos complejos. Esto tal vez sea muy bueno, una vez que tengamos cierto nivel de dominio. Pero al inicio, esto nos puede confundir más de lo que nos va ayudar.
					<br/>
	      	<br/>
					Es muy importante elegir un libro que toque temas cotidianos, donde se utilice el idioma de una manera sencilla para poder nosotros ir avanzando poco a poco. Eventualmente, vamos a tener el nivel para poder descifrar o para poder comprender libros de un mayor nivel. Pero como ya lo mencionamos, es muy recomendable comenzar con algo más acorde a nuestra situación inicial.
					<br/>
	      	<br/>
					Cómo te pudiste dar cuenta leer también es un ejercicio que nos puede ayudar a incrementar nuestro dominio en el idioma. Elige un libro, práctica y verás cómo poco a poco vas a ir mejorando tus estructuras. Y en menos tiempo de lo que crees, vas a estar hablando el idioma de una manera perfecta.
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
