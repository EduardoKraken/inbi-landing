<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada_videojuegos.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
          <h6 class="text-uppercase mt-2 mb-n2 text-white">Ene.</h6>
          <h1 class="m-0 text-white">28</h1>
        </div>
      </div>
      <div class="pt-4 pb-2">
        <div class="d-flex mb-3">
          <div class="d-flex align-items-center ml-4">
            <i class="far fa-bookmark text-primary mr-2"></i>Practicar inglés usando videojuegos
          </div>
        </div>
        <h2 class="font-weight-bold">Practicar inglés usando videojuegos</h2>
      </div>

      <div class="mb-5">
        <p>
          Así es, tal como lo leíste. Jugar videojuegos es una excelente manera de practicar el idioma. Hoy vivimos en un mundo globalizado donde el acceso a los mejores juegos es casi instantáneo y los podemos usar no solo con fines recreativos, ya que además de ser muy divertido pasar el tiempo jugando videojuegos podrías aprovechar esa oportunidad y mejorar tu inglés mientras juegas.
        </p>

        <h2 class="mb-4">Juegos de Rol</h2>
        <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/practicar-ingles-usando-videojuegos-2.webp" alt="Image">
        <p>
          Estos juegos destacan por tener el control total de lo que le pasa a tu personaje y son geniales para practicar el idioma. Estos juegos destacan por tener una amplia historia que esta regularmente llena de interacciones entre personajes claves de la historia. Estos juegos te obligan a entender lo que se está diciendo ya que un pequeño detalle podría ser la pista que estás buscando para seguir avanzando en tu aventura. Quien no recuerda al mítico Zelda o Final Fantasy, juegos que además de legendarios nos obligaron a entender que es lo que se decía en cada una de sus escenas.
        </p>

        <h3 class="mb-4">Juegos de deportes</h3>
        <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/practicar-ingles-usando-videojuegos-3.webp" alt="Image">
        <p>
          Este tipo de juegos por lo general están más enfocados en la acción que en desarrollar una historia sin embargo también podemos sacar provecho de ellos. Regularmente estos juegos están plagados de narradores y comentaristas que suelen llenar nuestros juegos de diálogos que relatan lo que está pasando. Aunque es cierto que estos diálogos llegan a ser repetitivos en cierto punto, esto no quiere decir que sean malos, ya que al estar expuesto a tantas repeticiones de las mismas frases terminan por grabarse en nuestra memoria con mayor eficiencia. Juegos como FIFA, Madden o NBA2K21 son excelentes exponentes de este género.
        </p>


        <h2 class="mb-4">Juegos Online</h2>
        <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/practicar-ingles-usando-videojuegos-4.webp" alt="Image">
        <p>
          Sin duda los reyes de la práctica del idioma, en estos tiempos jugar en línea es algo tan común que prácticamente ya no recordamos los videojuegos antes de esta opción. Estos juegos nos dan la oportunidad de conectarnos con personas literalmente de todo el mundo y jugar con ellos en tiempo real. Es una excelente oportunidad para practicar lo que sabes del idioma. Saber inglés te permitirá mejorar de una manera increíble ya que al poder participar con equipos de países donde se habla inglés hace que puedas aprender nuevas técnicas o practicar tus habilidades con jugadores totalmente distintos a ti. Juegos como. League of legends o Warzone son increíbles ejemplos de esta categoría.
        </p>

        <h3 class="mb-4">Juegos con una historia casi cinematográfica</h3>
        <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/practicar-ingles-usando-videojuegos-5.webp" alt="Image">
        <p>
          Hoy los videojuegos tienen historias que se pueden comparar incluso con películas y eso lo podemos aprovechar para practicar nuestro “Listening”, cuantos de nosotros no nos hemos emocionado con historias de juegos como “The last of us” o los gráficos adictivos de “God of war”. Sin duda este tipo de juegos nos ofrecen horas de diversión y de práctica del idioma.
          <br/>
          <br/>
          Sin duda los videojuegos hoy en día no solo son utilizados para pasar un buen rato de diversión sino también pueden ser usados para practicar el idioma y ser mejores cada día.
        </p>
      </div>
    </div>
    <!-- Comment Form End -->
    <!-- Blog Detail End -->

    <?php
      include 'post-recientes.php';
    ?>
  </div>
</div>
<!-- Detail End -->
