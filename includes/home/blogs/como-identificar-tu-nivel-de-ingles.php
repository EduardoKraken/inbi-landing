<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-identificar-tu-nivel-ingles.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
          <h6 class="text-uppercase mt-2 mb-n2 text-white">Ago</h6>
          <h1 class="m-0 text-white">11</h1>
        </div>
      </div>
      <div class="pt-4 pb-2">
        <div class="d-flex mb-3">
          <div class="d-flex align-items-center ml-4">
            <i class="far fa-bookmark text-primary mr-2"></i>¿Cómo identificar tu nivel de inglés?
          </div>
        </div>
        <h2 class="font-weight-bold">¿Cómo identificar tu nivel de inglés?</h2>
      </div>

      <div class="mb-5">
        <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/como-identificar-nivel-ingles-1.webp" alt="Image">
        <p>
          Normalmente, cuando una persona quiere aprender inglés lo hace pensando en poder sacar beneficio de esta herramienta en un futuro. Cuando tomamos la decisión de aprender el idioma lo hacemos con un objetivo bastante claro, este objetivo puede ser académico o laboral. Ya que tenemos en mente irnos de intercambio, trabajar para una empresa o hacer negocios con alguna compañía en el extranjero
          <br/>
          <br/>
          Ya una vez que tomamos la decisión y estamos estudiando el idioma nos encontramos con la pregunta universal. ¿Cuál es mi nivel de inglés? ¿Realmente se puede medir? y eso es lo que veremos el día de hoy en este artículo.
        </p>

        <h3 class="mb-4">¿Por dónde empezar?</h3>
        <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/como-identificar-nivel-ingles-2.webp" alt="Image">
        <p>
          Lo primero que debemos tener claro es que los idiomas no son matemáticas. Así que, no pueden ser medidos tan fácilmente. 3Si, por ejemplo, yo les preguntará ¿Cuál es su nivel de español? probablemente ustedes que están leyendo esto van a pensar un número distinto, ya que cómo se dan cuenta los idiomas no tienen un valor numérico de referencia que le podamos asignar.
          <br/>
          <br/>
          Es por eso que te damos una recomendación. Al momento de ir a una empresa a pedir trabajo, no pongas que tú sabes el 70% el 80% o el 90% del idioma ya que se van a dar cuenta que no tenemos una idea clara de cómo se debe expresar el nivel en los idiomas, y puede mermar nuestras oportunidades de conseguir el empleo.
          <br/>
          <br/>
          Este problema no es nuevo y para solucionarlo se creó algo que se llama marco de referencia europeo. Estos parámetros nos indican de una manera muy clara cuáles son las habilidades que tiene una persona en determinado idioma. Todos los idiomas pueden ser medidos mediante esta métrica y vamos a aprender qué significa cada uno de los niveles, para que podamos determinar dónde estamos y cómo vamos a mejorar.
        </p>


        <h2 class="mb-4">A1</h2>
        <p>
          Estar en este nivel significa que desconocemos el idioma sino en su totalidad, si la mayoría del mismo. Normalmente, este nivel lo van a tener personas que nunca han estado expuestas a tal idioma. Y por ende, todo es nuevo para ellos. Generalmente, a este tipo de persona se le conoce como principiantes.
          <br/>
          <br/>
          Para las personas que están en este nivel es muy difícil mantener una conversación fluida. Si acaso entienden solamente ciertas palabras, y tienen problemas importantes al momento de tratar de generar ideas en el idioma.
        </p>

        <h2 class="mb-4">A2</h2>
        <p>
          Las personas que están en este nivel, están un poco más acostumbradas a las palabras del idioma por aprender. La persona es capaz de mantener conversaciones de temas simples y con respuestas cortas.
          <br/>
          <br/>
          Estando en este nivel, aún nos falta un gran recorrido para poder considerarnos expertos en el idioma. Sin embargo, ya somos capaces de comprender ciertas situaciones y de expresar lo que pensamos de manera simple.
        </p>


        <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/como-identificar-nivel-ingles-3.webp" alt="Image">
        <h3 class="mb-4">B1</h3>
        <p>
          Este nivel ya es considerado como un nivel intermedio. Las personas que están en este rango, ya son capaces de comprender temas un poco más complicados con situaciones espontáneas. Por ejemplo, mantener una conversación con una persona en la calle acerca de diversos temas.
          <br/>
          <br/>
          Hablar del clima, de nuestros planes al futuro o nuestro día a día ya va a ser posible cuando contamos con este nivel de inglés
        </p>

        <h3 class="mb-4">B2</h3>
        <p>
          En este nivel la persona ya tiene un conocimiento bastante importante del idioma, es capaz de generar conversaciones más complejas y de entender conceptos abstractos.
          <br/>
          <br/>
          En este nivel, la persona comienza a entender diferentes acentos del idioma. Ya es capaz de hablar con personas que tengan un tono de voz agudo o grave sin mayor problema.
          <br/>
          <br/>
          Al estar en este rango ya es posible determinar cuándo hablar de manera formal y cuando hablar de manera coloquial, ya que su vocabulario se ha incrementado de forma muy importante. En este punto la persona se puede considerar bilingüe, ya que sí es cierto que no domina el idioma con excelencia en cuanto a temas gramaticales se refiere, ya se es capaz de tener cualquier conversación sin mayor inconveniente.
        </p>


        <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/como-identificar-nivel-ingles-4.webp" alt="Image">
        <h3 class="mb-4">C1</h3>
        <p>
          En este nivel la persona ya puede considerarse avanzada en el uso del idioma. Es capaz de entender estructuras complejas y desenvolverse sin mayor problema en un ambiente con vocabulario especializado.
          <br/>
          <br/>
          Este es el nivel que normalmente piden las universidades al momento de aceptar un intercambio, ya que como bien sabemos, dentro de las aulas se utilizara vocabulario complejo y muy específico de cierto tema. A este nivel deben aspirar las personas que están interesadas de ejercer una profesión en el idioma inglés.
        </p>

        <h3 class="mb-4">C2</h3>
        <p>
          Por último, tenemos este nivel, en el cual la persona ya tiene un uso perfecto del idioma. Este nivel es sólo recomendado para aquellas personas que quieran hacer del inglés su estilo de vida. Generalmente cuando una persona quiere ser traductora, cuando quieres ser intérprete o cuando se dedica a impartir cátedra del idioma en alguna universidad de filosofía o algo por el estilo, es cuando se tiene una necesidad real de llegar hasta este nivel en el idioma.
        </p>
      </div>
    </div>
    <!-- Comment Form End -->
    <!-- Blog Detail End -->

    <?php
    include 'post-recientes.php';
    ?>
  </div>
</div>
<!-- Detail End -->
