<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada_halloween.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Oct.</h6>
					<h1 class="m-0 text-white">31</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>Halloween
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Halloween</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	El otoño llegó y trae consigo una de las más queridas festividades en la cultura norteamericana, nos estamos refiriendo a “Halloween” o “Noche de brujas”. Sin duda todos conocemos esta tradición en la cual los niños se disfrazan y salen a la calle a pedir dulces mientras gritan “Noche de brujas, Halloween”. Conocemos la tradición, pero ¿Cuál es su origen?, en este artículo hablaremos del nacimiento de una de las tradiciones más memorables de nuestros tiempos.
	      </p>

	      <h2 class="mb-4">El inicio (The beginning)</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/Blog-35.webp" alt="Image">
	      <p>
	      	We know that Halloween takes place on the last day of October. The word itself has an incredible meaning which is “Hallowed evening”. All Hallows´ Eve and All Saints´ Day both are intended to honor dead people. As the years passed by the name was shortened to “Halloween” and it remains like this even today.
	      	<br/>
	      	<br/>
					Sabemos que Halloween tiene lugar el ultimo día de octubre. La palabra por si misma tiene un significado increíble que significa “La noche de los santos”. “La noche de los santos” y el “Día de muertos” son días cuya función es honrar a los muertos. Conforme los años asaron el nombre se fue acortando, hasta quedar en “Halloween” como lo conocemos actualmente.
	      	<br/>
	      	<br/>
					Many years ago there was a holiday that involved a lot of ceremonies to connect spirits, and there was one ceremony which was really famous, the one held by the Celts. It is said that the Celts celebrated wearing costumes that had animal shapes in order to protect them against ghosts. This is the origin of the Halloween costumes.
	      	<br/>
	      	<br/>
					Hace muchos años, había una fecha especial que involucraba muchas ceremonias para conectar con los espíritus y había una ceremonia que era muy famosa, la que tenían los Celtas. Se dice que los Celtas se vestían con disfraces con forma de animales para protegerlos de los espíritus. Este es el origen de los disfraces de Halloween
				</p>

	      <h3 class="mb-4">Trick or treat (Dulce o truco)</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/Blog-37.webp" alt="Image">
	      <p>
	      	It is also believed that the Celts prepared a big feast in honor of the spirits. They believed that if they gave them food, they wouldn´t take any action against them. As you could imagine, this is the origin of the “Giving candies” tradition. As years went by, people didn´t do big feasts anymore, and the food was changed by candies. When a family gives a candy to a kid, the kid promises not to take any action against them (Pranks).
					<br/>
	      	<br/>
					También se cree que los Celtas preparaban un gran festín en honor a los espíritus. Ellos creían que, si le daban comida a los espíritus, estos no tomarían ninguna acción contra ellos. Como te lo puedes imaginar, este fue el origen de la tradición de dar dulces a los niños. Conforme los años fueron pasando, la gente ya no hizo festines y la comida fue intercambiada por dulces. Cuando una familia da un dulce a un niño, el niño promete no hacer nada en contra de la familia (Bromas)
				</p>


				<h2 class="mb-4">The celebration (La celebración)</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/Blog-35.webp" alt="Image">
	      <p>
	      	Halloween is a world-wide known celebration, it is especially popular in the U.S and many other countries from America. This celebration is often surrounded by different opinions due to its “Evil origin”. The truth is that if you feel comfortable celebrating Halloween take your best disguise and prepare yourself for a night full of candies and laughs everywhere.
	      	<br/>
	      	<br/>
					Halloween es una celebración conocida mundialmente, es especialmente popular en los Estados Unidos y muchos otros países de América. Esta celebración esta normalmente rodeada por diferentes opiniones debido a su “Origen maligno”. A decir verdad, si te sientes cómodo o cómoda celebrando Halloween, ponte tu mejor disfraz y prepárate para una noche llena de dulces y risas por todos lados.
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
