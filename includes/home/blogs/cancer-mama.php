<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada_cancer_mama.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Oct.</h6>
					<h1 class="m-0 text-white">14</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>Cáncer de mama alrededor del mundo “Breast cancer worldwide”
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Cáncer de mama alrededor del mundo “Breast cancer worldwide”</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	Lo que más disfrutamos de lo que hacemos, es impactar la vida de las personas mediante el conocimiento, ver a nuestros alumnos cumplir sus metas y alcanzar sus objetivos es lo que nos motiva a seguir adelante día con día. Sin embargo, existe algo más importante para nosotros que el conocimiento de nuestros alumnos y eso es la salud. Es por eso, que nos sumaremos a los esfuerzos para luchar contra el cáncer de mama, a nuestro estilo, pero siempre con el objetivo claro de aportar nuestro granito de arena para evitar que cualquier mujer sufra de esta enfermedad.
					<br/>
					<br/>
					Comencemos por decir que en inglés la palabra enfermedad se dice “DIsease”, y el cáncer de mama (Breast cáncer) es lamentablemente una de las “Most common diseases” en mujeres alrededor del mundo. Es una enfermedad que se presenta solo en mujeres, y las cifras que se están alcanzando en el mundo son preocupantes. Existen diferentes acciones que ayudaran a evitar que se adquiera la enfermedad o si ya se adquirió poder erradicarla con eficiencia. 
				</p>

	      <h2 class="mb-4">¿Qué tan grande es el problema? “How big is the problem?”</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/Blog-28.webp" alt="Image">
	      <p>
	      	Se estima que “About 2.3 million women” fueron diagnosticadas con cáncer de mama durante el 2020, situándose como la causa principal de mortalidad. Esta es una enfermedad que no respeta “Ages” ya que puede afectar a cualquier mujer desde que inicia la pubertad hasta la vejez, aunque debemos decir que “Breast cancer risk doubles each decade until the menopause. “(El riesgo de sufrir cáncer de mama se duplica cada 10 años hasta alcanzar la menopausia)
					<br/>
					<br/>
					Se debe mencionar que la “Disease” no es exclusiva de las mujeres estrictamente hablando, aunque para ser realistas se tienen estimaciones de que solo el .05% de los casos de “Breast cancer” se presenta en varones, lo cual lo convierte en una enfermedad extraordinariamente rara en este género.
					<br/>
					<br/>
					Para poder combatir el problema se deben tener en consideración diferentes cosas que aportar a reducir las posibilidades de contraer la enfermedad o en su defecto reducir las probabilidades de que se convierta en una enfermedad mortal.
				</p>

	      <h3 class="mb-4">¿Cómo puedo evitar contraer cáncer de mama? “How do we avoid getting breast cancer?”</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/Blog-29.webp" alt="Image">
	      <p>
	      	•	Undertaking vigorous physical activity DECREASES the risk of premenopausal breast cancer (Hacer actividad fisica vigorosa reduce el riesgo de contraer cáncer de mama antes de la menopausia)
					<br/>
					•	Breastfeeding DECREASES the risk of breast cancer in the mother. (Amamantar reduce el riesgo de contraer cáncer de mama en la madre)
					<br/>
					•	Avoid drinking alcoholic beverages DECREASES the risk of premenopausal breast cancer (Evitar ingerir bebidas alcohólicas reduce el riesgo de contraer cáncer de mama)
					<br/>
					<br/>
					Como podemos observar, llevar un estilo de vida sedentario y con una dieta no contralada son las principales factoras de riesgo para que el cáncer de mama se presente en las personas. Aunque es verdad que la parte genética también forma parte de los factores principales de riesgo para adquirir esta forma de cáncer, no hay nada aun que se pueda hacer ara modificar este aliciente genético a desarrollar la enfermedad. Así que recomendamos tomar en consideración estos consejos para reducir al mínimo las posibilidades modificando todo aquello que esté en nuestras manos.
				</p>


				<h2 class="mb-4">Detección del cáncer de mama “Breast cancer detection”</h2>
	      <img class="img-fluid w-50 float-left mr-4 mb-3" src="public/img/blogs/Blog-30.webp" alt="Image">
	      <p>
	      	Las posibilidades de sobrevivir al cáncer de mama se incrementan de forma drástica si se logra una detección temprana. Cuando la detección del cáncer se genera en las primeras fases, la esperanza de que la enfermedad no sea mortal es inmensa, así que lo más recomendable siempre va a ser establecer una rutina de chequeos acorde a la edad para estar revisando que todo este en orden.
					<br/>
					<br/>
					It is recommended that women who are 50 to 74 years old and are at average risk for breast cancer get a mammogram every two years. Women who are 40 to 49 years old should talk to their doctor or other health care professional about when to start and how often to get a mammogram. (Se recomienda que mujeres entre 50 y 74 años que tienen un riesgo promedio de contraer el cáncer de mama se hagan una mamografía al menos 1 vez cada 2 años- Mujeres que están entre los 40 y 49 años deben hablar con su médico para establecer un plan de frecuencias para realizarse las mamografías).
					<br/>
					<br/>
					Cabe destacar que una mamografía es un proceso prácticamente indoloro y muy rápido, además de accesible tanto en locaciones como en precio. 
					<br/>
					<br/>
					Esta es una enfermedad que puede ser erradicada mediante la cultura de la prevención, si tú conoces a alguien que está en los rangos de edad antes mencionados y consideras que no conoce estos detalles, platica con ella y hagamos conciencia acerca de esta enfermedad, probablemente habrás salvado la vida de una madre, esposa, hermana y amiga. Todos estamos juntos en esta batalla, si estás leyendo esto es por que conoces el acceso a internet y tienes acceso a un mundo de información privilegiada, pero recordemos que muchos de nuestros adultos mayores no, así que hagamos conciencia, compartamos esta información no solo durante este mes, sino durante toda nuestra vida y sumémonos a la solución de este problema. 
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
