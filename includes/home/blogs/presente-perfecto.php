<!-- Page Header Start -->
<div class="container-fluid pt-0 pt-lg-5 mb-5 d-md-block">
</div>

<!-- Detail Start -->
<div class="container py-5">
  <div class="row">
    <!-- Blog Detail Start -->
    <div class="col-lg-8">
      <div class="position-relative">
        <img class="img-fluid w-100" src="public/img/blogs/portada-presente-perfecto.webp" alt="">
        <div class="position-absolute bg-primary d-flex flex-column align-items-center justify-content-center" style="width: 80px; height: 80px; bottom: 0; left: 0;">
	        <h6 class="text-uppercase mt-2 mb-n2 text-white">Sep</h6>
					<h1 class="m-0 text-white">19</h1>
	      </div>
    	</div>
	    <div class="pt-4 pb-2">
	      <div class="d-flex mb-3">
	        <div class="d-flex align-items-center ml-4">
	          <i class="far fa-bookmark text-primary mr-2"></i>Presente perfecto
	        </div>
	      </div>
	      <h2 class="font-weight-bold">Presente perfecto</h2>
	    </div>

	    <div class="mb-5">
	      <p>
	      	En este artículo vamos a hablar de los pros y contras de aprender inglés gratis. Sabemos que pensaras, está escrito por una escuela así que me dirán que es malo, sin embargo, seremos muy objetivos para poderte ofrecer un panorama excelente y puedas tomar una decisión informada.
	      </p>

	      <h2 class="mb-4">¿Realmente es necesario?</h2>
	      <p>
	      	Comencemos por decir, que uno de los problemas más grandes de los alumnos al momento de aprender un idioma, es el hecho de que no tienen bien definido qué es lo que necesitan aprender.
	      	<br/>
	      	<br/>
					Evidentemente, si decimos que la gramática no es necesaria para desarrollar un lenguaje estaríamos diciendo una de las mentiras más grandes que puedan existir. Sin embargo, y aquí es donde viene el truco, no todos los alumnos realmente necesitan aprender estos conceptos de memoria.
					<br/>
	      	<br/>
					Pongamos como base el español, qué es el idioma que dominamos. Nosotros podemos desarrollarnos de forma perfecta en un ambiente cotidiano, podemos tener amigos, podemos pedir un trabajo y podemos estudiar lo que nosotros deseemos sin mayor inconveniente.
					<br/>
	      	<br/>
					Sí yo te pidiera a ti, que estás leyendo esto, una idea que contenga un adverbio de concesión en español, probablemente dirás ¿Y qué es eso? Y esto es exactamente lo que los alumnos batallan para comprender. Tú no piensas lo que es un adverbio de concesión, simplemente lo utilizas en el día a día. Eso es lo que los alumnos deben hacer al momento de aprender un idioma, si nos dejáramos de preocupar tanto por aprender términos gramaticales y pusiéramos más énfasis en cómo debemos comunicarnos, el aprendizaje del idioma sería muchísimo más sencillo
					<br/>
	      	<br/>
					Así que para contestar la pregunta de si es necesario, la respuesta es: sí y no. Aprender estos términos de forma perfecta, va a ser necesario para aquellos que quieran ser maestros de inglés, traductores o lingüistas, es decir que quieran dedicarle su vida a la enseñanza del idioma. Si por el contrario, tú solamente quieres utilizar el idioma como medio de comunicación, realmente no es conveniente que pases gran parte del tiempo estudiando estos términos que difícilmente vas a utilizar en tu día a día.
				</p>

	      <h3 class="mb-4">¿Qué es el presente perfecto?</h3>
	      <img class="img-fluid w-50 float-right ml-4 mb-3" src="public/img/blogs/presente-perfecto-1.webp" alt="Image">
	      <p>
	      	En este momento, es importante recordar que en todos los idiomas solo existen tres tiempos: el presente, el pasado y el futuro. Todas las estructuras dentro de una lengua expresan de una u otra manera algunas de estas tres opciones.
	      	<br/>
	      	<br/>
					El presente perfecto sirve para poder expresar ideas en pasado que no tienen un tiempo específico.
					<br/>
	      	<br/>
					Por ejemplo, si yo quiero decir que yo he visitado nueva York 10 veces en mi vida, ahí utilizaría el presente perfecto. ¿Por qué? Porque no estoy especificando exactamente cuando he visitado la ciudad.
					<br/>
	      	<br/>
					Podemos entonces decir, que el presente perfecto va a servir para expresar experiencias, cosas que hemos hecho a lo largo de nuestra vida. Evidentemente, también existe la parte negativa de la estructura y esto serviría para expresar cosas que no hemos hecho a lo largo de nuestra vida.
				</p>


				<h2 class="mb-4">¿Cuál es la diferencia con el pasado simple?</h2>
	      <p>
	      	Una las preguntas más comunes que se realiza un estudiante del idioma inglés es la diferencia entre estas estructuras, sin embargo, es bastante sencilla. Vamos a utilizar el pasado simple, cuando el tiempo en el que se hizo la acción es relevante y vamos a utilizar el presente perfecto cuando el tiempo en el que se hizo la acción simplemente es irrelevante.
	      	<br/>
	      	<br/>
					Veamos la diferencia en 2 ideas:
					<br/>
					• Mike ate a sándwich yesterday
					<br/>
					• Mike has eaten a sandwich
					<br/>
	      	<br/>
					En la primera idea utilizamos el pasado simple debido a que estamos especificando que fue ayer el día que Mike comió el sándwich.
					<br/>
	      	<br/>
					En la segunda idea utilizamos el presente perfecto debido a que no especificamos cuando exactamente ha comido un sándwich, solamente expresamos quq es una acción que ya ha realizado en su vida.
					<br/>
	      	<br/>
					Y así básicamente, es cómo se utiliza el presente perfecto. Realmente es una estructura que genera mucha confusión, sin embargo, una vez que se entiende es bastante sencilla de ser utilizada.
					<br/>
	      	<br/>
					Recuerda el inglés no tiene por qué ser difícil, de hecho, todo lo contrario, cada día tienes que avanzar en tu manera de comunicarte y al final lograrás tu objetivo de ser bilingüe.
				</p>
	    </div>
	  </div>
    <!-- Comment Form End -->
	  <!-- Blog Detail End -->

	  <?php
	    include 'post-recientes.php';
	  ?>
	</div>
</div>
<!-- Detail End -->
