<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-12">
        <h1 class="mt-2 mb-4 text-center">Curso de inglés para jóvenes y adultos - Aprende a hablar inglés tan fácil como aprendiste español!</h1>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/cursos/5-intensivo.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Nuestro método</h2>
        <p class="mb-4 mt-4">
          Nuestro curso “Adults” es ideal para que jóvenes y adultos puedan aprender inglés, ya que nos enfocamos en primero hablar el idioma y después escribirlo. Esta metodología está más que comprobada, ya que fue la manera en la que aprendimos español.
          <br/>
          <br/>
          Nuestras clases son totalmente en inglés, lo que nos permitirá darles resultados en un tiempo increíblemente corto.
          <br/>
          <br/>

          INBI es la mejor opción como escuela de inglés para jóvenes y adultos en Monterrey y su área metropolitana, debido a la manera tan practica en la que enseñamos el idioma.
        </p>
        <a href="https://wa.me/+5218120474412?text=Quiero+más+información+del+curso+de+inglés+ADULTOS+y+las+promociones+disponibles" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Contactar a una asesora</a>
      </div>
    </div>
  </div>
</div>
<!-- About End -->


<!-- HORARIOS -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/general/horario.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Horarios</h2>
        <h4 class="font-weight-normal text-muted mt-4">Contamos con clases de </h4>
        <div class="list-inline mb-4">
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>lunes a viernes de 9 de la mañana a 9 de la noche</p>
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>O si lo prefieres puedes estudiar los sábados en un horario de 9 de la mañana a 2 de la tarde.</p>
        </div>
        <!-- <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Read More</a> -->
      </div>
    </div>
  </div>
</div>

<!-- NUESTRA APP -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/general/app-movil.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Nuestra App</h2>
        <p class="mb-4 mt-4">
          En nuestra App podrás practicar las lecciones vistas en clase para que tu aprendizaje sea aun mejor. Básicamente podrás aprender ingles gratis en tu celular a la par que tomas tus clases con nosotros.
          <br/>
          <br/>
          Además de practicar, también podrás llevar un control de tus calificaciones diariamente lo que te permitirá saber en que hay que mejorar y practicar en clases.
        </p>
        <a href="https://play.google.com/store/apps/details?id=com.lmsmovil.app&hl=es_MX&pli=1" target="_blank" class="btn btn-primary py-md-2 px-md-4 mr-3 font-weight-semi-bold text-white">Android</a>
        <a href="https://apps.apple.com/us/app/inbi-school/id1616751414?platform=iphone" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">iOS</a>
      </div>
    </div>
  </div>
</div>

<!-- BECAS -->
<div class="container-fluid pt-5 pb-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/general/becas.webp" alt="">
      </div>
      <div class="col-lg-6 mb-5 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Becas</h2>
        <p class="font-weight-normal text-muted mb-4 mt-4">
          Creemos que aprender inglés debe estar al alcance de todos, así que tenemos un programa de becas continuas que te permitirán lograr tus metas sin sacrificar tu bolsillo.
          <br/>
          <br/>
          Envíanos un mensaje y con gusto una asesora te contactara para explicarte la beca a detalle. 
        </p>
        <a href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+las+becas+para+el+curso+Adults" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Más información</a>
      </div>
    </div>
  </div>
</div>
