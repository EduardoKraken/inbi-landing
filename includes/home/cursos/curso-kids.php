<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>



<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-12">
        <h1 class="mt-2 mb-4 text-center">Curso de inglés para niños - Regálales un mejor futuro 🚀</h1>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/cursos/1-KIDS.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Nuestro método</h2>
        <p class="mb-4 mt-4">
          El curso “Kids” está diseñado para niños y niñas de 5 a 8 años. Sabemos que como padres de familia siempre buscamos la mejor escuela de inglés para niños, sin embargo, es importante saber que no todos los niños aprenden de la misma forma, es por eso que les enseñaremos mediante “Inteligencias múltiples”.
          <br/>
          <br/>
          El problema de los niños al aprender el idioma es que se aburren con facilidad. En nuestras clases tus hijos, colorearán, cantarán, verán películas y muchas cosas más que harán que aprender sea una experiencia divertida. 
          Todas nuestras aulas son interactivas, para que tus niños puedan aprender en un ambiente relajado y seguro.
          <br/>
          <br/>

          INBI es la mejor opción como escuela de inglés para niños y jóvenes en Monterrey y su área metropolitana gracias a que combinamos la diversión con el aprendizaje del idioma. 
        </p>
        <a href="https://wa.me/+5218120474412?text=Quiero+más+información+del+curso+de+inglés+KIDS+y+las+promociones+disponibles" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Contactar a una asesora</a>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- SEGURIDAD -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">

      <div class="col-lg-6 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/general/aula.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Seguridad</h2>
        <h4 class="font-weight-normal text-muted mb-4 mt-4">Para nosotros la seguridad de tus hijos es muy importante, es por ello que</h4>
        <div class="list-inline mb-4">
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Todas nuestras aulas están equipadas con sistema de circuito cerrado para saber en todo momento lo que pasa dentro de clases y estar 100% seguros que tus hijos están teniendo la mejor experiencia.</p>
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Además, todas nuestras escuelas cuentan con sensores en la puerta principal, lejos del alcance de los niños, haciéndoles imposible el poder llegar al exterior del plantel.</p>
        </div>
        <!-- <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Read More</a> -->
      </div>

    </div>
  </div>
</div>

<!-- HORARIOS -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/general/horario.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Horarios</h2>
        <div class="list-inline mb-4 mt-4">
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Nuestros alumnos pueden acudir 2 días entre semana</p>
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>o bien solo el día sábado a recibir sus clases</p>
        </div>
        <h4 class="font-weight-normal text-muted mb-4">En ambos modelos se avanzará a la misma velocidad, así que se debe decidir basado en qué horario se acopla más a su día a día.</h4>
        <!-- <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Read More</a> -->
      </div>
    </div>
  </div>
</div>

<!-- NUESTRA APP -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 mt-3 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/general/app-movil.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Nuestra App</h2>

        <p class="mb-4 mt-4">
          Como padres de familia, sabemos la importancia de la inversión que hacen en la educación de sus hijos.
          <br/>
          <br/>
          Es por eso que, en nuestra App, llevaras el control DIARIAMENTE de las calificaciones de tus pequeños, además podrás ver su desempeño en clases y cómo van evolucionando en el idioma
          <br/>
          <br/>
          Trabajaremos juntos para lograr que tus hijos cumplan su meta de ser bilingües y poder brindarles mejores oportunidades en un futuro.
        </p>
        <a href="https://play.google.com/store/apps/details?id=com.lmsmovil.app&hl=es_MX&pli=1" target="_blank" class="btn btn-primary py-md-2 px-md-4 mr-3 font-weight-semi-bold text-white">Android</a>
        <a href="https://apps.apple.com/us/app/inbi-school/id1616751414?platform=iphone" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">iOS</a>
      </div>
    </div>
  </div>
</div>

<!-- BECAS -->
<div class="container-fluid pt-5 pb-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/general/becas.webp" alt="">
      </div>
      <div class="col-lg-6 mb-5">
        <h2 class="mt-2 mb-4 text-center">Becas</h2>
        <h4 class="font-weight-normal text-muted mb-4 mt-4">Queremos que tus hijos estudien, pero entendemos que debe ser a un precio justo.</h4>
        <div class="list-inline mb-4">
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Conoce nuestro programa de becas individuales o nuestra beca de hermanos. </p>
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Regalarles un mejor futuro es más económico de lo que te imaginas.</p>
        </div>
        <a href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+las+becas+para+el+curso+Kids" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Más información</a>
      </div>
    </div>
  </div>
</div>
