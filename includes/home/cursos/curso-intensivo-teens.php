<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-12">
        <h1 class="mt-2 mb-4 text-center">El mejor curso de inglés para jovenes - Su futuro es hoy! </h1>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/cursos/3-TEENS.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Nuestro método</h2>
        <p class="mb-4 mt-4">
          Nuestro curso “Teens” está diseñado para niños y niñas de 9 a 12 años. En esta etapa de la vida, los niños comienzan a interesarse por actividades diferentes de cuando eran más pequeños. Nuestro curso para aprender inglés toma en cuenta este cambio y adaptaremos el sistema de inteligencias múltiples.
          <br/>
          <br/>
          Dentro del aula, nuestros alumnos podrán interactuar con los pizarrones inteligentes y practicar el idioma mientras están cantando, jugando y haciendo nuevos amigos.
          <br/>
          <br/>

          INBI es la mejor opción como escuela de inglés para niños y jóvenes en Monterrey y su área metropolitana, gracias a que combinamos la diversión con el aprendizaje del idioma.
        </p>
        <a href="https://wa.me/+5218120474412?text=Quiero+más+información+del+curso+de+inglés+TEENS+y+las+promociones+disponibles" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Contactar a una asesora</a>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- SEGURIDAD -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/general/aula.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Seguridad</h2>
        <h4 class="font-weight-normal text-muted mb-4 mt-4">Al igual que en el curso “Kids” nos aseguraremos que tus hijos aprendan en un entorno agradable y fuera de peligro, para ello </h4>
        <div class="list-inline mb-4">
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>tenemos un sistema de cámaras en todas nuestras aulas que nos permite tener el control de lo que sucede en clases en todo momento. .</p>
        </div>
        <!-- <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Read More</a> -->
      </div>
    </div>
  </div>
</div>

<!-- HORARIOS -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/general/horario.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Horarios</h2>
        <div class="list-inline mb-4 mt-4">
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Nuestros alumnos pueden acudir 2 días entre semana</p>
          <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>o bien solo el día sábado a recibir sus clases</p>
        </div>
        <h4 class="font-weight-normal text-muted mb-4">En ambos modelos se avanzará a la misma velocidad, así que se debe decidir basado en que horario se acopla mas a su día a día.</h4>
        <!-- <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Read More</a> -->
      </div>
    </div>
  </div>
</div>

<!-- NUESTRA APP -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">

      <div class="col-lg-6 mt-3 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/general/app-movil.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Nuestra App</h2>
        <p class="mb-4 mt-4">
          En nuestra App, los alumnos podrán reforzar lo aprendido en clases de manera gratuita 24/7. 
          <br/>
          <br/>
          Como padres de familia, también tendrán acceso a la App para llevar un seguimiento perfecto de lo que ocurre con el conocimiento de sus hijos.
          <br/>
          <br/>
          Trabajaremos juntos para lograr que sus hijos cumplan su meta de ser bilingües y poder brindarles mejores oportunidades en un futuro.
        </p>
        <a href="https://play.google.com/store/apps/details?id=com.lmsmovil.app&hl=es_MX&pli=1" target="_blank" class="btn btn-primary py-md-2 px-md-4 mr-3 font-weight-semi-bold text-white">Android</a>
        <a href="https://apps.apple.com/us/app/inbi-school/id1616751414?platform=iphone" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">iOS</a>
      </div>
    </div>
  </div>
</div>

<!-- BECAS -->
<div class="container-fluid pt-5 pb-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/general/becas.webp" alt="">
      </div>
      <div class="col-lg-6 mb-5">
        <h1 class="mt-2 mb-4 text-center">Becas</h1>
        <p class="font-weight-normal text-muted mb-4 mt-4">
          Queremos que el dinero no sea un problema, así que hemos diseñado un programa de becas para que estudiar inglés sea económico y poder brindarles a los niños la oportunidad de tener un mejor futuro dominando el idioma
          <br/>
          <br/>
          Tenemos la posibilidad de brindar precios especiales a nuestros alumnos de forma individual o bien becas de hermanos, envíanos un mensaje y una asesora con gusto te informará de nuestras promociones 
        </p>
        <a href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+las+becas+para+el+curso+Teens" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Más información</a>
      </div> 
    </div>
  </div>
</div>
