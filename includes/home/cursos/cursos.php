<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>

<!-- SECCIÓN DE CURSOS -->
<div class="section layout_padding margin-bottom_30">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="text-center">
            <h1 class="mt-2 mb-5">Nuestros cursos</h1>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <a class="font-weight-semi-bold" href="curso-kids">
          <div class="full blog_img_popular">
            <img class="img-responsive" src="public/img/cursos/1-KIDS.webp" alt="#" /> 
            <h2>Kids (5 a 8 años)</h2>
          </div>
        </a>
      </div>
      <div class="col-md-4">
        <a class="font-weight-semi-bold" href="curso-intensivo-teens">
          <div class="full blog_img_popular">
            <img class="img-responsive" src="public/img/cursos/3-TEENS.webp" alt="#" />
            <h2>Teens ( 8 a 12 años)</h2>
          </div>
        </a>
      </div>
      <div class="col-md-4">
        <a class="font-weight-semi-bold" href="curso-intensivo-adultos">
          <div class="full blog_img_popular">
            <img class="img-responsive" src="public/img/cursos/5-intensivo.webp" alt="#" />
            <h2>Jóvenes y Adultos</h2>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
<!-- end section -->