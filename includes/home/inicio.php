  <!-- Carousel Start -->
  <div class="container-fluid p-0 mb-5">
    <div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#header-carousel" data-slide-to="1"></li>
        <li data-target="#header-carousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="img-fluid" src="public/img/inicio/carousel-1.webp" alt="Image">
          <div class="carousel-caption d-flex align-items-center justify-content-center">
            <div class="p-5" style="width: 100%; max-width: 1200px;">
              <h1 class="text-white mb-md-3 texto_banner">En INBI nos enfocamos en que nuestros alumnos aprendan inglés tal cual aprendieron español. Contamos con cursos para niños, jóvenes y adultos en nuestros 6 planteles en Monterrey y su área metropolitana, además de tener nuestro curso en línea que hace posible aprender inglés desde cualquier lugar del mundo. </h1>
              <a href="https://wa.me/+5218120474412?text=Quiero+más+información+del+curso+de+inglés+y+las+promociones+disponibles" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2 text-white">Conoce nuestros cursos</a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="img-fluid" src="public/img/inicio/carousel-2.webp" alt="Image">
          <div class="carousel-caption d-flex align-items-center justify-content-center">
            <div class="p-5" style="width: 100%; max-width: 1200px;">
              <h5 class="text-white text-uppercase mb-md-3">¿No sabes por dónde empezar? </h5>
              <h1 class="text-white mb-md-3 texto_banner">Hemos desarrollado un examen que nos permitirá saber tus fortalezas y debilidades en el idioma. Realizarlo no te tomará más de 10 minutos y es totalmente gratis. Además, obtendrás una constancia digital de tu nivel de inglés.</h1>
              <a href="examen-ubicacion" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2 text-white">Conoce tu nivel de inglés</a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img class="img-fluid" src="public/img/inicio/carousel-3.webp" alt="Image">
          <div class="carousel-caption d-flex align-items-center justify-content-center">
            <div class="p-5" style="width: 100%; max-width: 1200px;">
              <h1 class="text-white mb-md-3 texto_banner">Comienza el año de la mejor manera. Aprender inglés no tiene por qué ser caro. Estudia el idioma al mejor precio, con nuestro increíble método de enseñanza. Envíanos un WhatsApp en el siguiente enlace y conoce nuestras becas para clases presenciales y en línea. ¡Solo tenemos 100 disponibles!  </h1>
              <a href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+las+becas+anunciadas+en+la+página+web" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold mt-2 text-white">Solicita una beca</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Carousel End -->


  <!-- About Start -->
  <div class="container-fluid pb-5">
    <div class="container">
      <div class="row align-items-center pb-1">
        <div class="col-lg-5">
          <img class="img-thumbnail p-3" src="public/img/estudiantes_tecnologia.webp" alt="" width="600px">
        </div>
        <div class="col-lg-7 mt-5 mt-lg-0">
          <!-- <small class="bg-primary text-white text-uppercase font-weight-bold px-1">Who We Are</small> -->
          <h1 class="mt-2 mb-4 text-center">Bienvenidos al mejor método para aprender inglés</h1>
          <p class="mb-4">
            En nuestras clases pondremos la tecnología para que nuestros alumnos puedan aprender inglés. En nuestra App podrás realizar actividades adicionales de forma gratuita que te ayudarán a mejorar lo visto en clases y lo mejor de todo es que es totalmente gratis.
            <br/>
            <br/>
            Podrás llevar un control total de tus calificaciones (o las de tu hijo o hija) que te permitirá saber las áreas a mejorar para convertirte en bilingüe en el menor tiempo posible. Nuestro método de enseñanza te encantará, conoce más de las sorpresas que tenemos para tu aprendizaje.
          </p>
          <a href="https://wa.me/+5218120474412?text=Quiero+más+información+del+curso+de+inglés+y+las+promociones+disponibles" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white">Conoce más</a>
        </div>
      </div>
    </div>
  </div>
  <!-- About End -->

  <!-- SECCIÓN DE CURSOS -->
  <div class="section layout_padding margin-bottom_30">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="text-center">
              <h1 class="mt-2 mb-5">Nuestros cursos</h1>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <a class="font-weight-semi-bold" href="curso-kids">
            <div class="full blog_img_popular">
              <img class="img-responsive" src="public/img/cursos/1-KIDS.webp" alt="#" width="800px"/> 
              <h2>Kids (5 a 8 años)</h2>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a class="font-weight-semi-bold" href="curso-intensivo-teens">
            <div class="full blog_img_popular">
              <img class="img-responsive" src="public/img/cursos/3-TEENS.webp" alt="#" width="800px"/>
              <h2>Teens (8 a 12 años)</h2>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a class="font-weight-semi-bold" href="curso-intensivo-adultos">
            <div class="full blog_img_popular">
              <img class="img-responsive" src="public/img/cursos/5-intensivo.webp" alt="#"  width="800px" />
              <h2>Jóvenes y Adultos</h2>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->


  <!-- CONOCE TU NIVEL DE INGLES -->
  <div class="container-fluid pb-5">
    <div class="container">
      <div class="row align-items-center pb-1">
        <div class="col-lg-7 mt-5 mt-lg-0">
          <!-- <small class="bg-primary text-white text-uppercase font-weight-bold px-1">Who We Are</small> -->
          <h1 class="mt-2 mb-4 text-center">Conoce tu nivel de inglés</h1>
          <p class="mb-4">
            Mediante nuestro examen en línea podrás conocer en pocos minutos tus áreas a mejorar en el idioma. Este examen evalúa tu capacidad para leer, escuchar y escribir en inglés. Al finalizar el examen, recibirás una constancia donde quedara plasmado tu nivel y consejos para que puedas mejorar día con día. Realiza tu examen y conoce como nuestro curso puede ayudarte a ser bilingüe y tener mejores oportunidades académicas y laborales.
          </p>
          <a href="examen-ubicacion" target="_blank" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white mb-3">Conocer mi nivel de inglés</a>
        </div>
        <div class="col-lg-5">
          <img class="img-thumbnail p-3" src="public/img/test-ingles.webp" alt=""  width="600px">
        </div>
      </div>
    </div>
  </div>
  <!-- CONOCE TU NIVEL DE INGLES -->


  <!-- PLANTELES -->
  <div class="container-fluid pb-3" id="secion-planteles">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 mb-5">
          <h1 class="mt-2 mb-4 text-center">Conoce nuestros planteles</h1>
          <h1 class="mt-2 mb-3 h1subtitle2">
            <span class="font-weight-bold">Todos nuestros cursos de inglés</span> 
            <br/>
          </h1>
          <p class="mb-4">
            son impartidos en aulas inteligentes dentro de nuestros 7 planteles en Monterrey y su área metropolitana, así que seguramente tenemos un plantel cerca de ti para que puedas estudiar de forma presencial.
            <br/>
            <br/>
            Cada aula está equipada con proyectores, sonido envolvente y un sistema interactivo donde podrás practicar usando nuestra App haciendo de tu aprendizaje la mejor experiencia.
            <br/>
            <br/>
            Tenemos planteles en Guadalupe (Pablo Livas y Linda Vista), Apodaca (Centro de Apodaca, Fresnos y San Miguel) y San Nicolás (Anáhuac y Casa Blanca).
          </p>
        </div>

        <div class="col-lg-6">
          <div class="row">
            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Anáhuac</h2>
                  <p>José Santos Chocano 111 <br/>Tel: (81) 2473-5131</p>
                  Más información<a class="font-weight-semi-bold semi_boton" href="sucursal_anahuac">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Apodaca</h2>
                  <p>Benito Júarez 113<br/>Tel: (81) 1180-8780</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_apodaca">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Casa Blanca</h2>
                  <p>Av. Casa Blanca #505 <br/>Tel: (81) 2473-6728</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_casa_blanca">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Fresnos</h2>
                  <p>Calle Guernica 127 <br/>Tel: (81) 2526-6434</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_fresnos">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Linconl</h2>
                  <p>Av. Agami #5374<br/></p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_lincoln">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Miguel Alemán</h2>
                  <p>Miguel Alemán 4319 <br/>Tel: (81) 2233-1381</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_linda_vista">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Online</h2>
                  <p>Tel: (81) 2047-4412</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_online">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">Pablo Livas</h2>
                  <p>Av. Pablo Livas #17 <br/>Tel: (81) 2473-5496</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_pablo_livas">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 mb-5">
              <div class="d-flex">
                <i class="fa fa-2x fa-map-marker text-primary mr-4"></i>
                <div class="d-flex flex-column">
                  <h2 class="font-weight-bold mb-1 text-h2-size">San Miguel</h2>
                  <p>Av. Acapulco #205 <br/>Tel: (81) 2673-9961</p>
                  Más información<a class="font-weight-semi-bold  semi_boton" href="sucursal_san_miguel">Clic aquí<i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- PLANTELES -->

  <?php
    include 'blogs/menu-blog.php';
  ?>

  <!-- CONTACTANOS -->
  <div class="container-fluid my-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-7 py-5 py-lg-0">
          <div class="row">
            <img class="img-thumbnail p-3" src="public/img/contactanos.webp" alt="" width="600px">
          </div>
        </div>
        <div class="col-lg-5">
          <div class="bg-contacto py-5 px-4 px-sm-5">
            <h2 class="text-white">Contáctanos</h2>
            <p class="text-white">Déjanos tus datos y una asesora se comunicará contigo para brindarte todos los detalles del curso y promociones vigentes</p>
            <div class="form-group">
              <input type="text" id="floatingNombre" class="form-control border-0 p-4" placeholder="Nombre" required="required" />
            </div>
            <div class="form-group">
              <input  class="form-control border-0 p-4" id="floatingCelular" placeholder="Teléfono" required="required" />
            </div>
            <div class="form-group">
              <textarea type="text" class="form-control border-0 p-4" id="floatingMensaje" placeholder="Mensaje" required="required">Quiero más información acerca del curso y las promociones vigentes.</textarea>
            </div>
            
            <div>
              <button class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white btn-block" id="btn-enviar-datos">Enviar datos</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Quote Request Start -->
</div>

<!-- Back to Top -->
<!-- <a href="#" class="btn btn-lg btn-primary back-to-top"><i class="fa fa-angle-up text-white"></i></a> -->
</body>

</html>