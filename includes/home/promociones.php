<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
  <div class="d-inline-flex text-white">
  </div>
</div>


<div class="container-fluid py-5">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h1 class="mt-2 mb-4 text-center">BECA DE HERMANOS</h1>

        <div class="list-inline mb-4 mt-4">
          <h1 class="textNoH1">
            <p class="font-weight-semi-bold mb-2">
              <i class="fa fa-angle-right text-primary mr-2"></i>Al inscribirte a nuestro curso de inglés para niños recibirás una beca especial. El mejor regalo que les puedes dar es un mejor futuro, hagámoslo posible. Envíanos un mensaje y conoce los detalles de la promoción.
            </p>
          </h1>
        </div>

        <a 
          href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+la+beca+de+hermanos+anunciada+en+la+página+web"
          class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white"
          target="_blank" 
        >Enviar WhatsApp a una asesora</a>
      </div>
      <div class="col-lg-6 mt-3">
        <img class="img-thumbnail p-3" src="public/img/promociones/beca_hermanos.webp" alt="">
      </div>
    </div>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/promociones/induccion.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h1 class="mt-2 mb-4 text-center">CURSO DE INDUCCIÓN</h1>
        <h5 class="font-weight-normal text-muted mb-4 mt-4">¿Quieres reforzar tus conocimientos en el idioma?</h5>
        <div class="list-inline mb-4 mt-4">
          <h1 class="textNoH1">
            <p class="font-weight-semi-bold mb-2"><i class="fa fa-angle-right text-primary mr-2"></i>Este curso es para ti. No importa si sabes inglés básico, intermedio o avanzado. Este curso en línea con maestros en vivo comenzará desde cero y te enseñará las bases del idioma de una manera fácil y divertida. Conoce las bases del curso enviándonos un mensaje </p>
          </h1>
        </div>
        <a 
          href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+del+curso+de+inducción+anunciado+en+la+página+web"
          class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold text-white"
          target="_blank" 
        >Enviar WhatsApp a una asesora</a>
      </div>
    </div>
  </div>
</div>
