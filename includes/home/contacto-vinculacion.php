<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5">
  <div class="d-inline-flex text-white">
  </div>
</div>

<div class="container-fluid py-5">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h1 class="mt-2 mb-4 text-center">Curso de inglés para empresas</h1>

        <div class="list-inline mb-4 mt-4">
          <p class="mb-2">
            Sabemos la importancia que tiene el desarrollo del personal dentro de la organización. Para ello tenemos un programa especial para empresas que le permitirá alcanzar metas individuales de sus colaboradores, así como incrementar la productividad de la compañía al contar con personal mejor capacitado y más comprometido. 
          </p>
        </div>
      </div>
      <div class="col-lg-6 mt-3">
        <img class="img-thumbnail p-3" src="public/img/empresas/empresas_ingles.webp" alt="">
      </div>
    </div>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/empresas/cursos_ingles.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">¿Cómo lo haremos? </h2>
        <div class="list-inline mb-4 mt-4">
          <p class="mb-2">
            Generaremos una sinergia que permitirá a sus colaboradores tener acceso a cualquiera de nuestros 7 planteles en Monterrey y su área metropolitana o bien tomar el curso en línea en cualquier lugar del mundo. Este acceso les permitirá tomar su curso de inglés iniciando en el nivel adecuado de acuerdo a su conocimiento previo del idioma.
          </p>
          <p class="mb-2">
            Esto permitirá que el colaborador pueda elegir un horario que no interfiera con sus actividades de la empresa y tomar sus clases en un lugar que le sea cómodo para sus trayectos diarios, generando un ambiente de confort necesario para el aprendizaje del idioma. 
          </p>
          <p class="mb-2">
            Conoceremos la situación inicial de cada persona mediante la aplicación de un examen de evaluación que se realiza a través de nuestra App. Permitiéndonos ofrecer soluciones adecuadas a cada persona. 
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">¿Cómo los evaluaremos?</h2>
        <div class="list-inline mb-4 mt-4">
          <p class="mb-2">
            Cada 2 semanas, los alumnos serán evaluados mediante exámenes que aseguren su conocimiento. Además de tener un control diario de la asistencia de forma electrónica.
          </p>
          <p class="mb-2">
            Estos reportes pueden hacerse llegar a la compañía, con la finalidad de que tengan la certeza que los colaboradores están haciendo uso del beneficio de manera correcta. 
          </p>
        </div>
      </div>
      <div class="col-lg-6 mt-3">
        <img class="img-thumbnail p-3" src="public/img/empresas/examen_ingles.webp" alt="">
      </div>
    </div>
  </div>
</div>

<div class="container-fluid py-5">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/empresas/beneficios_ingles.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Beneficios del convenio empresarial</h2>
        <div class="list-inline mb-4 mt-4">
          <p class="mb-2">
            <i class="fa fa-angle-right text-primary mr-2"></i>Precios preferenciales.<br/>
            <i class="fa fa-angle-right text-primary mr-2"></i>Reportes grupales.<br/>
            <i class="fa fa-angle-right text-primary mr-2"></i>Colaboradores mejor capacitados.<br/>
            <i class="fa fa-angle-right text-primary mr-2"></i>Equipo de trabajo mas comprometido con la empresa.<br/>
            <i class="fa fa-angle-right text-primary mr-2"></i>7 planteles presenciales para tomar clases. <br/>
            <i class="fa fa-angle-right text-primary mr-2"></i>Hacer extensiva la beca para familiares de sus colaboradores.<br/>
          </p>
          <p class="mb-2">
            Entre muchos otros… 
          </p>

          <p class="mb-2">
            Tenemos muchos beneficios adicionales que nos gustaría hacerte saber. Envíanos un correo a través del siguiente formulario de contacto, para ofrecerte una atención personalizada a la brevedad. 
          </p>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Contact Start -->
<div class="container-fluid py-5">
  <div class="container">
    <div class="text-center">
      <h2 class="mt-2 mb-5">Envíanos tus datos para comunicarnos contigo</h2>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="d-flex align-items-center border mb-3 p-4">
          <i class="fas fa-2x fa-phone-alt text-primary mr-3"></i>
          <div class="d-flex flex-column">
            <h5 class="font-weight-bold">Llámanos</h5>
            <p class="m-0">
            <a href="tel:+8120474412">+52 (81) 2047 4412</a></p>
          </div>
        </div>

        <div class="d-flex align-items-center border mb-3 p-4">
          <i class="fa fa-2x fa-envelope-open text-primary mr-3"></i>
          <div class="d-flex flex-column">
            <h5 class="font-weight-bold">Correo</h5>
            <a href="mailto:vinculacion@inbi.mx">vinculacion@inbi.mx</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="contact-form">
          <form name="sentMessage" id="contactForma" novalidate="novalidate">
            <div class="form-row">
              <div class="col-md-6">
                <div class="control-group">
                  <input type="text" class="form-control p-4" id="floatingNombre" placeholder="Nombre" required="required" data-validation-required-message="Ingresa tu nombre" />
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="control-group">
                  <input  class="form-control p-4" id="floatingCelular" placeholder="Celular" required="required" data-validation-required-message="Ingresta tu celular" />
                  <p class="help-block text-danger"></p>
                </div>
              </div>
            </div>
            <div class="control-group">
              <textarea class="form-control" rows="5" id="floatingMensaje" placeholder="Message" required="required" data-validation-required-message="Ingresa un mensaje">Hola, me gustaría obtener más información sobre los convenios educativos con INBI School of English.</textarea>
            </div>
            <div>
              <button class="btn btn-primary px-4 text-white mt-4" style="height: 50px;" type="submit" id="btn-enviar-datos-vinculacion">Enviar Mensaje</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
