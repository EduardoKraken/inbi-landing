<div class="container-fluid">
  <div class="container">
    <div class="row mt-4">
      <div class="col-xl-12 col-md-12  mb-4 mt-5">
        <h1 class="text-primary text-justify" style="color: black !important;"><b>AVISO DE PRIVACIDAD</b></h1>
        <div class="text-justify">
        	De acuerdo a lo previsto en la "LEY FEDERAL de Protección de Datos Personales"
					INBI SCHOOL OF ENGLISH declara ser una empresa legalmente constituida de
					conformidad con las leyes mexicanas, con domicilio ubicado en, Jose Santos
					Chocano #111 Colonia Anahuac CP 666450 San Nicolas de los Garza, Nuevo Leon
					así como manifestar ser la responsable del tratamiento de sus datos personales.

					<br/>
					<br/>
					<br/>

					<b>Oficina de privacidad ubicada en:</b> Mismo domicilio.
					<br/><br/>

					<b>Teléfonos de la oficina de privacidad:</b> 24735131
					<br/><br/>

					<b>Correo electrónico:</b> informes@inbi.mx
					<br/><br/><br/>


					<b>DEFINICIONES:</b>
					<br/><br/>

					<b>Datos personales. -</b> Cualquier información concerniente a una persona física
					identificada o identificable.
					<br/><br/>

					<b>Titular. -</b> La persona física (TITULAR) a quien identifica o corresponden los datos
					personales.
					<br/><br/>

					<b>Responsable. -</b> Persona física o moral de carácter privado que decide sobre el
					tratamiento de los datos personales.
					<br/><br/>

					<b>Tratamiento. -</b> La obtención, uso (que incluye el acceso, manejo, aprovechamiento,
					transferencia o disposición de datos personales), divulgación o almacenamiento de
					datos personales por cualquier medio.
					<br/><br/>

					<b>Transferencia. -</b> Toda comunicación de datos realizada a persona distinta del
					responsable o encargado del tratamiento.
					<br/><br/>

					<b>Derechos ARCO. -</b> Derechos de acceso, rectificación, cancelación y oposición.
					<br/><br/>

					<b>Consentimiento Tácito. -</b> Se entenderá que el titular ha consentido en el tratamiento
					de los datos, cuando habiéndose puesto a su disposición el Aviso de Privacidad, no
					manifieste su oposición.
					<br/><br/>

					<b>FINALIDADES PRIMARIAS. -</b> Los datos personales que recabamos de usted, los
					utilizaremos para las siguientes finalidades que son necesarias para el servicio que
					solicita:
					<br/><br/>

					<b>Fines educativos., Fines de marketing.</b>
        </div>
      </div>
    </div>
  </div>
</div>
