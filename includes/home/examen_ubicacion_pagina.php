<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-12">
        <h1 class="mt-2 mb-4 text-center">Conoce tu nivel de inglés con nuestra prueba en línea.</h1>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- HORARIOS -->
<div class="container-fluid pb-4">
  <div class="container">
    <div class="row align-items-center pb-1 pt-0">
      <div class="col-lg-6 mt-3 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/inicio/carousel-2.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <div class="list-inline mb-4 mt-4">
          <h1 class="textNoH1">
            <p class="mb-2 h3 mb-5">Para aprender el idioma es importante conocer cuáles son las áreas que debemos mejorar, en este examen evaluaremos tus habilidades para hablar, escuchar y escribir el idioma.</p>
            <p class="mb-2"><b>Esta prueba te ayudara a saber si tu nivel de ingles es básico, intermedio o avanzado.</b></p>
            <p class="mb-2"><b>Para realizarlo, solo necesitas estar en un lugar sin distracciones y preferentemente tener unos audífonos para escuchar los audios a la perfección.</b></p>
            <p class="mb-2"><b>Al finalizar el examen, obtendrás una constancia que validara tu nivel de inglés y además te diremos en que nivel de nuestro curso deberías comenzar para lograr tu meta de ser bilingüe en tiempo récord.</b></p>
            <p class="mb-2"><b>Para iniciar el examen, solo presiona el botón y contesta las preguntas sin ayudas externas.</b></p>
          </h1>
          <a href="examen-ubicacion" target="_blank" class="btn btn-primary py-md-2 px-md-4 text-white mt-3">Presentar examen</a>
        </div>
      </div>

    </div>
  </div>
</div>
