<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>

<!-- HORARIOS -->
<div class="container-fluid pb-4">
  <div class="container">
    <div class="row align-items-center pb-1 pt-1">
      <div class="col-lg-6 mt-3 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/sucursales/fresnos.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h1 class="mt-2 mb-4 text-center">INBI Online</h1>
        <div class="list-inline mb-4 mt-4">
          <h1 class="textNoH1">
            <p class="mb-2 text-center h3 mb-5">La mejor escuela de inglés para niños, jóvenes y adultos con modalidad en línea</p>
            <p class="mb-2"><b>En este curso podrás aprender inglés sin salir de casa, con nuestras clases en vivo podrás practicar y aclarar tus dudas.</b></p>
            <p class="mb-2"><b>En nuestra App podrás repasar todo lo visto en clases 24/7 para asegurar que tu aprendizaje será el mejor</b></p>
            <p class="mb-2"><b>Envía un mensaje y con gusto una asesora del plantel Online te dará la información que necesites </b></p>
          </h1>
          <a href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+las+clases+y+promociones+del+curso+en+línea" target="_blank" class="btn btn-primary py-md-2 px-md-4 text-white mt-3">Enviar un WhatsApp al plantel</a>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container-fluid pb-3">
  <div class="container">
    <div class="row ">
      <div class="col-xl-4 col-md-6 mb-4 text-center order-md-2">
        <h1 class="mt-2 mb-4 text-center">Datos de la sucursal</h1>
        <hr>
        <h5><i class="fa fa-map-marker-alt"></i> <b>Dirección:</b></h5>
        <p>Av. Miguel Alemán 4319, Linda Vista, 67130 Guadalupe, N.L.</p>
        <hr>
        <h5><i class="fa fa-phone-alt"></i> <b>Teléfono:</b></h5>
        <p><a href="tel:8122331381"> Tel: 81-2233-1381</a></p>
        <hr>
        <h5><i class="fa fa-clock"></i> <b>Horario de atención</b></h5>
        <p>Lunes a Viernes de 8:30am a 9:00pm</p>
        <p>Sabados de 8:30am a 3:30pm</p>
      </div>
    </div>
  </div>
</div>