<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>

<!-- HORARIOS -->
<div class="container-fluid pb-4">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 mt-3 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/sucursales/sanmiguel.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h1 class="mt-2 mb-4 text-center">INBI SAN MIGUEL</h1>
        <div class="list-inline mb-4 mt-4">
          <h1 class="textNoH1">
            <p class="mb-2 text-center h3 mb-5">La mejor escuela de inglés para niños, jóvenes y adultos cerca de ti</p>
            <p class="mb-2"><b>Nuestra escuela está ubicada en San Miguel sobre la avenida Acapulco. </b></p>
            <p class="mb-2"><b>Todas nuestras aulas están equipadas con sistemas multimedia para que tengas una experiencia interactiva en clase.</b></p>
            <p class="mb-2"><b>Envía un mensaje y con gusto una asesora del plantel San Miguel te dará la información que necesites</b></p>
          </h1>
          <a href="https://wa.me/+5218120474412?text=Quiero+más+información+acerca+de+las+clases+y+promociones+del+plantel+San+Miguel" target="_blank" class="btn btn-primary py-md-2 px-md-4 text-white mt-3">Enviar un WhatsApp al plantel</a>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container-fluid pb-3">
  <div class="container">
    <div class="row ">
      <div class="col-xl-4 col-md-6 mb-4 text-center order-md-2">
        <h1 class="mt-2 mb-4 text-center">Datos de la sucursal</h1>
        <hr>
        <h5><i class="fa fa-map-marker-alt"></i> <b>Dirección:</b></h5>
        <p>Blvd. Acapulco 201-Local E, Privada las Golodrinas, 66649 Cd Apodaca, N.L.</p>
        <hr>
        <h5><i class="fa fa-phone-alt"></i> <b>Teléfono:</b></h5>
        <p><a href="tel:8126739961"> Tel: 81-2673-9961</a></p>
        <hr>
        <h5><i class="fa fa-clock"></i> <b>Horario de atención</b></h5>
        <p>Lunes a Viernes de 8:30am a 9:00pm</p>
        <p>Sabados de 8:30am a 3:30pm</p>
      </div>
      <div class="col-xl-8 col-md-6 mb-4 order-md-1">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.551949768998!2d-100.18198528466638!3d25.71925471663248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eb580672bd3f%3A0x596fd7fa2a6639eb!2sINBI%20San%20Miguel%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620168476600!5m2!1ses!2smx" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
    </div>
  </div>
</div>