<!-- Page Header Start -->
<div class="container-fluid page-header d-flex flex-column align-items-center justify-content-center pt-0 pt-lg-5 mb-5">
</div>



<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-12">
        <h1 class="mt-2 mb-4 text-center">Sobre nosotros</h1>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- NUESTRO METODO -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/vision/filosofia_objetivos.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Filosofía y objetivo</h2>
        <p class="mb-4 mt-4 font-weight-semi-bold">
          La filosofía de nuestra escuela de inglés es brindar un enfoque en el aprendizaje práctico y la aplicación. Creemos que la mejor manera de aprender un nuevo idioma es a través de la experiencia y la aplicación práctica de lo que se ha aprendido. Por lo tanto, nos esforzamos por proporcionar un ambiente de aprendizaje que sea lo más realista y práctico posible para nuestros estudiantes.
					<br/>
          <br/>
					Nuestro objetivo es ayudar a nuestros estudiantes a desarrollar habilidades efectivas y confiadas en el inglés. Para lograr esto, ofrecemos clases dinámicas y participativas que incluyen conversaciones, juegos de rol, simulaciones de situaciones cotidianas, y otras actividades que fomenten la práctica y la aplicación del inglés.
        </p>
      </div>
    </div>
  </div>
</div>
<!-- About End -->

<!-- SEGURIDAD -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">

      <div class="col-lg-6 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/vision/objetivos.webp" alt="">
      </div>

      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <div class="list-inline mb-4 font-weight-semi-bold">
          <p class="mb-4 mt-4">
					Creemos en un enfoque centrado en el estudiante y en su capacidad de aprender y mejorar sus habilidades en el inglés. Nuestra filosofía se basa en la idea de que cada estudiante tiene un ritmo único de aprendizaje y que es importante brindarle un apoyo y un ambiente de aprendizaje personalizado para ayudarlo a alcanzar sus metas y objetivos de aprendizaje.
					<br/>
          <br/>
					En resumen, nuestra filosofía de enfoque en el aprendizaje práctico y la aplicación se centra en brindar una experiencia de aprendizaje efectiva y significativa a nuestros estudiantes, para que puedan comunicarse con confianza y eficacia en el inglés.
        </p>
        </div>
        <!-- <a href="" class="btn btn-primary py-md-2 px-md-4 font-weight-semi-bold">Read More</a> -->
      </div>

    </div>
  </div>
</div>

<!-- HORARIOS -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/vision/vision_inbi.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0">
        <h2 class="mt-2 mb-4 text-center">Misión</h2>
        <div class="list-inline mb-4 mt-4">
          <p class="font-weight-semi-bold mb-2">Nuestra misión es brindar una educación de alta calidad en el idioma inglés, ayudando a nuestros estudiantes a desarrollar habilidades efectivas y confiadas en el uso del idioma.
        </div>
      </div>
    </div>
  </div>
</div>

<!-- NUESTRA APP -->
<div class="container-fluid pb-3">
  <div class="container">
    <div class="row align-items-center pb-1">
      <div class="col-lg-6 mt-3 order-md-2">
        <img class="img-thumbnail p-3" src="public/img/vision/mision_inbi.webp" alt="">
      </div>
      <div class="col-lg-6 mt-5 mt-lg-0 order-md-1">
        <h2 class="mt-2 mb-4 text-center">Visión</h2>
        <p class="font-weight-semi-bold mb-4 mt-4">
          Nuestra visión es ser reconocidos como la mejor escuela de inglés, brindando una experiencia de aprendizaje significativa y un enseñanza de alta calidad.
        </p>
      </div>
    </div>
  </div>
</div>

<!-- BECAS -->
<div class="container-fluid pt-5 pb-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <img class="img-thumbnail p-3" src="public/img/vision/valores_inbi.webp" alt="">
      </div>
      <div class="col-lg-6 mb-5">
        <h2 class="mt-2 mb-4 text-center">Valores</h2>
        <div class="list-inline mb-4">
          <p class="mb-2">
          	<i class="fa fa-angle-right text-primary mr-2"></i>
          	<b>Centrado en el estudiante:</b> Creemos en un enfoque centrado en el estudiante y en su capacidad de aprender y mejorar sus habilidades en el inglés.
          </p>

          <p class="mb-2">
          	<i class="fa fa-angle-right text-primary mr-2"></i>
          	<b>Aprendizaje efectivo: </b> Brindamos un ambiente de aprendizaje efectivo que fomente la práctica y la aplicación del inglés.
          </p>

          <p class="mb-2">
          	<i class="fa fa-angle-right text-primary mr-2"></i>
          	<b>Profesionalismo: </b> Trabajamos con clases en un entorno lo más similar al mundo real.
          </p>

          <p class="mb-2">
          	<i class="fa fa-angle-right text-primary mr-2"></i>
          	<b>Personalización: </b> Creemos en un apoyo y un ambiente de aprendizaje personalizado para cada estudiante, para ayudarlo a alcanzar sus metas y objetivos de aprendizaje.
          </p>

          <p class="mb-2">
          	<i class="fa fa-angle-right text-primary mr-2"></i>
          	<b>Excelencia: </b> Nos esforzamos por brindar una experiencia de aprendizaje de la máxima calidad y por ser reconocidos como una escuela de inglés de excelencia.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
