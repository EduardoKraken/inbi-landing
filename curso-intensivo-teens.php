<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "El mejor curso de inglés para jovenes - Su futuro es hoy!";
    $metadescription = "Tus hijos amaran nuestras clases de inglés. Podrán practicar en nuestra App mientras se divierten. Conoce nuestro programa de becas y regálales un futuro brillante";
    $canonical = "<link rel='canonical' href='https://inbi.mx/curso-intensivo-teens'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/cursos/curso-intensivo-teens.php';
    include 'includes/footers/footer.php';
?>