<?php
require 'inc/conexion.php';
if(isset($_GET['id'])){
  $id=$_GET['id'];
  $sql = "SELECT e.*, n.nombre AS niveldescrip FROM examen_ubicacion e LEFT JOIN niveles_evaluacion n ON n.id = e.nivel where e.id=$id";

  if (!$resultado = $conn->query($sql)) {
    echo $sql;
  }
  $data = $resultado->fetch_assoc();

  $sqlSucursal = "select nombre from sucursales where id=".$data['sucursal'];
  $resSucursal = $conn->query($sqlSucursal);
  $ds = $resSucursal->fetch_assoc();
  $nombreSucursal = $ds['nombre'];


  $sqlAnalizado = "select * from clasificacion_respuestas where id_examen_ubicacion = $id";
  $resA = $conn->query($sqlAnalizado);
  $arreglo= $resA->fetch_assoc();
  $valor_1  = $arreglo['valor_1'];
  $opcion_1 = $arreglo['opcion_1'];
                
  //////////// Obtener la descripcion  de la evaluacion ////////////////

  $valor_11  = $arreglo['valor_11'];
  $opcion_11 = $arreglo['opcion_11'];
  
  if($valor_11=="nok"){
      $sqlMensaje = "SELECT descripcion FROM opciones_respuesta_preguntas_incorrectas where id=$opcion_11";
      $resM = $conn->query($sqlMensaje);
      $m= $resM->fetch_assoc();
      $mensajeOnce  = $m['descripcion'];
  }

}

// Obtener el nombre del nivel(solo impirmir Nivel No, no poner los incisos) y la descripcion.
$ssN= "select nombre,descripcion from niveles_evaluacion where id=".$data['nivel'];
$rnn   = $conn->query($ssN);
$nivel = $rnn->fetch_assoc();
$nombreNivel =$nivel['nombre'];
$descripcion =$nivel['descripcion'];

$nNivel = explode(" ", $nombreNivel);
$nombre_nivel =$nNivel[0]." ".$nNivel[1];  
// Optionally define the filesystem path to your system fonts
// otherwise tFPDF will use [path to tFPDF]/font/unifont/ directory
define("_SYSTEM_TTFONTS", "C:/Windows/Fonts/");
require('tfpdf/tfpdf.php');
$pdf = new tFPDF('P','mm','A4');
$pdf->AddPage();
// Add a Unicode font (uses UTF-8)
$pdf->Image('diploma_inbi.jpg' , 0 ,0, 210 , 297,'JPG');

// Load a UTF-8 string from a file and print it
$pdf->AddFont('arial','','arial.ttf',true);
$pdf->AddFont('arialbd','','arialbd.ttf',true);


/*Este es el contenido del DIPLOMA */ 
$pdf->SetTextColor(59,45,140);
$pdf->SetFont('arial','',20);
$pdf->SetXY(40,70);
$pdf->Write(8, "INBI otorga la siguiente constancia a:");
$pdf->Ln(20);


$pdf->Ln(20);
$pdf->SetFont('arial','',26);
$pdf->SetXY(10,90);
$pdf->MultiCell(0,8, $data['nombre'] , $border=0, $align="C", $fill=false);
$pdf->Ln(20);
$pdf->SetFont('arial','',20);
$pdf->SetXY(10,115);
$textoFijo = "Al haber realizado de manera satisfactoria nuestra prueba de verificación de nivel “Student´s performance test”. El examen fue evaluado usando nuestra metodología obteniendo las conclusiones expuestas a continuación.";
$pdf->MultiCell(0,8, $textoFijo , $border=0, $align="C", $fill=false);


$pdf->Ln(20);
$pdf->SetFont('arial','',25);
$pdf->SetXY(10,160);
$pdf->MultiCell(0,8, $data["niveldescrip"] , $border=0, $align="C", $fill=false);


$pdf->SetFont('arial','',12);
$pdf->Ln(10);
$pdf->SetXY(10,190);
$pdf->MultiCell(0,8, $descripcion , $border=0, $align="C", $fill=false);
$pdf->Ln(15);


/*Termina  el contenido del DIPLOMA */ 

$pdf->SetXY(10,290);
$pdf->SetFont('arial','B',10);
$pdf->Write(7,".");
$pdf->Ln(15);



/***********************************************************/
/****** Imagenes *******/

$pdf->SetXY(10,50);
$pdf->SetFont('arialbd','',11);
$pdf->SetXY(10,65);
$pdf->Write(7,"Descripción de imagen 1.");
$pdf->SetXY(10,75);
$pdf->SetFont('arial','',10);
$pdf->Write(7," Respuesta: " .$data['respuesta_imagen_1']);
if($valor_11=="nok"){
  $pdf->SetXY(10,100);
  $pdf->Write(6,"$mensajeOnce");
  $pdf->Ln(15);
}else{
  $pdf->SetXY(10,100);
  $pdf->Write(6,"Correcta");
  $pdf->Ln(15);  
}


$pdf->SetXY(10,190);
$pdf->SetFont('arialbd','',30);
$textoFijo="Datos personales";
$pdf->MultiCell(0,8, $textoFijo , $border=0, $align="C", $fill=false);
$pdf->Ln(15);

$pdf->SetXY(10,200);
$pdf->SetFont('arialbd','',11);
$pdf->Write(7,"Teléfono: ".$data['telefono']);
$pdf->Ln(15);

$pdf->SetXY(10,206);
$pdf->SetFont('arialbd','',11);
$pdf->Write(7,"Motivo: ".$data['motivo_aprender']);
$pdf->Ln(15);

$pdf->SetXY(10,212);
$pdf->SetFont('arialbd','',11);
$pdf->Write(7,"Horario: ".$data['horario']);
$pdf->Ln(15);

$pdf->SetXY(10,218);
$pdf->SetFont('arialbd','',11);
$pdf->Write(7,"sucursal: ".$nombreSucursal);
$pdf->Ln(15);

//$pdf->cell(10,10,"Descripción de imagen 2.");

//$pdf->Image('imagenes/Diapositiva16.JPG' , 40 ,250, 40 , 30,'JPG');




$pdf->Output();

?>