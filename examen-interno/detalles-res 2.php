
<?php
require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>    

<style>
#respuestaAjax {
  display:none;
}
#retornaError {
  display:none;
}
</style>
    <title>Plataforma de evaluaciones INBI</title>
  </head>
  <body>
<?php

  if (isset($_GET['a'])) {
    $a  = limpiar($_GET['a']);
  } else {
    $a      ='';
  }


  if(!empty($a)){
    $sql = "SELECT * FROM examen_ubicacion where id='".$a."'";
  }
  
  if (!$resultado = $conn->query($sql)) {
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $conn->errno . "\n";
    echo "Error: " . $conn->error . "\n";
    exit;
  }

  if ($resultado->num_rows === 0) {
    echo "No se encontraron registros. Inténtelo de nuevo.";
    exit;
  }
  $data = $resultado->fetch_assoc();

  $sucursalNombre = "SELECT nombre from sucursales where id = ".$data['sucursal']."";
  $resSucNombre   = $conn->query($sucursalNombre);
  $sucursal = $resSucNombre->fetch_assoc();
  
  // Obtener los valores de las opciones, cuando la respuesta es incorrecta
  $sqlRespuestas = "SELECT * FROM opciones_respuesta_preguntas_incorrectas";
  $resRespuestas = $conn->query($sqlRespuestas);
  
  
  while($respuestas = $resRespuestas->fetch_assoc()){
    $respuestaOpcion[] = $respuestas['id']."=>".$respuestas['nombre']."=>".$respuestas['alt'];
  }

  ?>

<div class="container"> <!--Inicia Container -->
    <div class="row">
        <div class="col-md-4"></div>
    </div>
  </div>
</div><!--Termima container-->

  <header>
      <div class="text-center">
        <img class="img-fluid" src="img/logo.jpg" width="182" height="182" Alt="">
      </div>
       <h1 class="text-center text-light">Examenes</h1>
       <h2 class="text-center text-light"> <span class="badge badge-primary">Examen de ubicacion <?php echo $data['nombre'];?></span></h2> 
  </header>    
    <div style="height:50px"></div>
     
    <!--Ejemplo tabla con DataTables-->
    <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">        
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                <tbody>
                                                     
                  <tr><td><span class="font-weight-bold">Nombre</span></td><td><?php echo $data['nombre'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Correo</span></td><td><?php echo $data['correo'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Teléfono</span></td><td><?php echo $data['telefono'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Motivo de aprender</span></td><td><?php echo $data['motivo_aprender'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Sucursal</span></td><td><?php echo $sucursal['nombre'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Horario</span></td><td><?php echo $data['horario'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Fecha Registro</span></td><td><?php echo $data['fecha_registro'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Origen de registro</span></td><td><?php echo $data['origen'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Nivel</span></td><td><?php echo empty($data['nivel']) ? 'Sin Asignar' : $data['nivel'];?></td></tr>
                </tbody>        
              </table>  
              <br/><br/>

              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><h2>Respuesta de las preguntas del examen de ubicación.</h2></td></tr>
                  <tr><td colspan="2"><h3>Parte 1: Preguntas Cotidianas.</h3></td></tr>
                  </tbody>        
              </table> 

              
              <!-- ############################# Empiezas las preguntas ################################## -->

              <form action="procesamiento-nivel.php" method="post" onsubmit="return validacionDatos()">
              
              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 1.- How old are you?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta1'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaUno" id="respuestaUno" value="ok" required> Ok </td> 
                  <td width="60%">
                   
                  <input type="radio" name="respuestaUno" id="respuestaUno" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaUno" id="opcionRespuestaUno">
                    <option value="">Seleccione una opción</option>
                    <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==1 || $valor[0]==2){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select>

                  </td>
                  </tr>
                  </tbody>        
              </table> 
              </tbody>        
              </table> 
              <br/><br/>


              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 2.- Do you have brothers and sisters?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta2'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaDos" id="respuestaDos" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaDos" id="respuestaDos" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaDos" id="opcionRespuestaDos">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==3){

                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select></td>
                  </tr>
                </tbody>        
              </table> 
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 3.- What do you prefer: dogs or cat?</span></td></tr>
                  <tr><td  width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta3'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaTres" id="respuestaTres" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaTres" id="respuestaTres" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaTres" id="opcionRespuestaTres">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==4){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select></td>
                  </tr>
                </tbody>        
              </table>
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 4.- Is it hoy outside?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta4'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaCuatro" id="respuestaCuatro" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaCuatro" id="respuestaCuatro" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaCuatro" id="opcionRespuestaCuatro">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==5){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select></td>
                  </tr>
                  </tbody>        
              </table> 
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 5.- What is your opinion about soccer?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta5'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaCinco" id="respuestaCinco" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaCinco" id="respuestaCinco" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaCinco" id="opcionRespuestaCinco">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==6 || $valor[0]==7 || $valor[0]==8){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select>
                  </td>
                  </tr>
                  </tbody>        
              </table> 
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><h3>Parte 2: Preguntas en Inglés.</h3></td></tr>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 1.- </span><audio class="asado" src="audios/pregunta-ingles-1.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_1'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaSeis" id="respuestaSeis" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaSeis" id="respuestaSeis" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaSeis" id="opcionRespuestaSeis">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==9){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>                 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 2.- </span><audio class="asado" src="audios/pregunta-ingles-2.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"> <?php echo $data['respuesta_ingles_2'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaSiete" id="respuestaSiete" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaSiete" id="respuestaSiete" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaSiete" id="opcionRespuestaSiete">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==10){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select>
                  </td>
                  </tr>
                  </tbody>        
              </table>                  
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 3.- </span><audio class="asado" src="audios/pregunta-ingles-3.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_3'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaOcho" id="respuestaOcho" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaOcho" id="respuestaOcho" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaOcho" id="opcionRespuestaOcho">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==11){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select>
                  </td>
                  </tr>
                  </tbody>        
              </table>                   
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 4.- </span><audio class="asado" src="audios/pregunta-ingles-4.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_4'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaNueve" id="respuestaNueve" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaNueve" id="respuestaNueve" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaNueve" id="opcionRespuestaNueve">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==12){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select>
                  </td>
                  </tr>
                  </tbody>        
              </table>                   
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 5.- </span><audio class="asado" src="audios/pregunta-ingles-5.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_5'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaDiez" id="respuestaDiez" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaDiez" id="respuestaDiez" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaDiez" id="opcionRespuestaDiez">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==13){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>  
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><h3>Parte 3: Descripción de imagenes.</h3></td></tr>
                  <tr><td colspan="2"><img class="img-fluid" src="imagenes/Diapositiva16.JPG" width="444" height="283" alt="examen de ingles"/></td></tr>
                  <tr><td width="40%">Respuesta: </td><td width="60%"><?php echo $data['respuesta_imagen_1'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaOnce" id="respuestaOnce" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaOnce" id="respuestaOnce" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaOnce" id="opcionRespuestaOnce">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==14 || $valor[0]==15 || $valor[0]==16 || $valor[0]==17 || $valor[0]==18 ){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>  
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>                   
                  <tr><td colspan="2"><img class="img-fluid" src="imagenes/Diapositiva17.JPG" width="444" height="283" alt="examen de ingles"/></td></tr>
                  <tr><td width="40%">Respuesta: </td><td width="60%"><?php echo $data['respuesta_imagen_2'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaDoce" id="respuestaDoce" value="nok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaDoce" id="respuestaDoce" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaDoce" id="opcionRespuestaDoce">
                  <option value="">Seleccione una opción</option>
                  <?php
                    foreach($respuestaOpcion as $fila){
                      $valor = explode("=>", $fila);
                      if($valor[0]==19 || $valor[0]==20 || $valor[0]==21 || $valor[0]==22 || $valor[0]==23 || $valor[0]==24 ){
                    ?>
                      <option value="<?php echo trim($valor[0]);?>"><?php echo $valor[1]." / ".$valor[2];?></option>
                      <?php
                      }
                    }
                    ?>
                  </select>
                  </td>
                  </tr>
                  </tbody>        
              </table>  
              <br/><br/>
              
            <!-- ############################# Terminan las preguntas ################################## -->

            </div>
          </div>
        </div>  

        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
            <label for="asignar-nivel">Asignar nivel</label>
            <select class="custom-select d-block w-100" name="asig-nivel" id="asig-nivel" required="" onchange="descripcion()"> 
              <option value="">Seleccione una opción.</option>
              <?php
              // Obtener los valores de las opciones, cuando la respuesta es incorrecta
              $sqlNE = "SELECT * FROM niveles_evaluacion";
              $resNE = $conn->query($sqlNE);
              while($n = $resNE->fetch_assoc()){
                ?>
                <option value="<?php echo $n['id'];?>"><?php echo $n['nombre'];?></option>
                <?php
              }
              ?>

            </select>
          </div>
          <div class="col-md-4">
          </div>          
        </div>

        <br/><br/>

          <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
            <div id="insertarDescripcion"></div>
            </div>
            <div class="col-md-3">
            </div>
          </div>



        <input type="hidden" id="id" name="id" value="<?php echo $data['id'];?>">
        <input type="hidden" id="unico" name="unico" value="<?php echo $data['uniq'];?>">
        <input type="hidden" id="correo" name="correo" value="<?php echo $data['correo'];?>">
        <input type="hidden" id="sucursal" name="sucursal" value="<?php echo $data['sucursal'];?>">

        <br/><br/>
        <div class="row">
          <div class="col-md-4 mb-3">
          </div>
          <div class="col-md-4 mb-3">
          <button class="btn btn-primary  btn-block" name="btnRegistrar" type="submit">Registrar la información</button>
          </div>
          <div class="col-md-4 mb-3">
          </div>
        </div>
        </form>
    </div>
  

    <br/><br/><br/>





<script src="js/jquery/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>   




<script>
function descripcion(){
  
  let d = document.getElementById("asig-nivel").value;

  if(d ==""){
    document.getElementById("insertarDescripcion").innerHTML="";
  }else{
      $.ajax({
      type: "POST",
      url: "ajax-descripcion.php",
      data: "id_nivel=" + d,
      success : function(text){
      //document.getElementById("insertarDescripcion").innerHTML=text;
      document.getElementById("insertarDescripcion").innerHTML="";
      $("#insertarDescripcion").append(text);
      
      }
    });
  }
}


function validacionDatos(){  
  /*** Validacion Pregunta 1 */
  var respuestaUno = document.getElementsByName("respuestaUno");
    for(i=0; i<respuestaUno.length; i++){
      if(respuestaUno[i].checked){
        var valorUno=respuestaUno[i].value;
    } 
  }  

  if(valorUno=="nok"){
    var listaUno = document.getElementById("opcionRespuestaUno");
    var valorSelectUno= listaUno.options[listaUno.selectedIndex].value;
    if(valorSelectUno==0){
      alert("Debe de seleccionar una opción para la respuesta 1");
      document.getElementById("opcionRespuestaUno").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 1 */


  /*** Validacion Pregunta 2 */
  var respuestaDos = document.getElementsByName("respuestaDos");
    for(i=0; i<respuestaDos.length; i++){
      if(respuestaDos[i].checked){
        var valorDos=respuestaDos[i].value;
    } 
  }  

  if(valorDos=="nok"){
    var listaDos = document.getElementById("opcionRespuestaDos");
    var valorSelectDos= listaDos.options[listaDos.selectedIndex].value;
    if(valorSelectDos==0){
      alert("Debe de seleccionar una opción para la respuesta 2");
      document.getElementById("opcionRespuestaDos").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 2 */

  /*** Validacion Pregunta 3 */
  var respuestaTres = document.getElementsByName("respuestaTres");
    for(i=0; i<respuestaTres.length; i++){
      if(respuestaTres[i].checked){
        var valorTres=respuestaTres[i].value;
    } 
  }  

  if(valorTres=="nok"){
    var listaTres = document.getElementById("opcionRespuestaTres");
    var valorSelectTres= listaTres.options[listaTres.selectedIndex].value;
    if(valorSelectTres==0){
      alert("Debe de seleccionar una opción para la respuesta 3");
      document.getElementById("opcionRespuestaTres").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 3 */


  /*** Validacion Pregunta 4 */
  var respuestaCuatro = document.getElementsByName("respuestaCuatro");
    for(i=0; i<respuestaCuatro.length; i++){
      if(respuestaCuatro[i].checked){
        var valorCuatro=respuestaCuatro[i].value;
    } 
  }  

  if(valorCuatro=="nok"){
    var listaCuatro = document.getElementById("opcionRespuestaCuatro");
    var valorSelectCuatro= listaCuatro.options[listaCuatro.selectedIndex].value;
    if(valorSelectCuatro==0){
      alert("Debe de seleccionar una opción para la respuesta 4");
      document.getElementById("opcionRespuestaCuatro").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 4 */

  /*** Validacion Pregunta 5 */
  var respuestaCinco = document.getElementsByName("respuestaCinco");
    for(i=0; i<respuestaCinco.length; i++){
      if(respuestaCinco[i].checked){
        var valorCinco=respuestaCinco[i].value;
    } 
  }  

  if(valorCinco=="nok"){
    var listaCinco = document.getElementById("opcionRespuestaCinco");
    var valorSelectCinco = listaCinco.options[listaCinco.selectedIndex].value;
    if(valorSelectCinco==0){
      alert("Debe de seleccionar una opción para la respuesta 5");
      document.getElementById("opcionRespuestaCinco").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 5 */


  /*** Validacion Pregunta 6 */
  var respuestaSeis = document.getElementsByName("respuestaSeis");
    for(i=0; i<respuestaSeis.length; i++){
      if(respuestaSeis[i].checked){
        var valorSeis=respuestaSeis[i].value;
    } 
  }  

  if(valorSeis=="nok"){
    var listaSeis = document.getElementById("opcionRespuestaSeis");
    var valorSelectSeis = listaSeis.options[listaSeis.selectedIndex].value;
    if(valorSelectSeis==0){
      alert("Debe de seleccionar una opción para la respuesta 6");
      document.getElementById("opcionRespuestaSeis").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 6 */


    /*** Validacion Pregunta 7 */
    var respuestaSiete = document.getElementsByName("respuestaSiete");
    for(i=0; i<respuestaSiete.length; i++){
      if(respuestaSiete[i].checked){
        var valorSiete=respuestaSiete[i].value;
    } 
  }  

  if(valorSiete=="nok"){
    var listaSiete = document.getElementById("opcionRespuestaSiete");
    var valorSelectSiete = listaSiete.options[listaSiete.selectedIndex].value;
    if(valorSelectSiete==0){
      alert("Debe de seleccionar una opción para la respuesta 7");
      document.getElementById("opcionRespuestaSiete").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 7 */


    /*** Validacion Pregunta 8 */
    var respuestaOcho = document.getElementsByName("respuestaOcho");
    for(i=0; i<respuestaOcho.length; i++){
      if(respuestaOcho[i].checked){
        var valorOcho=respuestaOcho[i].value;
    } 
  }  

  if(valorOcho=="nok"){
    var listaOcho = document.getElementById("opcionRespuestaOcho");
    var valorSelectOcho = listaOcho.options[listaOcho.selectedIndex].value;
    if(valorSelectOcho==0){
      alert("Debe de seleccionar una opción para la respuesta 8");
      document.getElementById("opcionRespuestaOcho").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 8 */



    /*** Validacion Pregunta 9 */
    var respuestaNueve = document.getElementsByName("respuestaNueve");
    for(i=0; i<respuestaNueve.length; i++){
      if(respuestaNueve[i].checked){
        var valorNueve=respuestaNueve[i].value;
    } 
  }  

  if(valorNueve=="nok"){
    var listaNueve = document.getElementById("opcionRespuestaNueve");
    var valorSelectNueve = listaNueve.options[listaNueve.selectedIndex].value;
    if(valorSelectNueve==0){
      alert("Debe de seleccionar una opción para la respuesta 9");
      document.getElementById("opcionRespuestaNueve").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 9 */


    /*** Validacion Pregunta 10 */
    var respuestaDiez = document.getElementsByName("respuestaDiez");
    for(i=0; i<respuestaDiez.length; i++){
      if(respuestaDiez[i].checked){
        var valorDiez=respuestaDiez[i].value;
    } 
  }  

  if(valorDiez=="nok"){
    var listaDiez = document.getElementById("opcionRespuestaDiez");
    var valorSelectDiez = listaDiez.options[listaDiez.selectedIndex].value;
    if(valorSelectDiez==0){
      alert("Debe de seleccionar una opción para la respuesta 10");
      document.getElementById("opcionRespuestaDiez").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 10 */



    /*** Validacion Pregunta 11 */
    var respuestaOnce = document.getElementsByName("respuestaOnce");
    for(i=0; i<respuestaOnce.length; i++){
      if(respuestaOnce[i].checked){
        var valorOnce=respuestaOnce[i].value;
    } 
  }  

  if(valorOnce=="nok"){
    var listaOnce = document.getElementById("opcionRespuestaOnce");
    var valorSelectOnce = listaOnce.options[listaOnce.selectedIndex].value;
    if(valorSelectOnce==0){
      alert("Debe de seleccionar una opción para la respuesta 11");
      document.getElementById("opcionRespuestaOnce").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 11 */


    /*** Validacion Pregunta 12 */
    var respuestaDoce = document.getElementsByName("respuestaDoce");
    for(i=0; i<respuestaDoce.length; i++){
      if(respuestaDoce[i].checked){
        var valorDoce=respuestaDoce[i].value;
    } 
  }  

  if(valorDoce=="nok"){
    var listaDoce = document.getElementById("opcionRespuestaDoce");
    var valorSelectDoce = listaDoce.options[listaDoce.selectedIndex].value;
    if(valorSelectDoce==0){
      alert("Debe de seleccionar una opción para la respuesta 12");
      document.getElementById("opcionRespuestaDoce").focus();
      return false;
    }
  }
  /*** Termina Validacion Pregunta 12 */




return true;
}
</script>
</body>
</html>