<?php
date_default_timezone_set('America/Monterrey');
?>

<?php
  function limpiar($a){
    $a = str_replace('‘','',$a);
    $a = str_replace('"','',$a);
    $a = str_replace('–','',$a);
    $a = str_replace('/*','',$a);
    $a = str_replace('*/','',$a);
    $a = str_replace('xp_','',$a);
    $a = str_replace('<','',$a);
    $a = str_replace('>','',$a);
    $a = str_replace('insert','',$a);
    $a = str_replace('delete','',$a);
    $a = str_replace('update','',$a);
    $a = str_replace('select','',$a);
    $a = str_replace('drop','',$a);
    $a = str_replace('table','',$a);
    $a = str_replace("'","\'",$a);
  return $a;
  }

  $hogar     = "74.208.190.136";
  $bd        = "examen_interno";
  $userHogar = "sistemas";
  $pasqui    = "%%hz@99";
                   
  $conn = new mysqli($hogar, $userHogar, $pasqui,$bd); 
  
  if ($conn->connect_errno) { 
    // Se podría contactar con uno mismo (¿email?), registrar el error, mostrar una bonita página, etc.
    // No se debe revelar información delicada
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    echo "Error: Fallo al conectarse a MySQL debido a: \n";
    echo "Errno: " . $conn->connect_errno . "\n";
    echo "Error: " . $conn->connect_error . "\n";    
    exit;
  }
  mysqli_set_charset($conn, "utf8");


  // CONEXION AL NUEVO ERP 
  // Modo local
  $host    = "74.208.190.136";
  $bdERP   = "erp_db";
  $userERP = "sistemas";
  $pasERP  = "%%hz@99";
                   
  $connERP = new mysqli( $host, $userERP, $pasERP, $bdERP ); 
  
  if ($connERP->connect_errno) { 
    // Se podría contactar con uno mismo (¿email?), registrar el error, mostrar una bonita página, etc.
    // No se debe revelar información delicada
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    echo "Error: Fallo al conectarse a MySQL debido a: \n";
    echo "Errno: " . $connERP->connect_errno . "\n";
    echo "Error: " . $connERP->connect_error . "\n";    
    exit;
  }
  mysqli_set_charset($connERP, "utf8");
?>



