<?php
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
  require 'inc/conexion.php';
?>
<!-- Black List: Buscar ip en una tabla de Black List -->
<!-- Generación de token y ID de session -->
<!-- Insertar la id y la session en la base de datos-->

<!-- Checar como esta el examen de ubicación de Grop Up -->
<!--Poner Captcha para evitar Span -->
<!--Ingresar estos datos en la base de datos -->

<!--Poder calcular el tiempo de el examen -->
<?php
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">

<head>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171876901-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-171876901-1');
  </script>
  <!-- Required meta tags -->
  <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script type="text/script" src="js/bootstrap.min.js"></script>
  <title>Plataforma de evaluaciones INBI</title>
  <style>
    .asado {
      width: 200px;
      height: 30px;
      vertical-align: middle;
    }

    .asado:active {
      border: none;
    }
  </style>
</head>


<body>
  <?php

    $accesoConcedido = false;

    $mensaje = "";

    if ($connERP->connect_errno) { 
      // No se debe revelar información delicada
      echo "Lo sentimos, este sitio web está experimentando problemas.";
      // echo "Error: Fallo al conectarse a MySQL debido a: \n";
      // echo "Errno: " . $connERP->connect_errno . "\n";
      // echo "Error: " . $connERP->connect_error . "\n";    
      exit;
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

      if(isset($_POST['folio'])){
        $folio = $_POST['folio'];

        if (validarFolio($folio, $connERP)) {
          // Si el folio es válido, realiza alguna acción
          $mensaje = "";
          $accesoConcedido = validarFolio($folio, $connERP);
        } else {
          // Si el folio no es válido, muestra un mensaje de error
          $mensaje = "El folio ingresado no es válido o ya cuenta con un registro.";
        }
      }
    }

    function validarFolio($folio, $connERP) {
      // Aquí puedes implementar la lógica de validación del folio
      // Por ejemplo, verificar su longitud, formato, caracteres permitidos, etc.
      // Devuelve true si el folio es válido, de lo contrario devuelve false
      // VALIDAR QUE EL FOLIO EXISTA

      $sql = "SELECT * FROM prospectos WHERE folio = '".$folio."' AND idexamenubicacion = 0;";

      if (!$resultado = $connERP->query($sql)) {
        return false;
      }

      $accesoConcedido = false;
      while($data = $resultado->fetch_assoc()){
        $accesoConcedido = $data['idprospectos'];
        $mensaje = "";
        return $accesoConcedido;
      }

      return false;
    }

    if( !isset($_POST['entrar']) ){
  ?>
      <div class="container">
        <!--Inicia Container -->
        <div class="row">

          <div class="col-md-12">
            <div class="text-center">
              <img class="img-fluid" src="img/logo.jpg" width="182" height="182" Alt="">
            </div>

            <div class="text-center">
              <h3>Examen de ubicación idioma Inglés</h3>
            </div>
          </div>
        </div>

        <br/>

        <?php 
          if(!$accesoConcedido){
          ?>
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                <form action="" method="POST" name="buscarFolio" id="buscarFolio">
                  <div class="form-group">
                    <label for="folio">
                      <div class="text-center">
                        <h5>Ingresa un FOLIO para continuar</h5>
                      </div>
                    </label>
                    <input type="text" class="form-control" name="folio" id="folio" required>
                  </div>
                  <div class="text-center">
                    <button type="submit" id="buscarFolio" name="buscarFolio" class="btn btn-primary">Validar folio</button>
                  </div>
                </form>

                <?php 
                  if( $mensaje ){ 
                ?>
                    <br/>
                    <!-- Poner aquí el mensaje de que hay un error -->
                    <div class="alert alert-danger" role="alert">
                      <?php echo $mensaje ?>
                    </div>
                <?php
                  }
                ?>
              </div>
            </div>
          <?php 
          }
        ?>


      <!-- Validar si ya se haya validado el folio primero -->
      <?php 
        if( $accesoConcedido ){
        ?> 
          <!--Inicia el formulario -->
          <div class="row">

            <div class="col-md-2"></div>
            <div class="col-md-8"><br /><br /><br />
              <span class="font-weight-bold">Instrucciones: </span> / <audio class="asado" src="audios/bienvenido.mp3" controls>
                <p>Tu navegador no implementa el elemento audio</p>
              </audio>
              <br />
              <img class="img-fluid" src="imagenes/Diapositiva1.PNG" width="730" height="490" Alt="">
              <form action="" method="POST" onsubmit="return evitarDuplicidadEnvio();" name="forma" id="forma">

                <input type="text" class="form-control" name="folio2" id="folio2" required value="<?php echo $accesoConcedido ?>" hidden>

                <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" required >
                </div>

                <div class="form-group">
                  <label for="correo">Correo</label>
                  <input type="email" class="form-control" name="correo" id="correo" required>
                </div>

                <div class="form-group">
                  <label for="telefono">Teléfono</label>
                  <input type="text" class="form-control" name="telefono" id="telefono" required>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlSelect1">¿Por qué quiere aprender Inglés?</label>
                  <select class="form-control" name="motivo_aprender" id="motivo_aprender" required>
                    <option value="">Seleccione una opción</option>
                    <option value="academico">Académico</option>
                    <option value="trabajo">Trabajo</option>
                    <option value="ocio">Ocio</option>
                  </select>
                </div>
                <?php
                  $sql = "SELECT * FROM sucursales";
                  if (!$resultado = $conn->query($sql)) {
                    echo "Error al buscar las suursales";
                    exit;
                  }
                ?>
                <div class="form-group">
                  <label for="exampleFormControlSelect1">Sucursal</label>
                  <select class="form-control" name="sucursal" id="sucursal" required>
                    <option value="">Seleccione una opción</option>
                    <?php
                      while ($data = $resultado->fetch_assoc()) {
                    ?>
                      <option value="<?php echo $data['id']; ?>"><?php echo $data['nombre']; ?></option>
                    <?php
                      }
                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlSelect1">Horario</label>
                  <select class="form-control" name="horario" id="horario" required>
                    <option value="">Seleccione una opción</option>
                    <option value="manana">Mañana</option>
                    <option value="tarde">Tarde</option>
                    <option value="sabatino">Sabatino</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlSelect1"><b>¿Cómo consideras tu nivel de inglés?</b></label>
                  <select class="form-control" name="nivel_ingles" id="nivel_ingles" required>
                    <option value="">Seleccione una opción</option>
                    <option value="bajo">Bajo</option>
                    <option value="medio">Medio</option>
                    <option value="alto">Alto</option>
                  </select>
                </div>


                <h3>Sección 1: Uso de las estructuras del idioma</h3>
                <br />

               <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 1.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva3.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p1" id="memoria1" value="0" required><b> a) a few</b><br/>
                  <input type= "radio" name="parte1p1" id="memoria2" value="1" required><b> b) much</b><br/>
                  <input type= "radio" name="parte1p1" id="memoria3" value="2" required><b> c) a lot of</b><br/>
                  <input type= "radio" name="parte1p1" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
                </div>
                <br /><br />

                 <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 2.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva4.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p2" id="memoria1" value="0" required><b> a)  drink</b><br/>
                  <input type= "radio" name="parte1p2" id="memoria2" value="1" required><b> b)  drinks</b><br/>
                  <input type= "radio" name="parte1p2" id="memoria3" value="2" required><b> c)  drinking</b><br/>
                  <input type= "radio" name="parte1p2" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />
                
                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 3.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva5.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p3" id="memoria1" value="0" required><b> a)  haven´t they?</b><br/>
                  <input type= "radio" name="parte1p3" id="memoria2" value="1" required><b> b)  don´t they?</b><br/>
                  <input type= "radio" name="parte1p3" id="memoria3" value="2" required><b> c)  hasn´t they?</b><br/>
                  <input type= "radio" name="parte1p3" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 4.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva6.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p4" id="memoria1" value="0" required><b> a)  the biggest</b><br/>
                  <input type= "radio" name="parte1p4" id="memoria2" value="1" required><b> b)  the bigger</b><br/>
                  <input type= "radio" name="parte1p4" id="memoria3" value="2" required><b> c)  the most big</b><br/>
                  <input type= "radio" name="parte1p4" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 5.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva7.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p5" id="memoria1" value="0" required><b> a)  this</b><br/>
                  <input type= "radio" name="parte1p5" id="memoria2" value="1" required><b> b)  these</b><br/>
                  <input type= "radio" name="parte1p5" id="memoria3" value="2" required><b> c)  those</b><br/>
                  <input type= "radio" name="parte1p5" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 6.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva8.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p6" id="memoria1" value="0" required><b> a)  buy</b><br/>
                  <input type= "radio" name="parte1p6" id="memoria2" value="1" required><b> b)  buying</b><br/>
                  <input type= "radio" name="parte1p6" id="memoria3" value="2" required><b> c)  bought</b><br/>
                  <input type= "radio" name="parte1p6" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 7.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva9.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p7" id="memoria1" value="0" required><b> a)  neither….nor</b><br/>
                  <input type= "radio" name="parte1p7" id="memoria2" value="1" required><b> b)  either….or</b><br/>
                  <input type= "radio" name="parte1p7" id="memoria3" value="2" required><b> c)  not only…. But also</b><br/>
                  <input type= "radio" name="parte1p7" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 8.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva10.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p8" id="memoria1" value="0" required><b> a)  sunny</b><br/>
                  <input type= "radio" name="parte1p8" id="memoria2" value="1" required><b> b)  cloudy</b><br/>
                  <input type= "radio" name="parte1p8" id="memoria3" value="2" required><b> c)  rainy</b><br/>
                  <input type= "radio" name="parte1p8" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 9.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva11.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p9" id="memoria1" value="0" required><b> a)  outgoing</b><br/>
                  <input type= "radio" name="parte1p9" id="memoria2" value="1" required><b> b)  greedy</b><br/>
                  <input type= "radio" name="parte1p9" id="memoria3" value="2" required><b> c)  lazy</b><br/>
                  <input type= "radio" name="parte1p9" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 10.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva12.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p10" id="memoria1" value="0" required><b> a)  get off</b><br/>
                  <input type= "radio" name="parte1p10" id="memoria2" value="1" required><b> b)  get down</b><br/>
                  <input type= "radio" name="parte1p10" id="memoria3" value="2" required><b> c)  get out</b><br/>
                  <input type= "radio" name="parte1p10" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <h3>Sección 2: Comprensión auditiva</h3>
                  <img class="img-fluid" src="imagenes/Diapositiva13.PNG" width="644" height="483" alt="examen de ingles"/>
                <br />
                <br />
                <br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 11.- <audio class="asado" src="audios/Pregunta 11.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva14.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p1" id="memoria1" value="0" required><b> a)  A stereo and a bookshelf</b><br/>
                  <input type= "radio" name="parte2p1" id="memoria2" value="1" required><b> b)  A bookshelf and a table</b><br/>
                  <input type= "radio" name="parte2p1" id="memoria3" value="2" required><b> c)  A stereo and a sofa</b><br/>
                  <input type= "radio" name="parte2p1" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 12.- <audio class="asado" src="audios/Pregunta 12.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva15.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p2" id="memoria1" value="0" required><b> a)  He wanted to buy some shorts</b><br/>
                  <input type= "radio" name="parte2p2" id="memoria2" value="1" required><b> b)  They agreed</b><br/>
                  <input type= "radio" name="parte2p2" id="memoria3" value="2" required><b> c)  He was looking for some pens</b><br/>
                  <input type= "radio" name="parte2p2" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />


                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 13.- <audio class="asado" src="audios/Pregunta 13.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva16.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p3" id="memoria1" value="0" required><b> a)  He doesn´t have energy anymore</b><br/>
                  <input type= "radio" name="parte2p3" id="memoria2" value="1" required><b> b)  He is bored</b><br/>
                  <input type= "radio" name="parte2p3" id="memoria3" value="2" required><b> c)  He is boring</b><br/>
                  <input type= "radio" name="parte2p3" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 14.- <audio class="asado" src="audios/Pregunta 14.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva17.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p4" id="memoria1" value="0" required><b> a)  Because he likes to spend money</b><br/>
                  <input type= "radio" name="parte2p4" id="memoria2" value="1" required><b> b)  Because he wants a raise</b><br/>
                  <input type= "radio" name="parte2p4" id="memoria3" value="2" required><b> c)  Because his wage is great</b><br/>
                  <input type= "radio" name="parte2p4" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 15.- <audio class="asado" src="audios/Pregunta 15.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva18.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p5" id="memoria1" value="0" required><b> a)  That it is an interested job</b><br/>
                  <input type= "radio" name="parte2p5" id="memoria2" value="1" required><b> b)  that he prefers working with computers instead of peolple</b><br/>
                  <input type= "radio" name="parte2p5" id="memoria3" value="2" required><b> c)  That his job is so interesting</b><br/>
                  <input type= "radio" name="parte2p5" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 16.- <audio class="asado" src="audios/Pregunta 16.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva19.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p6" id="memoria1" value="0" required><b> a)  They didn´t know where to place some items</b><br/>
                  <input type= "radio" name="parte2p6" id="memoria2" value="1" required><b> b)  They were talking about how great the magazines were</b><br/>
                  <input type= "radio" name="parte2p6" id="memoria3" value="2" required><b> c)  He was looking for a new dictionary</b><br/>
                  <input type= "radio" name="parte2p6" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 17.- <audio class="asado" src="audios/Pregunta 17.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva20.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte2p7" id="memoria1" value="0" required><b> a)  The man wanted to take them out</b><br/>
                  <input type= "radio" name="parte2p7" id="memoria2" value="1" required><b> b)  The girl wanted to stay longer around the neighborhood</b><br/>
                  <input type= "radio" name="parte2p7" id="memoria3" value="2" required><b> c)  The man wanted to know how long they would be there</b><br/>
                  <input type= "radio" name="parte2p7" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <h3>Sección 3: Identificar coherencia en el idioma</h3>
                <span class="font-weight-bold">Instrucciones Parte 3: </span>
                <img class="img-fluid" src="imagenes/Diapositiva21.PNG" width="644" height="483" alt="examen de ingles"/>
                <br /><br />
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 18.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva22.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p1" id="memoria1" value="0" required><b> a)  grandparents</b><br/>
                  <input type= "radio" name="parte3p1" id="memoria2" value="1" required><b> b)  joining</b><br/>
                  <input type= "radio" name="parte3p1" id="memoria3" value="2" required><b> c)  in</b><br/>
                  <input type= "radio" name="parte3p1" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 19.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Pregunta 19.png" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p2" id="memoria1" value="0" required><b> a)  use</b><br/>
                  <input type= "radio" name="parte3p2" id="memoria2" value="1" required><b> b)  most</b><br/>
                  <input type= "radio" name="parte3p2" id="memoria3" value="2" required><b> c)  among</b><br/>
                  <input type= "radio" name="parte3p2" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 20.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva24.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p3" id="memoria1" value="0" required><b> a)  can</b><br/>
                  <input type= "radio" name="parte3p3" id="memoria2" value="1" required><b> b)  do</b><br/>
                  <input type= "radio" name="parte3p3" id="memoria3" value="2" required><b> c)  as</b><br/>
                  <input type= "radio" name="parte3p3" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 21.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva25.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p4" id="memoria1" value="0" required><b> a)  a</b><br/>
                  <input type= "radio" name="parte3p4" id="memoria2" value="1" required><b> b)  influence</b><br/>
                  <input type= "radio" name="parte3p4" id="memoria3" value="2" required><b> c)  his</b><br/>
                  <input type= "radio" name="parte3p4" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 22.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva26.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p5" id="memoria1" value="0" required><b> a)  since</b><br/>
                  <input type= "radio" name="parte3p5" id="memoria2" value="1" required><b> b)  easily</b><br/>
                  <input type= "radio" name="parte3p5" id="memoria3" value="2" required><b> c)  of</b><br/>
                  <input type= "radio" name="parte3p5" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 23.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva27.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p6" id="memoria1" value="0" required><b> a)  noisy</b><br/>
                  <input type= "radio" name="parte3p6" id="memoria2" value="1" required><b> b)  pollution</b><br/>
                  <input type= "radio" name="parte3p6" id="memoria3" value="2" required><b> c)  also</b><br/>
                  <input type= "radio" name="parte3p6" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 24.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva28.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <input type= "radio" name="parte3p7" id="memoria1" value="0" required><b> a)  among</b><br/>
                  <input type= "radio" name="parte3p7" id="memoria2" value="1" required><b> b)  best</b><br/>
                  <input type= "radio" name="parte3p7" id="memoria3" value="2" required><b> c)  on</b><br/>
                  <input type= "radio" name="parte3p7" id="memoria4" value="3" required><b> d)  No se cómo se debe contestar</b>
                </div>
                <br /><br />

                <!-- INSTRUCCIONES TRES -->
                <h3>Sección 4: Generación de ideas</h3>
                  <img class="img-fluid" src="imagenes/Diapositiva29.PNG" width="644" height="483" alt="examen de ingles"/>
                <br /><br />
                <!-- TERMINA INSTRUCCIONES TRES -->

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      QUESTION 25.- 
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/Diapositiva30.PNG" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <textarea class="form-control" name="respuesta_imagen_1" id="respuesta_imagen_1" rows="3" required></textarea>
                </div>
                <br /><br />

                <style>
                  #enviando {
                    display: none;
                    text-align: center;
                  }
                </style>

                <div class="text-center">
                  <button type="submit" id="entrar" name="entrar" class="btn btn-primary">Finalizar</button>
                </div>

                <div class="form-group">
                  <div class="text-center">
                    <table width="100%">
                      <tr>
                        <td width="33%"></td>
                        <td width="33%"><button id="enviando" type="button" class="btn btn-info"> Enviando formulario </button></td>
                        <td width="33%"></td>
                      </tr>
                    </table>

                  </div>
                </div>

              </form>
              <br /><br />

            </div>
            <div class="col-md-2"></div>
          </div>
        <?php
        }
      ?>
      <!-- Termina el formulario-->
    </div>
    <!--Termima container-->
    <script>
      function verRespuesta(){
        var sum = 0;
        let parte1p1  = document.getElementsByName('parte1p1');
        let parte1p2  = document.getElementsByName('parte1p2');
        let parte1p3  = document.getElementsByName('parte1p3');
        let parte1p4  = document.getElementsByName('parte1p4');
        let parte1p5  = document.getElementsByName('parte1p5');
        let parte1p6  = document.getElementsByName('parte1p6');
        let parte1p7  = document.getElementsByName('parte1p7');
        let parte1p8  = document.getElementsByName('parte1p8');
        let parte1p9  = document.getElementsByName('parte1p9');
        let parte1p10 = document.getElementsByName('parte1p10');

        let parte2p1 = document.getElementsByName('parte2p1');
        let parte2p2 = document.getElementsByName('parte2p2');
        let parte2p3 = document.getElementsByName('parte2p3');
        let parte2p4 = document.getElementsByName('parte2p4');
        let parte2p5 = document.getElementsByName('parte2p5');
        let parte2p6 = document.getElementsByName('parte2p6');
        let parte2p7 = document.getElementsByName('parte2p7');
        
        let parte3p1 = document.getElementsByName('parte3p1');
        let parte3p2 = document.getElementsByName('parte3p2');
        let parte3p3 = document.getElementsByName('parte3p3');
        let parte3p4 = document.getElementsByName('parte3p4');
        let parte3p5 = document.getElementsByName('parte3p5');
        let parte3p6 = document.getElementsByName('parte3p6');
        let parte3p7 = document.getElementsByName('parte3p7');
        
        if(parte1p1[2].checked){ sum += 1; }
        if(parte1p2[1].checked){ sum += 1; }
        if(parte1p3[1].checked){ sum += 1; }
        if(parte1p4[0].checked){ sum += 1; }
        if(parte1p5[2].checked){ sum += 1; }
        if(parte1p6[2].checked){ sum += 1; }
        if(parte1p7[0].checked){ sum += 1; }
        if(parte1p8[1].checked){ sum += 1; }
        if(parte1p9[2].checked){ sum += 1; }
        if(parte1p10[0].checked){ sum += 1; }

        if(parte2p1[2].checked){ sum += 1; }
        if(parte2p2[1].checked){ sum += 1; }
        if(parte2p3[1].checked){ sum += 1; }
        if(parte2p4[2].checked){ sum += 1; }
        if(parte2p5[2].checked){ sum += 1; }
        if(parte2p6[0].checked){ sum += 1; }
        if(parte2p7[2].checked){ sum += 1; }

        if(parte3p1[2].checked){ sum += 1; }
        if(parte3p2[0].checked){ sum += 1; }
        if(parte3p3[2].checked){ sum += 1; }
        if(parte3p4[2].checked){ sum += 1; }
        if(parte3p5[0].checked){ sum += 1; }
        if(parte3p6[1].checked){ sum += 1; }
        if(parte3p7[2].checked){ sum += 1; }
      }
    </script>

  <?php  
  }else{

    $sum = 0;

    $parte1p1 = limpiar($_POST['parte1p1']);
    $parte1p2 = limpiar($_POST['parte1p2']);
    $parte1p3 = limpiar($_POST['parte1p3']);
    $parte1p4 = limpiar($_POST['parte1p4']);
    $parte1p5 = limpiar($_POST['parte1p5']);
    $parte1p6 = limpiar($_POST['parte1p6']);
    $parte1p7 = limpiar($_POST['parte1p7']);
    $parte1p8 = limpiar($_POST['parte1p8']);
    $parte1p9 = limpiar($_POST['parte1p9']);
    $parte1p10 = limpiar($_POST['parte1p10']);

    $parte2p1 = limpiar($_POST['parte2p1']);
    $parte2p2 = limpiar($_POST['parte2p2']);
    $parte2p3 = limpiar($_POST['parte2p3']);
    $parte2p4 = limpiar($_POST['parte2p4']);
    $parte2p5 = limpiar($_POST['parte2p5']);
    $parte2p6 = limpiar($_POST['parte2p6']);
    $parte2p7 = limpiar($_POST['parte2p7']);

    $parte3p1 = limpiar($_POST['parte3p1']);
    $parte3p2 = limpiar($_POST['parte3p2']);
    $parte3p3 = limpiar($_POST['parte3p3']);
    $parte3p4 = limpiar($_POST['parte3p4']);
    $parte3p5 = limpiar($_POST['parte3p5']);
    $parte3p6 = limpiar($_POST['parte3p6']);
    $parte3p7 = limpiar($_POST['parte3p7']);
    
    if($parte1p1 == 2){ $sum += 1; }
    if($parte1p2 == 1){ $sum += 1; }
    if($parte1p3 == 1){ $sum += 1; }
    if($parte1p4 == 0){ $sum += 1; }
    if($parte1p5 == 2){ $sum += 1; }
    if($parte1p6 == 2){ $sum += 1; }
    if($parte1p7 == 0){ $sum += 1; }
    if($parte1p8 == 1){ $sum += 1; }
    if($parte1p9 == 2){ $sum += 1; }
    if($parte1p10 == 0){ $sum += 1; }

    if($parte2p1 == 2){ $sum += 1; }
    if($parte2p2 == 1){ $sum += 1; }
    if($parte2p3 == 1){ $sum += 1; }
    if($parte2p4 == 2){ $sum += 1; }
    if($parte2p5 == 2){ $sum += 1; }
    if($parte2p6 == 0){ $sum += 1; }
    if($parte2p7 == 2){ $sum += 1; }

    if($parte3p1 == 2){ $sum += 1; }
    if($parte3p2 == 0){ $sum += 1; }
    if($parte3p3 == 2){ $sum += 1; }
    if($parte3p4 == 2){ $sum += 1; }
    if($parte3p5 == 0){ $sum += 1; }
    if($parte3p6 == 1){ $sum += 1; }
    if($parte3p7 == 2){ $sum += 1; }

    $nombre          = limpiar($_POST['nombre']);
    $correo          = limpiar($_POST['correo']);
    $telefono        = limpiar($_POST['telefono']);
    $motivo_aprender = limpiar($_POST['motivo_aprender']);
    $sucursal        = limpiar($_POST['sucursal']);
    $nivel_ingles    = limpiar($_POST['nivel_ingles']);
    $idprospectos    = limpiar($_POST['folio2']);
    $sqlNombreSucursal="SELECT nombre FROM sucursales WHERE id=$sucursal";

    if (!$resultado = $conn->query($sqlNombreSucursal)) {
      $nombreSucursal ="SIN SUCURSAL";
    }

    $data = $resultado->fetch_assoc();
    $nombreSucursal = $data['nombre'];

    $horario         = limpiar($_POST['horario']);
    switch($horario){
      case "manana":
        $nombreHorario="Mañana";
        break;
      case "tarde":
        $nombreHorario="Tarde";
        break;
      case "sabatino":
        $nombreHorario="Sabatino";
        break;
      default:
      $nombreHorario="SIN HORARIO";
      break;
    }

    $fecha              = date("Y-m-d H:i:s");
    $uniq               = uniqid();
    $origen             = "WEB";
    $respuesta_imagen_1 = limpiar($_POST['respuesta_imagen_1']);

    $parte1p1 = limpiar($_POST['parte1p1']);

    if (mysqli_connect_errno()) {
        echo "error al conectar la base de datos";
        exit();
    }

    $sql = "INSERT INTO examen_ubicacion (
      uniq,
      nombre,
      correo,telefono,motivo_aprender,sucursal,horario,nivel_ingles,fecha_registro,origen,respuesta_imagen_1,idprospectos,cant) VALUES (
      '".$uniq."',
      '".$nombre."',
      '".$correo."',
      '".$telefono."',
      '".$motivo_aprender."',
      ".$sucursal.",
      '".$horario."',
      '".$nivel_ingles."',
      '".$fecha."',
      '".$origen."',
      '".$respuesta_imagen_1."',
      ".$idprospectos.",
      '".$sum."')";

      $sqlERP = "UPDATE prospectos SET idexamenubicacion = '".$uniq."' WHERE idprospectos = ".$idprospectos.";";

    if ($connERP->query($sqlERP) === TRUE) {
      echo "";
    }else{
      echo "Error en la inserción: " . $connERP->error;
      echo "No se pudieron ingresar los datos, contácta a sistemas";
      exit;
    }
    

    if ($conn->query($sql) === TRUE) {

      $to      = 'inbiventas@gmail.com,inbiventas1@gmail.com,armando.molina@inbi.mx,informes@inbi.mx,devany.molina@inbi.mx';
      $subject = 'Registro de examen de ubicación Inbi.';


      $headers = "From: " . $correo. "\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

      $message = "
        <!doctype html>
        <html lang=\"en\">
          <head>
            <title>Registro de examen de ubicación.</title>
        <style>
          body {
            margin: 0;
            padding: 0;
          }
          h1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 110%;
            font-weight: bold;
            color: #2E86C1;
          }

          h2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 100%;
            font-weight: bold;
            color: #2E86C1 ;
          }
        p{
          color: #666;
          font-family: Arial, Helvetica, sans-serif;
        }
          .center {
            position: relative;
            margin: auto;
            padding: auto;
            text-align: center;
          }
          .contenedor {
            width: 80%;
            height: auto;
            padding: 0;
            margin: auto;
            background-color: #fff;
          }
          .centro {
            width: 200px;
            height: 30px;
            vertical-align: middle;
          }
          .asado:active {
            border: none;
          }
          .linea {
            width: 100%;
        height: 1px;
        border: 1px solid #cdcdcd;
          }
          .go {
            background-color:#FF5733;
            color:#fff;
            border:none;
            padding:10px 15px 10px 15px;
            font-family: Arial, Helvetica, sans-serif;
          }
          .dato-title {
            font-size: 100%;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
          }
          .dato-description {
            font-size: 100%;
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
          }

        </style>


        </head>
        <body>
          <div class=\"contenedor\">
            <h1>INBI SCHOOL</h1>
            
            <h2>¡Informacion de la persona que contesto el examen de ubicación de INBI SCHOOL!</h2>

            <p>".$nombre." a contestao el examen de evaluación, este correo es una notificación con la información basica, 
            si quiere consultar el registro completo haga click en el boton Ir al registro. </p>
            <br/><br/><br/>

              <table cellpadding=\"8\">
              <tr style=\'background: #eee;\'><td><span class=\"dato-title\">Name:</span> </td><td><span class=\"dato-description\">" . $nombre . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Correo:</span></td><td><span class=\"dato-description\">" . $correo . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Teléfono:</span> </td><td><span class=\"dato-description\">" . $telefono . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Motivos de aprendizaje:</span> </td><td><span class=\"dato-description\">" . $motivo_aprender . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Horarios que solicita:</span> </td><td><span class=\"dato-description\">" . $nombreHorario . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Horarios que solicita:</span> </td><td><span class=\"dato-description\">" . $nombreSucursal . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Fecha:</span> </td><td><span class=\"dato-description\">" . $fecha . "</span></td></tr>
              </table>
              <br/><br/>
              <div class=\"center\">
                
              </div>

          </div>
        </body>
        </html>
        ";

      mail($to, $subject, $message, $headers);
    ?>

    <!-- <span class=\"go\"><a href=\"https://www.fastenglish.com.mx/prueba-url.php?v=".$uniq."\">Ir al registro</a></span> -->
    <br/>
    <br/>
    <br/><br/>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
        <div class="text-center">
          <img src="img/logo.jpg">
        </div>
        <p class="text-center">
                      Has contestado el examen con éxito, un representante de nuestra escuela se comunicará contigo a la brevedad al número que nos proporcionaste. 
                      De igual manera debes estar al pendiente de tú correo para los avisos referentes a futuros exámenes. 
                      Gracias por tú participación.</p>
        </div>
        <div class="col-md-3">
        </div>
      </div>
    </div>
    <script>
      // setTimeout(function(){ window.location.href = "https://www.fastenglish.com.mx"; }, 10000);
    </script>

  <?php
    } else {
      echo "Error al registrar, los datos del examen.";
     
    }

    $conn->close();
    $connERP->close();
    }
  ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>