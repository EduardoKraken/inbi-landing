<?php
require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>    


    <title>Plataforma de evaluaciones INBI</title>
  </head>
  <body>
<?php

  if (isset($_GET['id'])) {
    $id  = limpiar($_GET['id']);
  } else {
    $id      ='';
  }


  if(!empty($id)){
    $sql = "SELECT * FROM examen_ubicacion where id=$id";
  }
  
  if (!$resultado = $conn->query($sql)) {
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $conn->errno . "\n";
    echo "Error: " . $conn->error . "\n";
    exit;
  }

  if ($resultado->num_rows === 0) {
    echo "No se encontraron registros. Inténtelo de nuevo.";
    exit;
  }
  $data = $resultado->fetch_assoc();

  $sucursalNombre = "SELECT nombre from sucursales where id = ".$data['sucursal']."";
  $resSucNombre   = $conn->query($sucursalNombre);
  $sucursal = $resSucNombre->fetch_assoc();
  
  ?>

<div class="container"> <!--Inicia Container -->
    <div class="row">
        <div class="col-md-4"></div>
    </div>
  </div>
</div><!--Termima container-->

    <header>
      <div class="text-center">
        <img class="img-fluid" src="img/logo.jpg" width="182" height="182" Alt="">
      </div>
         <h1 class="text-center text-light">Examenes</h1>
         <h2 class="text-center text-light"> <span class="badge badge-primary">Examen de ubicacion <?php echo $data['nombre'];?></span></h2> 
    </header>    
    
    <div style="height:50px"></div>
     
    <!--Ejemplo tabla con DataTables-->
    <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">        
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                <tbody>
                                                     
                  <tr><td><span class="font-weight-bold">Nombre</span></td><td><?php echo $data['nombre'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Correo</span></td><td><?php echo $data['correo'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Teléfono</span></td><td><?php echo $data['telefono'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Motivo de aprender</span></td><td><?php echo $data['motivo_aprender'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Sucursal</span></td><td><?php echo $sucursal['nombre'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Horario</span></td><td><?php echo $data['horario'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Fecha Registro</span></td><td><?php echo $data['fecha_registro'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Origen de registro</span></td><td><?php echo $data['origen'];?></td></tr>
                
                  <form action="actualizar-nivel.php" method="post">
                  <tr><td><span class="font-weight-bold">Nivel</span></td><td>
                  <div style="width:30%">
                  <?php
                  if(!empty($data['nivel'])){
                    $ssN= "select id,nombre from niveles_evaluacion";
                    $rnn   = $conn->query($ssN);
                    ?>
                    <select class="form-control" name="nivel_asignado" id="nivel_asignado" required>
                        <option value="">Seleccione una opción</option>
                        <?php
                        while($nivel = $rnn->fetch_assoc()){
                          if($nivel['id']==$data['nivel']){
                          ?>
                          <option value="<?=$nivel['id']?>" selected><?=$nivel['nombre']?></option>
                          <?php
                          }else{
                            ?>
                            <option value="<?=$nivel['id']?>"><?=$nivel['nombre']?></option>
                            <?php
                          }  
                        }
                        ?>
                    </select>
                    <?php
                  }else{
                    echo "Sin asignar";
                  }
                  ?>
                  </div>
                  </td></tr>

                  <tr>
                  <td><input type="hidden" name="idAlumno" id="idAlumno" value="<?=$id;?>"></td>
                  <td><div style="width:30%"><button type="submit" class="btn btn-primary btn-sm" name="btnEditarNivel">Actualizar Nivel</button></div></td>
                  </tr>
                  </form>

                
                </tbody>        
              </table>  
              <br/><br/>

              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
				 <tr><td colspan="2"><h2>Respuesta de las preguntas del examen de ubicación.</h2></td></tr>
              	 <tr><td colspan="2"><h3>Inicisos del 1 al 24: <?php echo $data['cant'];?></h3></td></tr>
                  <tr><td colspan="2"><h3>Parte 3: Descripción de imagenes.</h3></td></tr>
                  <tr><td width="40%">Respuesta: </td><td width="60%"><?php echo $data['respuesta_imagen_1'];?></td></tr>
                  </tbody>        
              </table>  
              <br/><br/>

            </div>
          </div>
        </div>  

        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">

          </div>
          <div class="col-md-4">
          </div>          
        </div>

        <br/><br/>
      
    </div>
  

    <br/><br/><br/>





<script src="js/jquery/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>   

</body>
</html>