<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Examen de ubicación";
    $canonical = "<link rel='canonical' href='https://inbi.mx/examen_ubicacion_pagina'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/examen_ubicacion_pagina.php';
    include 'includes/footers/footer.php';
?>