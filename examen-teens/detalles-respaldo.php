<?php
require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>    

<style>
#respuestaAjax {
  display:none;
}
#retornaError {
  display:none;
}
</style>
    <title>Plataforma de evaluaciones INBI</title>
  </head>
  <body>
<?php


  if (isset($_GET['a'])) {
    $a  = limpiar($_GET['a']);
  } else {
    $a      ='';
  }


  if(!empty($a)){
    $sql = "SELECT * FROM examen_ubicacion where id='".$a."'";
  }
  
  if (!$resultado = $conn->query($sql)) {
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $conn->errno . "\n";
    echo "Error: " . $conn->error . "\n";
    exit;
  }

  if ($resultado->num_rows === 0) {
    echo "No se encontraron registros. Inténtelo de nuevo.";
    exit;
  }
  $data = $resultado->fetch_assoc();

  $sucursalNombre = "SELECT nombre from sucursales where id = ".$data['sucursal']."";
  $resSucNombre   = $conn->query($sucursalNombre);
  $sucursal = $resSucNombre->fetch_assoc();
  
  $conn->close();
?>

<div class="container"> <!--Inicia Container -->
    <div class="row">
        <div class="col-md-4"></div>
    </div>
  </div>
</div><!--Termima container-->

<header>
         <h1 class="text-center text-light">Examenes</h1>
         <h2 class="text-center text-light"> <span class="badge badge-primary">Examen de ubicacion <?php echo $data['nombre'];?></span></h2> 
     </header>    
    <div style="height:50px"></div>
     
    <!--Ejemplo tabla con DataTables-->
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">        
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                <tbody>
                                                     
                  <tr><td><span class="font-weight-bold">Nombre</span></td><td><?php echo $data['nombre'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Correo</span></td><td><?php echo $data['correo'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Teléfono</span></td><td><?php echo $data['telefono'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Motivo de aprender</span></td><td><?php echo $data['motivo_aprender'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Sucursal</span></td><td><?php echo $sucursal['nombre'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Horario</span></td><td><?php echo $data['horario'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Fecha Registro</span></td><td><?php echo $data['fecha_registro'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Origen de registro</span></td><td><?php echo $data['origen'];?></td></tr>
                  <tr><td><span class="font-weight-bold">Nivel</span></td><td><?php echo empty($data['nivel']) ? 'Sin Asignar' : $data['nivel'];?></td></tr>
                </tbody>        
              </table>  
              <br/><br/>

              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><h2>Respuesta de las preguntas del examen de ubicación.</h2></td></tr>
                  <tr><td colspan="2"><h3>Parte 1: Preguntas Cotidianas.</h3></td></tr>
                  </tbody>        
              </table> 

              
              <!-- ############################# Empiezas las preguntas ################################## -->


              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 1.- How old are you?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta1'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaUno" id="respuestaUno" value="ok" required> Ok </td> 
                  <td width="60%">
                   
                  <input type="radio" name="respuestaUno" id="respuestaUno" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaUno" id="opcionRespuestaUno">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select>
                
                  </td>
                  </tr>
                  </tbody>        
              </table> 
              </tbody>        
              </table> 
              <br/><br/>


              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 2.- Do you have brothers and sisters?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta2'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaDos" id="respuestaDos" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaDos" id="respuestaDos" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaDos" id="opcionRespuestaDos">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                </tbody>        
              </table> 
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 3.- What do you prefer: dogs or cat?</span></td></tr>
                  <tr><td  width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta3'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaTres" id="respuestaTres" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaTres" id="respuestaTres" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaTres" id="opcionRespuestaTres">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                </tbody>        
              </table>
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 4.- Is it hoy outside?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta4'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaCuatro" id="respuestaCuatro" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaCuatro" id="respuestaCuatro" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaCuatro" id="opcionRespuestaCuatro">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table> 
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 5.- What is your opinion about soccer?</span></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta5'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaCinco" id="respuestaCinco" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaCinco" id="respuestaCinco" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaCinco" id="opcionRespuestaCinco">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table> 
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>
                  <tr><td colspan="2"><h3>Parte 2: Preguntas en Inglés.</h3></td></tr>
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 1.- </span><audio class="asado" src="audios/pregunta-ingles-1.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_1'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaSeis" id="respuestaSeis" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaSeis" id="respuestaSeis" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaSeis" id="opcionRespuestaSeis">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>                 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 2.- </span><audio class="asado" src="audios/pregunta-ingles-2.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"> <?php echo $data['respuesta_ingles_2'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaSiete" id="respuestaSiete" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaSiete" id="respuestaSiete" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaSiete" id="opcionRespuestaSiete">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>                  
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 3.- </span><audio class="asado" src="audios/pregunta-ingles-3.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_3'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaOcho" id="respuestaOcho" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaOcho" id="respuestaOcho" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaSiete" id="opcionRespuestaSiete">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>                   
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 4.- </span><audio class="asado" src="audios/pregunta-ingles-4.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_4'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaNueve" id="respuestaNueve" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaNueve" id="respuestaNueve" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaNueve" id="opcionRespuestaNueve">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>                   
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><span class="font-weight-bold">QUESTION 5.- </span><audio class="asado" src="audios/pregunta-ingles-5.mp3" controls autoplay></td></tr>
                  <tr><td width="40%">Respuesta:</td><td width="60%"><?php echo $data['respuesta_ingles_5'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaDiez" id="respuestaDiez" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaDiez" id="respuestaDiez" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaDiez" id="opcionRespuestaDiez">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>  
              <br/><br/>




              <table class="table table-striped table-bordered" style="width:100%">
                <tbody> 
                  <tr><td colspan="2"><h3>Parte 3: Descripción de imagenes.</h3></td></tr>
                  <tr><td colspan="2"><img class="img-fluid" src="imagenes/Diapositiva16.JPG" width="444" height="283" alt="examen de ingles"/></td></tr>
                  <tr><td width="40%">Respuesta: </td><td width="60%"><?php echo $data['respuesta_imagen_1'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaOnce" id="respuestaOnce" value="ok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaOnce" id="respuestaOnce" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaOnce" id="opcionRespuestaOnce">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>  
              <br/><br/>



              <table class="table table-striped table-bordered" style="width:100%">
                <tbody>                   
                  <tr><td colspan="2"><img class="img-fluid" src="imagenes/Diapositiva17.JPG" width="444" height="283" alt="examen de ingles"/></td></tr>
                  <tr><td width="40%">Respuesta: </td><td width="60%"><?php echo $data['respuesta_imagen_2'];?></td></tr>
                  <tr>
                  <td width="40%"><input type="radio" name="respuestaDoce" id="respuestaDoce" value="nok" required> Ok</td>
                  <td width="60%">
                  <input type="radio" name="respuestaDoce" id="respuestaDoce" value="nok" required> Error
                  
                  <select class="form-control" name="opcionRespuestaDoce" id="opcionRespuestaDoce">
                    <option value="0">Seleccione una opción</option>
                    <option value="1">Uno</option>
                    <option value="2">Dos</option>
                  </select></td>
                  </tr>
                  </tbody>        
              </table>  
              <br/><br/>

            </div>
          </div>
        </div>  
    </div>
    <br/>  
    <div id="retornaError" class="container">
        <div class="row">
        <p>Se agrego el nivel de ingles para este usuario.</p>
        </div>
      </div>

      <br/><br/>
      <div id="respuestaAjax" class="container">
        <div class="row">
        <p>Se agrego el nivel de ingles para este usuario.</p>
        </div>
      </div>
    <?php
    if(empty($data['nivel'])){
    ?>
    <form id="form-asignar">
    <div class="container">  
      <div class="row">
          <div class="col-md-6 mb-3">
            <label for="asignar-nivel">Asignar nivel</label>
            <select class="custom-select d-block w-100" name="asig-nivel" id="asig-nivel" required="">
              <option value="">Seleccionar</option>
              <option value="1">Uno</option>
              <option value="2">Dos </option>
              <option value="3">Tres </option>
              <option value="4">Cuatro</option>
              <option value="5">Cinco</option>
              <option value="6">Seis</option>
            </select>
            <div class="invalid-feedback">
              Seleccione una sucural
            </div>
          </div>
      </div>
      <input type="hidden" id="id" name="id" value="<?php echo $data['id'];?>">
      <input type="hidden" id="unico" name="unico" value="<?php echo $data['uniq'];?>">
      <input type="hidden" id="correo" name="correo" value="<?php echo $data['correo'];?>">
      <input type="hidden" id="sucursal" name="sucursal" value="<?php echo $data['sucursal'];?>">


      <div class="row">
        <div class="col-md-6 mb-3">
          <button class="btn btn-primary  btn-block" type="submit">Asignar</button>
        </div>
      </div>
    </div>

        
    </form>
    <br/><br/>
    <?php
    }
    ?>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
<script src="js/jquery/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>   
<script>


function vamonos(){
  window.location.href = "examenes.php";
}


function mazinger(){
var asig_nivel = $("#asig-nivel").val();
var id         = $("#id").val();
var unico      = $("#unico").val();
var correo     = $("#correo").val();
var sucursal   = $("#sucursal").val();

$.ajax({
    type: "POST",
    url: "registrar-nivel.php",
    data: "asig_nivel=" + asig_nivel + "&id=" + id + "&unico=" + unico + "&correo=" + correo + "&sucursal=" + sucursal,
    success : function(text){
      
      limpio = text.trim();
      if(limpio == "exito"){
        vamonos();
      }else{

      }
      
    }
});

}




$("#form-asignar").submit(function(event){
event.preventDefault();
mazinger();
});
</script>
</body>
</html>