<?php
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
  date_default_timezone_set('America/Monterrey');
  require 'inc/conexion.php';
?>

<!doctype html>
<html lang="en">

  <head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171876901-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'UA-171876901-1');
    </script>

    <!-- Required meta tags -->
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <title>Plataforma de evaluaciones INBI</title>

    <style>
      .asado {
        width: 200px;
        height: 30px;
        vertical-align: middle;
      }

      .asado:active {
        border: none;
      }

      body{
        background: #F4F2F2 ;
      }

      .shadowCard{
        border-radius: 8px !important; 
        box-shadow: 0 20px 27px 0 rgba(0,0,0,0.1)  !important;
      }
    </style>

  </head>


<body>
  <?php
    if (!isset($_POST['entrar'])) {
  ?>
    <div class="container">
      <div class="card shadowCard mt-2">
        <div class="card-body">
          <!--Inicia Container -->
          <div class="row">

            <div class="col-md-12">
              <div class="text-center">
                <img class="img-fluid" src="img/logo.jpg" width="182" height="182" Alt="">
              </div>

              <div class="text-center">
                <h3>Examen de ubicación idioma Inglés</h3>
              </div>
            </div>
          </div>



          <!-- PREGUNTAS -->
          <div class="row">
            <div class="col-md-2"></div>

            <!-- CONTENIDO -->
            <div class="col-md-8">
              
              <br/>
              <br/>

              <form action="" method="POST" onsubmit="return evitarDuplicidadEnvio();" name="forma" id="forma">


                <!-- PREUNGTAS INCIIALES DEL FORMULARIO -->
                <div class="form-group">
                  <label for="nombre">Nombre del tutor (a)</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" required>
                </div>

                <div class="form-group">
                  <label for="telefono">Teléfono de contacto</label>
                  <input type="text" class="form-control" name="telefono" id="telefono" required>
                </div>

                <div class="form-group">
                  <label for="nombre_alumno">Nombre del Alumno(a): </label>
                  <input type="text" class="form-control" name="nombre_alumno" id="nombre_alumno" required>
                </div>

                <div class="form-group">
                  <label for="edad_alumno">Edad del Alumno(a): </label>
                  <input type="text" class="form-control" name="edad_alumno" id="edad_alumno" required>
                </div>

                <?php
                  $sql = "SELECT * FROM sucursales ORDER BY nombre";
                  
                  if (!$resultado = $conn->query($sql)) {
                    echo "Error al buscar las sucursales";
                    exit;
                  }
                ?>

                <div class="form-group">

                  <div class="row">

                    <!-- Sucursales -->
                    <div class="col-md-6">
                      <label for="exampleFormControlSelect1">Sucursal</label>
                      <select class="form-control" name="sucursal" id="sucursal" required>
                        <option value="">Seleccione una opción</option>
                        <?php
                          while ($data = $resultado->fetch_assoc()) {
                        ?>
                            <option value="<?php echo $data['id']; ?>"><?php echo $data['nombre']; ?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div>

                    <!-- HORARIO -->
                    <div class="col-md-6">
                      <label for="exampleFormControlSelect1">Horario</label>
                      <select class="form-control" name="horario" id="horario" required>
                        <option value="">Seleccione una opción</option>
                        <option value="tarde">Entre semana vespertino</option>
                        <option value="sabatino">Sabatino</option>
                      </select>
                    </div>

                  </div>

                  <br/>

                  <div class="row">

                    <!-- ¿Cómo consideras tu nivel de inglés? -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">¿Cómo consideras tu nivel de inglés?</label>
                        <select class="form-control" name="nivel_ingles" id="nivel_ingles" required>
                          <option value="">Seleccione una opción</option>
                          <option value="bajo">Bajo</option>
                          <option value="medio">Intermedio</option>
                          <option value="alto">Alto</option>
                        </select>
                      </div>
                    </div>

                    <!-- ¿Cuánto tiempo ha estudiado inglés?  -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">¿Cuánto tiempo ha estudiado inglés? </label>
                        <select class="form-control" name="tiempo_estudio" id="tiempo_estudio" required>
                          <option value="">Seleccione una opción</option>
                          <option value="nunca">Nunca</option>
                          <option value="menos_6_meses">Menos de 6 meses</option>
                          <option value="entre_6_y_1">Entre 6 meses y 1 año</option>
                          <option value="mas_de_1">Más de 1 año</option>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>

                <br/>
                <hr/>
                <br/>

                <!-- PREGUNTAS DEL FORMULARIOOOO -->

                <div>
                  <span class="font-weight-bold">
                    Instrucciones: 
                  </span>
                  <br/>
                  Permita que el alumno (a) conteste el examen sin ayuda, esta prueba no tiene como objetivo aprobar o reprobar, sino conocer las áreas de oportunidad de nuestros alumnos para ayudarles de la mejor manera. 
                  <br/>
                  <br/>
                  Si no sabe una respuesta, no pasa nada, solo seleccione <b>“No sé cómo se debe contestar”</b> 
                </div>

                <br />
                <br />

                <h5>Etapa 1.- Seleccione la idea que mejor describa la imagen que vera a continuación</h5>

                <br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h4>1.- What are the girls doing?</h4>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/imagen_1.jpg" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>Choose your answer</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p1" id="memoria1" value="0" required class="mr-1"> a) They are so happy.<br/>
                  <input type= "radio" name="parte1p1" id="memoria2" value="1" required class="mr-1"> b) They are running.<br/>
                  <input type= "radio" name="parte1p1" id="memoria3" value="2" required class="mr-1"> c) She are playing.<br/>
                  <input type= "radio" name="parte1p1" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br/>
                <br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h4>2.- Where is the lion?</h4>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/imagen_2.jpg" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>Choose your answer</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p2" id="memoria1" value="0" required class="mr-1"> a) It is on the grass.<br/>
                  <input type= "radio" name="parte1p2" id="memoria2" value="1" required class="mr-1"> b) It is in the grass.<br/>
                  <input type= "radio" name="parte1p2" id="memoria3" value="2" required class="mr-1"> c) It is at the grass.<br/>
                  <input type= "radio" name="parte1p2" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br/>
                <br/>
                
                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h4>3.- What is the kid holding?</h4>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/imagen_3.jpg" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>Choose your answer</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p3" id="memoria1" value="0" required class="mr-1"> a) A big smile.<br/>
                  <input type= "radio" name="parte1p3" id="memoria2" value="1" required class="mr-1"> b) A toy.<br/>
                  <input type= "radio" name="parte1p3" id="memoria3" value="2" required class="mr-1"> c) He is smiling.<br/>
                  <input type= "radio" name="parte1p3" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br/>
                <br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h4>4.- How old is he?</h4>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/imagen_4.jpg" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>Choose your answer</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p4" id="memoria1" value="0" required class="mr-1"> a) He is really happy.<br/>
                  <input type= "radio" name="parte1p4" id="memoria2" value="1" required class="mr-1"> b) He doesn´t have hair.<br/>
                  <input type= "radio" name="parte1p4" id="memoria3" value="2" required class="mr-1"> c) He is old.<br/>
                  <input type= "radio" name="parte1p4" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br/><br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h4>5.- What is there?</h4>
                    </span>
                  </label>
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/imagen_5.jpg" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>Choose your answer</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p5" id="memoria1" value="0" required class="mr-1"> a) There is a spoon.   <br/>
                  <input type= "radio" name="parte1p5" id="memoria2" value="1" required class="mr-1"> b) There are a fork.   <br/>
                  <input type= "radio" name="parte1p5" id="memoria3" value="2" required class="mr-1"> c) There is a blender. <br/>
                  <input type= "radio" name="parte1p5" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br/><br/>


                <h5>Etapa 2.- Traduzca de forma correcta las siguientes oraciones</h5>
                <br/>


                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>6.- “Esos carros son muy rápidos”</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p6" id="memoria1" value="0" required class="mr-1"> a) This cars are very fast.  <br/>
                  <input type= "radio" name="parte1p6" id="memoria2" value="1" required class="mr-1"> b) Those cars are very fast. <br/>
                  <input type= "radio" name="parte1p6" id="memoria3" value="2" required class="mr-1"> c) These cars are very fast. <br/>
                  <input type= "radio" name="parte1p6" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>7.- “Yo comí muchas frutas ayer”</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p7" id="memoria1" value="0" required class="mr-1"> a) I ate many fruits yesterday.   <br/>
                  <input type= "radio" name="parte1p7" id="memoria2" value="1" required class="mr-1"> b) I eaten many fruits yesterday. <br/>
                  <input type= "radio" name="parte1p7" id="memoria3" value="2" required class="mr-1"> c) I eat many fruits yesterday.   <br/>
                  <input type= "radio" name="parte1p7" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar   
                </div>
                <br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>8.- “Mi mejor amiga escribirá un libro”</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p8" id="memoria1" value="0" required class="mr-1"> a) My best friend is writing a book. <br/>
                  <input type= "radio" name="parte1p8" id="memoria2" value="1" required class="mr-1"> b) My best friend writes a book.     <br/>
                  <input type= "radio" name="parte1p8" id="memoria3" value="2" required class="mr-1"> c) My best friend will write a book. <br/>
                  <input type= "radio" name="parte1p8" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>9.- “Chicago es una ciudad hermosa”</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p9" id="memoria1" value="0" required class="mr-1"> a) Chicago is a city beautiful. <br/>
                  <input type= "radio" name="parte1p9" id="memoria2" value="1" required class="mr-1"> b) Chicago is a beautiful city. <br/>
                  <input type= "radio" name="parte1p9" id="memoria3" value="2" required class="mr-1"> c) Chicago is a beautiful.      <br/>
                  <input type= "radio" name="parte1p9" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br />

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      <h5>10.- “Mi papá trabaja en una compañía grande”</h5>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p10" id="memoria1" value="0" required class="mr-1"> a) My dad work in a big company.    <br/>
                  <input type= "radio" name="parte1p10" id="memoria2" value="1" required class="mr-1"> b) My dad works in a big company.   <br/>
                  <input type= "radio" name="parte1p10" id="memoria3" value="2" required class="mr-1"> c) My dad working in a big company. <br/>
                  <input type= "radio" name="parte1p10" id="memoria4" value="3" required class="mr-1"> d) No se cómo se debe contestar
                </div>
                <br />
                
                <h5>Etapa 3.- Escuche los siguientes audios y seleccione la respuesta que considere correcta</h5>
                <br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      11.- What pet does he have?
                      <br/>
                      <br/>
                      <audio class="asado" src="audios/1.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p11" id="memoria1" value="0" required class="mr-1">a) A friend. <br/>
                  <input type= "radio" name="parte1p11" id="memoria2" value="1" required class="mr-1">b) A dog     <br/>
                  <input type= "radio" name="parte1p11" id="memoria3" value="2" required class="mr-1">c) A cat.    <br/>
                  <input type= "radio" name="parte1p11" id="memoria4" value="3" required class="mr-1">d) No se cómo se debe contestar
                </div>
                <br/><br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      12.- Where does he read books sometimes?
                      <br/>
                      <br/>
                      <audio class="asado" src="audios/2.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p12" id="memoria1" value="0" required class="mr-1">a) At home.        <br/>
                  <input type= "radio" name="parte1p12" id="memoria2" value="1" required class="mr-1">b) In the library. <br/>
                  <input type= "radio" name="parte1p12" id="memoria3" value="2" required class="mr-1">c) At school.      <br/>
                  <input type= "radio" name="parte1p12" id="memoria4" value="3" required class="mr-1">d) No se cómo se debe contestar
                </div>
                <br/><br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      13.- Where does his father work?
                      <br/>
                      <br/>
                      <audio class="asado" src="audios/3.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p13" id="memoria1" value="0" required class="mr-1">a) In a restaurant.        <br/>
                  <input type= "radio" name="parte1p13" id="memoria2" value="1" required class="mr-1">b) They are sad sometimes. <br/>
                  <input type= "radio" name="parte1p13" id="memoria3" value="2" required class="mr-1">c) In a hospital.          <br/>
                  <input type= "radio" name="parte1p13" id="memoria4" value="3" required class="mr-1">d) No se cómo se debe contestar
                </div>
                <br/><br/>

                <div class="form-group">
                  <label for="respuesta-ingles-1">
                    <span class="font-weight-bold">
                      14.- What food does he like?
                      <br/>
                      <br/>
                      <audio class="asado" src="audios/4.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/>
                    </span>
                  </label>
                  <br/>
                  <input type= "radio" name="parte1p14" id="memoria1" value="0" required class="mr-1">a) He loves Asian food.   <br/>
                  <input type= "radio" name="parte1p14" id="memoria2" value="1" required class="mr-1">b) He likes Western food. <br/>
                  <input type= "radio" name="parte1p14" id="memoria3" value="2" required class="mr-1">c) He loves any food.     <br/>
                  <input type= "radio" name="parte1p14" id="memoria4" value="3" required class="mr-1">d) No se cómo se debe contestar
                </div>
                <br />

                <h5>Etapa 4.- Describa la siguiente imagen en INGLÉS usando todas las palabras que sean posibles. Entre más escriba es mejor</h5>
                <br/>

                <div class="form-group">
                  <label for="respuesta-ingles-imagen1">
                    <img class="img-fluid" src="imagenes/imagen_6.jpg" width="644" height="483" alt="examen de ingles"/>
                  </label>
                  <br/>
                  <textarea class="form-control" name="respuesta_imagen_1" id="respuesta_imagen_1" rows="3" required></textarea>
                </div>

                <style>
                  #enviando {
                    display: none;
                    text-align: center;
                  }
                </style>

                <div class="text-center">
                  <button type="submit" id="entrar" name="entrar" class="btn btn-primary">Finalizar</button>
                </div>

                <div class="form-group">
                  <div class="text-center">
                    <table width="100%">
                      <tr>
                        <td width="33%"></td>
                        <td width="33%"><button id="enviando" type="button" class="btn btn-info"> Enviando formulario </button></td>
                        <td width="33%"></td>
                      </tr>
                    </table>

                  </div>
                </div>

              </form>
              <br/><br/>

            </div>
          <div class="col-md-2"></div>


          </div>
      </div>
      <!-- Termina el formulario-->
    </div>
    <!--Termima container-->
    <script>
      function verRespuesta(){
        var sum = 0;
        let parte1p1   = document.getElementsByName('parte1p1');
        let parte1p2   = document.getElementsByName('parte1p2');
        let parte1p3   = document.getElementsByName('parte1p3');
        let parte1p4   = document.getElementsByName('parte1p4');
        let parte1p5   = document.getElementsByName('parte1p5');
        let parte1p6   = document.getElementsByName('parte1p6');
        let parte1p7   = document.getElementsByName('parte1p7');
        let parte1p8   = document.getElementsByName('parte1p8');
        let parte1p9   = document.getElementsByName('parte1p9');
        let parte1p10  = document.getElementsByName('parte1p10');
        let parte1p11  = document.getElementsByName('parte1p11');
        let parte1p12  = document.getElementsByName('parte1p12');
        let parte1p13  = document.getElementsByName('parte1p13');
        let parte1p14  = document.getElementsByName('parte1p14');
        
        if(parte1p1[1].checked){ sum += 1; }
        if(parte1p2[0].checked){ sum += 1; }
        if(parte1p3[1].checked){ sum += 1; }
        if(parte1p4[2].checked){ sum += 1; }
        if(parte1p5[0].checked){ sum += 1; }
        if(parte1p6[1].checked){ sum += 1; }
        if(parte1p7[0].checked){ sum += 1; }
        if(parte1p8[2].checked){ sum += 1; }
        if(parte1p9[1].checked){ sum += 1; }
        if(parte1p10[1].checked){ sum += 1; }
        if(parte1p11[1].checked){ sum += 1; }
        if(parte1p12[1].checked){ sum += 1; }
        if(parte1p13[2].checked){ sum += 1; }
        if(parte1p14[1].checked){ sum += 1; }
        
      }
    </script>

    <?php  
  }
  else{
    $sum = 0;

    $parte1p1  = limpiar($_POST['parte1p1']);
    $parte1p2  = limpiar($_POST['parte1p2']);
    $parte1p3  = limpiar($_POST['parte1p3']);
    $parte1p4  = limpiar($_POST['parte1p4']);
    $parte1p5  = limpiar($_POST['parte1p5']);
    $parte1p6  = limpiar($_POST['parte1p6']);
    $parte1p7  = limpiar($_POST['parte1p7']);
    $parte1p8  = limpiar($_POST['parte1p8']);
    $parte1p9  = limpiar($_POST['parte1p9']);
    $parte1p10 = limpiar($_POST['parte1p10']);
    $parte1p11 = limpiar($_POST['parte1p11']);
    $parte1p12 = limpiar($_POST['parte1p12']);
    $parte1p13 = limpiar($_POST['parte1p13']);
    $parte1p14 = limpiar($_POST['parte1p14']);

    if($parte1p1 == 1){ $sum += 1; }
    if($parte1p2 == 0){ $sum += 1; }
    if($parte1p3 == 1){ $sum += 1; }
    if($parte1p4 == 2){ $sum += 1; }
    if($parte1p5 == 0){ $sum += 1; }
    if($parte1p6 == 1){ $sum += 1; }
    if($parte1p7 == 0){ $sum += 1; }
    if($parte1p8 == 2){ $sum += 1; }
    if($parte1p9 == 1){ $sum += 1; }
    if($parte1p10 == 1){ $sum += 1; }
    if($parte1p11 == 1){ $sum += 1; }
    if($parte1p12 == 1){ $sum += 1; }
    if($parte1p13 == 2){ $sum += 1; }
    if($parte1p14 == 1){ $sum += 1; }

    $nombre          = limpiar($_POST['nombre']);
    $telefono        = limpiar($_POST['telefono']);
    $nombre_alumno   = limpiar($_POST['nombre_alumno']);
    $edad_alumno     = limpiar($_POST['edad_alumno']);
    $sucursal        = limpiar($_POST['sucursal']);
    $horario         = limpiar($_POST['horario']);
    $nivel_ingles    = limpiar($_POST['nivel_ingles']);
    $tiempo_estudio  = limpiar($_POST['tiempo_estudio']);

    $sqlNombreSucursal="SELECT nombre FROM sucursales WHERE id=$sucursal";

    if (!$resultado = $conn->query($sqlNombreSucursal)) {
      $nombreSucursal ="SIN SUCURSAL";
    }

    $data = $resultado->fetch_assoc();
    $nombreSucursal = $data['nombre'];

    $horario         = limpiar($_POST['horario']);

    switch($horario){
      case "manana":
      $nombreHorario="Mañana";
      break;
      case "tarde":
      $nombreHorario="Tarde";
      break;
      case "sabatino":
      $nombreHorario="Sabatino";
      break;
      default:
      $nombreHorario="SIN HORARIO";
      break;
    }

    $fecha              = date("Y-m-d H:i:s");
    $uniq               = uniqid();
    $origen             = "WEB";
    $respuesta_imagen_1 = limpiar($_POST['respuesta_imagen_1']);

    $parte1p1 = limpiar($_POST['parte1p1']);

    if (mysqli_connect_errno()) {
      echo "error al conectar la base de datos";
      exit();
    }

    $sql = "INSERT INTO examen_ubicacion (
      uniq,
      nombre,
      correo,telefono,nombre_alumno,edad_alumno,tiempo_estudio,motivo_aprender,sucursal,horario,nivel_ingles,fecha_registro,origen,respuesta_imagen_1,cant) VALUES (
      '".$uniq."',
      '".$nombre."',
      '".$correo."',
      '".$telefono."',
      '".$nombre_alumno."',
      '".$edad_alumno."',
      '".$tiempo_estudio."',
      '".$motivo_aprender."',
      ".$sucursal.",
      '".$horario."',
      '".$nivel_ingles."',
      '".$fecha."',
      '".$origen."',
      '".$respuesta_imagen_1."',
      '".$sum."')";

      // echo $sql;

      if ($conn->query($sql) === TRUE) {

        $to      = 'inbiventas@gmail.com,inbiventas1@gmail.com,armando.molina@inbi.mx,informes@inbi.mx,devany.molina@inbi.mx';
        $subject = 'Registro de examen de ubicación Inbi.';


        $headers = "From: " . $correo. "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $message = "
        <!doctype html>
        <html lang=\"en\">
        <head>
        <title>Registro de examen de ubicación.</title>
        <style>
        body {
          margin: 0;
          padding: 0;
        }
        h1 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 110%;
          font-weight: bold;
          color: #2E86C1;
        }

        h2 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 100%;
          font-weight: bold;
          color: #2E86C1 ;
        }
        p{
          color: #666;
          font-family: Arial, Helvetica, sans-serif;
        }
        .center {
          position: relative;
          margin: auto;
          padding: auto;
          text-align: center;
        }
        .contenedor {
          width: 80%;
          height: auto;
          padding: 0;
          margin: auto;
          background-color: #fff;
        }
        .centro {
          width: 200px;
          height: 30px;
          vertical-align: middle;
        }
        .asado:active {
          border: none;
        }
        .linea {
          width: 100%;
          height: 1px;
          border: 1px solid #cdcdcd;
        }
        .go {
          background-color:#FF5733;
          color:#fff;
          border:none;
          padding:10px 15px 10px 15px;
          font-family: Arial, Helvetica, sans-serif;
        }
        .dato-title {
          font-size: 100%;
          font-weight: bold;
          font-family: Arial, Helvetica, sans-serif;
        }
        .dato-description {
          font-size: 100%;
          font-weight: normal;
          font-family: Arial, Helvetica, sans-serif;
        }

        </style>


        </head>
        <body>
        <div class=\"contenedor\">
        <h1>INBI SCHOOL</h1>

        <h2>¡Informacion de la persona que contesto el examen de ubicación de INBI SCHOOL!</h2>

        <p>".$nombre." a contestao el examen de evaluación, este correo es una notificación con la información basica, 
        si quiere consultar el registro completo haga click en el boton Ir al registro. </p>
        <br/><br/><br/>

        <table cellpadding=\"8\">
        <tr style=\'background: #eee;\'><td><span class=\"dato-title\">Name:</span> </td><td><span class=\"dato-description\">" . $nombre . "</span></td></tr>
        <tr><td><span class=\"dato-title\">Correo:</span></td><td><span class=\"dato-description\">" . $correo . "</span></td></tr>
        <tr><td><span class=\"dato-title\">Teléfono:</span> </td><td><span class=\"dato-description\">" . $telefono . "</span></td></tr>
        <tr><td><span class=\"dato-title\">Motivos de aprendizaje:</span> </td><td><span class=\"dato-description\">" . $motivo_aprender . "</span></td></tr>
        <tr><td><span class=\"dato-title\">Horarios que solicita:</span> </td><td><span class=\"dato-description\">" . $nombreHorario . "</span></td></tr>
        <tr><td><span class=\"dato-title\">Horarios que solicita:</span> </td><td><span class=\"dato-description\">" . $nombreSucursal . "</span></td></tr>
        <tr><td><span class=\"dato-title\">Fecha:</span> </td><td><span class=\"dato-description\">" . $fecha . "</span></td></tr>
        </table>
        <br/><br/>
        <div class=\"center\">

        </div>

        </div>
        </body>
        </html>
        ";

        mail($to, $subject, $message, $headers);
        ?>

        <!-- <span class=\"go\"><a href=\"https://www.fastenglish.com.mx/prueba-url.php?v=".$uniq."\">Ir al registro</a></span> -->
        <br/>
        <br/>
        <br/><br/>
        <div class="container">
          <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
              <div class="text-center">
                <img src="img/logo.jpg">
              </div>
              <p class="text-center">
                Has contestado el examen con éxito, un representante de nuestra escuela se comunicará contigo a la brevedad al número que nos proporcionaste. 
                De igual manera debes estar al pendiente de tú correo para los avisos referentes a futuros exámenes. 
              Gracias por tú participación.</p>
            </div>
            <div class="col-md-3">
            </div>
          </div>
        </div>
        <script>
      // setTimeout(function(){ window.location.href = "https://www.fastenglish.com.mx"; }, 10000);
        </script>

        <?php
      } else {
        echo "Error al registrar, los datos del examen.";

      }

      $conn->close();
    }
    ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  </html>