<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Practicar Inglés Usando VideoJuegos";
    $canonical = "<link rel='canonical' href='https://inbi.mx/practicar-ingles-usando-videojuegos'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/practicar-ingles-usando-videojuegos.php';
    include 'includes/footers/footer.php';
?>