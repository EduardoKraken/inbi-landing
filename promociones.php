<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Becas para estudiar inglés - Aprende el idioma al mejor precio ";
    $metadescription = "El inglés es la mejor inversión que se puede hacer. Con nuestros programas de becas podrás lograr tu meta de ser bilingüe. Conoce todos los detalles de las becas.";
    $canonical = "<link rel='canonical' href='https://inbi.mx/promociones'/>";

    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/promociones.php';
    include 'includes/footers/footer.php';
?>