<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "La mejor escuela de inglés - Cursos para todas las edades - INBI";
  $metadescription = "Tenemos clases presenciales y en línea en nuestros 8 planteles en Nuevo León. Somos un centro examinador autorizado de Cambridge. Conoce nuestras becas.";
  $canonical = "<link rel='canonical' href='https://inbi.mx/'/>";
  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/inicio.php';
  include 'includes/footers/footer.php';
?>