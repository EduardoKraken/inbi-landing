<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Curso de inglés para niños - Regálales un mejor futuro";
    $metadescription = "En nuestro curso tus hijos se divertirán mientras aprenden. Nuestros planteles cuentan con todas las medidas de seguridad. ¡Conoce nuestras becas!";
    $canonical = "<link rel='canonical' href='https://inbi.mx/curso-kids'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/cursos/curso-kids.php';
    include 'includes/footers/footer.php';
?>