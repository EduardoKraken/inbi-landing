<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "¿Cómo identificar tu nivel de inglés?";
    $canonical = "<link rel='canonical' href='https://inbi.mx/como-identificar-tu-nivel-de-ingles'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/como-identificar-tu-nivel-de-ingles.php';
    include 'includes/footers/footer.php';
?>