<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "El inglés y el internet";
    $canonical = "<link rel='canonical' href='https://inbi.mx/ingles-internet'/>";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blogs/ingles-internet.php';
    include 'includes/footers/footer.php';
?>