<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "Cursos";
  $canonical = "<link rel='canonical' href='https://inbi.mx/cursos'/>";
  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/cursos/cursos.php';
  include 'includes/footers/footer.php';
?>