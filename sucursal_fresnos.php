<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Clases de inglés en Apodaca - Clases para todas las edades";
    $metadescription = "Estamos muy cerca de ti, conoce nuestros planteles en Apodaca. Aprende inglés al mejor precio con nuestras becas. Conoce nuestras promociones y sé bilingüe.";
    $canonical = "<link rel='canonical' href='https://inbi.mx/sucursal_fresnos'/>";

    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/sucursales/sucursal_fresnos.php';
    include 'includes/footers/footer.php';
?>